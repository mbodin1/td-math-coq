\documentclass[runningheads,francais]{llncs}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{todonotes}
\usepackage{amsmath}
\usepackage{tkz-tab}
\usepackage{newtxtext,newtxmath}
\usepackage{textcomp}
\usepackage{orcidlink}
\usepackage{caption}
\usepackage{subcaption}
\usetikzlibrary{backgrounds,calc}



\def\keywordname{{\bf Mots-clefs :}}

\title{Instrumentation de l'association de registres sémiotiques dans un assistant de preuve}
\author{
	Emmanuel Beffara\inst{1}\orcidlink{0000-0003-1993-3401}
	\and
	Martin Bodin\inst{2}\orcidlink{0000-0003-3588-3782}
	\and
	Nadine Mandran\inst{1}\orcidlink{0000-0002-8660-3827}
	\and
	Rémi Molinier\inst{3}\orcidlink{0000-0002-3742-5307}
}
\institute{
	% Univ. Grenoble Alpes, IREM de Grenoble
	% \and
	Univ. Grenoble Alpes, CNRS, Grenoble INP, LIG, 38000 Grenoble, France
	\and
	Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France
	\and
	Univ. Grenoble Alpes, CNRS, Institut Fourier, F-38000 Grenoble, France
}

\begin{document}

\maketitle

\begin{abstract}
	Les assistants de preuve sont des outils informatiques conçus pour
	accompagner l'écriture de démonstrations mathématiques formalisées et
	leur utilisation pour l'enseignement de la preuve fait l'objet de diverses
	expérimentations en université.
	Leur mode d'interaction se limite habituellement au registre textuel, à
	l'opposé de la pratique courante sur papier qui exploite souvent différents
	registres visuels.
	Ce travail propose la conception et l'évaluation d'un prototype d'interface
	qui permette d'associer des registres textuels et visuels pour améliorer
	la flexibilité de l'outil et faciliter l'apprentissage de la preuve en
	mathématiques.
\end{abstract}

\keywords{enseignement des mathématiques
\and registres sémiotiques
\and instrumentation
\and assistant de preuve}

\section{Introduction}

%\subsection{La démonstration en mathématiques}

La preuve mathématique est un objet d'un haut niveau d'abstraction dont
l'acquisition est un enjeu d'enseignement majeur
à l'entrée à l'université.
La didactique met en évidence le rôle
crucial de l'articulation entre les différents rôles qu'elle
joue~\cite{hanna-2000-proof}: au delà de la validation des assertions
(vérification, explication), la preuve a un rôle de
communication (présentation systématique des résultats et
transmission à une communauté) et elle intervient dans le processus
de recherche (exploration des conséquences d'une hypothèse, élaboration de
conjectures, découverte de résultats).
À chaque rôle correspondent des savoir-faire différents, ce qui fait
de la preuve un objet dont la compréhension et l'enseignement restent
difficiles.
De plus, au passage du secondaire à l'université correspond un saut
dans le niveau d'exigence formel quant à la production de démonstrations,
avec un passage de l'argumentation à du raisonnement essentiellement
déductif~\cite{balacheff-2019-argumentation}.

%\subsection{Assistants de preuve}

Au vu de ces difficultés, il est naturel de vouloir
instrumenter la preuve comme on instrumente d'autres objets fondamentaux
des mathématiques.
En effet, calculatrices, tableurs, traceurs de courbes, logiciels de géométrie
ou de calcul formel sont présents en classe et intégrés
aux usages.
Des assistants de preuve, logiciels dédiés à l'élaboration interactive de
démonstrations, ont été développés depuis la fin des années 1960, initialement
pour la recherche.
Ils visent principalement des experts,
mais le besoin d'élargir le public visé a conduit au développements
d'assistants (cf. Mizar, CalcCheck) dont la syntaxe est lisible par des
mathématiciens qui ne connaissent pas l'outil.
% De plus, la plupart des assistants de preuve permettent l'extension de leur
% syntaxe via la définition de nouvelles notations, ce qui permet de rapprocher
% les notations utilisées de celles d'une communauté externe.

%\subsection{Assistants de preuve dans l'enseignement}

Après des premières expériences dans les années 2000~\cite{raffalli-2002-computer,blanc-2007-proofs},
l'intégration d'assistants de
preuve dans l'enseignement supérieur est expérimenté depuis quelques années
dans différentes universités~\cite{kerjean-2022-utilisation}.
On observe généralement un meilleur engagement des étudiants dans la
recherche de preuve avec l'outil que dans une modalité classique sur papier,
ce qui s'explique notamment par la présence de rétroactions immédiates,
et la facilitation du tâtonnement.
Les assistants de preuve semblent favoriser l'utilisation
correcte %du langage mathématique,
de la logique et du symbolisme mais peuvent
aussi conduire les étudiants vers de la preuve très formelle, renforçant
les aspects syntaxiques au détriment du sens.
En effet, du fait de leur grande technicité, ces outils peuvent faire
obstacle à l'apprentissage des
notions visées, au risque que l'enseignement instrumenté par assistant de preuve
devienne un enseignement de l'outil et de ses spécificités plutôt que de la
preuve mathématique en tant que telle.
Face à ce problème, plusieurs outils (cf. Edukera, D\(\exists\forall\)duction) ont été conçus
en simplifiant l'interface proposée à l'étudiant, au prix d'une limitation
des possibilités offertes en termes de démonstration.

% Par ailleurs, l'utilisation d'assistants de preuve est le plus souvent
% réservée à des cursus d'informatique où les
% enseignants sont généralement eux-mêmes experts dans l'usage de l'outil
% et les étudiants sont déjà habitués à utiliser ce type d'interface,
% proche des environnements utilisés pour la programmation.
% De plus, ces cours visent explicitement la maitrise de l'outil et leur but
% est rarement d'apprendre des notions mathématiques nouvelles en
% utilisant l'assistant de preuve comme support.

%\subsection{Variété des registres}

Une autre difficulté est liée à l'écart entre la pratique courante de la
preuve et le cadre formel imposé par les assistants.
Dans la pratique experte, la preuve exploite
souvent des changements de registre sémiotique~\cite{duval-1993-registres}
avec l'emploi de figures, schémas, diagrammes, etc.
Chaque registre a ses règles et conventions propres qui en font
un outil de compréhension et de communication et l'association de
différents registres enrichit la compréhension en variant les représentations
d'un même objet.
Dans l'enseignement, l'articulation entre le schéma et la preuve textuelle est
centrale, dès le collège avec la géométrie, puis
au lycée avec par exemple l'emploi de courbes
ou de tableaux, puis à l'université dans différentes
branches des mathématiques.
L'utilisation pertinente des différents registres fait donc partie
des enjeux de l'enseignement des mathématiques.

Pourtant, en dehors de la géométrie où le registre visuel est
intrinsèque à l'objet d'étude, les assistants de preuve sont généralement
focalisés sur la production de démonstrations purement textuelles, sous forme
de programmes (pour les outils experts) ou de rédactions idéalisées
(pour les outils à visée pédagogique)
% La possibilité de définir des notations plus familières
% ne remet pas en cause cette exclusivité du registre textuel.
et les registres visuels restent alors au brouillon et à la charge de
l'apprenant, sans statut explicite au sein de la preuve.
Dans l'optique d'une instrumentation de
l'apprentissage de la preuve, il semble donc important de prendre en compte
cette nécessaire association de différents registres.
% De même que l'assistant accompagne l'écriture d'une démonstration
% textuelle et en vérifie les étapes, il
% semblerait utile qu'il propose des représentations des situations de preuve
% dans d'autres registres comme moyens de visualiser et d'élaborer la preuve.

\section{Travaux connexes}

En géométrie,
où la dimension visuelle est inhérente au sujet,
beaucoup de travaux ont été
menés en EIAH et en
didactique, depuis l'introduction de la géométrie
dynamique~\cite{laborde-1994-cabrigeometre} où la
préoccupation pour la preuve a conduit à la réalisation
de micro-mondes pour le raisonnement géométrique~\cite{luengo-1999-analyse}.
Des travaux plus récents ont formalisé la géométrie euclidienne
dans des assistants de preuve et fourni des interfaces
exploitant des outils existants de géométrie
dynamique~\cite{guilhot-2005-formalisation,pham-2012-combination}.
%
En dehors de la géométrie,
peu de développements exploitent l'association de différents registres.
Des expériences existent,
comme ProofWeb~\cite{hendriks-2010-teaching} qui affiche
la preuve en cours sous forme d'arbre de dérivation
(cf Figure~\ref{fig:proofweb})
ou Alectryon~\cite{pit-claudel-2020-untangling} qui permet
de représenter visuellement des objets manipulés par l'assistant de preuve
Coq~\cite{coq}, ce qui permet d'incorporer des éléments graphiques dans
l'affichage au lieu de notations
lourdes ou inhabituelles.

Ces éléments viennent en support
visuel, afin de mieux comprendre l'état interne de l'assistant de preuve:
il est toujours nécessaire d'utiliser des commandes textuelles pour interagir.
En cela, ces approches ne changent pas radicalement le registre utilisé pour
l'interaction avec l'outil.

La communauté Coq s’est montrée intéressée par ces différents outils,
même en mettant de côté leur intérêt pédagogique.
En effet, ajouter un support graphique (même partiel)
aide à mieux communiquer sur des résultats certifiés.
De plus, certains domaines s'appuient naturellement sur des diagrammes,
et ce support pourrait faciliter le développement des bibliothèques associées
(notamment en théorie des catégories~\cite{ANR-coreact}).

\section{Proposition de contribution}

Notre premier objectif est de construire un prototype pour assister
l'enseignement de la preuve en exploitant l'articulation entre plusieurs
registres.
Une attention particulière est portée sur l'objectif de faire de l'assistant
de preuve un instrument d'apprentissage appropriable par l'étudiant, dans un
mécanisme de genèse instrumentale~\cite{rabardel-1995-hommes}.
Nous nous basons pour cela sur la Théorie des situations
didactiques~\cite{brousseau-1998-theorie} et notamment le modèle
cK\textcent~\cite{balacheff-2003-ck} qui vise à l'opérationnaliser:
ce cadre permet d'identifier les caractéristiques de l'EIAH en tant que milieu
et les variables didactiques qu'elles induisent
(complexité des raisonnements nécessaires, possibilités d'interaction,
degré d'automatisation, etc.) et par la suite permettra une
modélisation des connaissances de l'apprenant par l'analyse des stratégies
de résolution qu'il adpote.
En conséquence, un second objectif est de valider la pertinence de ces modèles
dans le contexte particulier des assistants de preuve.

Dans cette première version, nous concevons une implémentation avec Coq des
tableaux de variation (voir Figure~\ref{fig:tv}) destinée à être manipulée
par des étudiants de début d'université.
Il s'agit d'une représentation visuelle d'un ensemble d'assertions logiques
associées à une méthode de démonstration, l'agencement de l'information dans
le tableau est porteur de sens et sa construction guide et structure le
raisonnement. Par ailleurs, il fait partie de la pratique courante au sortir du lycée, ce
qui permet de s'appuyer sur un savoir-faire existant chez les étudiants.

% Par ailleurs, ce qui se jouera avec les tableaux de variation pourra être
% transposé à d'autres supports visuels utilisés pour démontrer (diagrammes de
% Venn, diagrammes commutatifs, etc).

L'outil envisagé permettra de naviguer librement entre le registre visuel du tableau,
où l'étudiant peut dialoguer avec l'assistant dans une interface graphique,
et le registre textuel habituel de l'assistant.
On peut alors envisager un spectre d'utilisations de l'outil,
du purement graphique (avec restitution éventuelle d'une preuve écrite)
au purement textuel (où l'outil produit une illustration graphique évoluant à chaque étape).
La Figure~\ref{fig:tv:interactions} illustre ainsi deux interactions possibles:
des conjectures associées à des preuves textuelles,
ou une interaction purement graphique avec des étapes de raisonnement.
%Lorsque le tableau est initialement créé, il est vide (tableau de gauche sur la figure).
%%
%Une première interaction consisterait à faire des conjectures,
%par exemple en proposant des valeurs (signe ou variation),
%comme dans le tableau en haut à droite de la figure.
%Ces conjectures seraient clairement indiquées comme telles:
%dans la Figure~\ref{fig:tv:interactions} la couleur rouge indique que la décroissance de~\(f\) sur \([1, +\infty[\) n'est pas encore prouvée.
%L'utilisateur peut alors les résoudre en ligne de commande ou continuer son interaction avec le tableau.
%%
%Un autre type d'interaction consiste à ajouter des étapes intermédiaires de raisonnements sur le tableau,
%par exemple en ajoutant une ligne pour le signe de \(1-x\),
%pour déduire le signe de \(f'\) et donc les variations de \(f\).
%Si ces étapes intermédiaires sont suffisamment immédiates,
%Coq pourra les valider telles quelles sans avoir besoin d'utilsier la ligne de commande.

\section{Méthode de conduite de la recherche}

Nous conduisons nos recherches selon les principes du Design-Based Research~\cite{wang-2005-designbased}
ainsi que les guides associés (citation d'auteur).
Cette méthode propose, entre autres, de construire les connaissances et les
outils associés de manière itérative en intégrant les acteurs du terrain. 
Ainsi, nous organiserons des focus-groups avec des étudiants pour évaluer le prototype et ensuite nous mènerons des études in situ en début de cycle à l'université.
Nous nous interrogerons sur l'utilisabilité de l'outil, l'engagement des
étudiants dans cette activité et leur utilisation de l'association des
registres. Les premiers résultats attendus sont une version du prototype et les avis des étudiants ayant participé au focus-group. 




%du Pour ce faire, nous adoptons la posture épistémologique du Constructivisme
%Pragmatique (CP) \cite{avenier-2015-finding}, dont la construction des
%hypothèses de la connaissance scientifique est basée sur la prise en compte de
%la réalité et de l'humain dans cette réalité.
%Nous avons choisi la méthode CP car dans ce travail, nous avons besoin de
%données de terrain produites pour construire et évaluer le prototype.
%Nous nous appuyons également sur le Design-Based Research
%\cite{wang-2005-designbased} ainsi que les guides associés (citation d'auteur)
%pour mener notre recherche.
%Cette méthode propose, entre autres, de construire les connaissances et les
%outils associés de manière itérative en intégrant les acteurs du terrain. 





\bibliography{biblio}
\bibliographystyle{splncs04}

\begin{figure}[p]
	\begin{center}
	\includegraphics[width=\textwidth]{images/ProofWeb.png}
	\end{center}
	\caption{L'interface de ProofWeb~\cite{hendriks-2010-teaching}, qui affiche l'arbre de preuve (en bas à droite) lors de la construction textuelle de la preuve (à gauche)}
	\label{fig:proofweb}
\end{figure}

\begin{figure}[p]
  \[
    \begin{cases}
      f(x) = 3x^2-2x^3 \\
      f'(x) = 6x(1-x)
    \end{cases}
    \qquad 
    \begin{tikzpicture}[baseline=(current bounding box.center)]
      \tkzTab[lgt=1.2,espcl=2]
      {$x$/.6, $f'(x)$/.6, $f(x)$/1.2}
      {$-\infty$, $0$, $1$, $+\infty$}
      {,-,z,+,z,-,}
      {+/,-/$0$,+/$1$,-/};
    \end{tikzpicture}
  \]
  \caption{Exemple de tableau de variation}
  \label{fig:tv}
\end{figure}

\begin{figure}[p]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\begin{tikzpicture}
		\begin{scope}
				\tkzTab[lgt=1.2,espcl=2]
				{$x$/.6, $f(x)$/1}
				{$-\infty$, $+\infty$}
				{ , };
				\node(start) at (T21) {};
		\end{scope}
		% Version « complète ».
		%\tikzset{arrow style/.style = {->, red}}
		%\tkzTabInit[color,colorT=red!10,lgt=1.2,espcl=2]
		%	{$x$/.6, $f(x)$/1.2}
		%	{$-\infty$, 0, 1, $+\infty$};
		%\tkzTabVar[color=red]{+/,-/$0$,+/$1$,-/};
		%%\node (recta) at (T11) {};
		%%\node (rectb) at (T22) {};
		%%\begin{scope}[on background layer]
		%%\draw[fill=red!50,opacity=.3] (T11) rectangle (T22);
		%%\end{scope}
			%
			% Version juste après 1.
			\begin{scope}[xshift=5cm]
			\tikzset{arrow style/.style = {opacity=0}}
			\tkzTabInit[lgt=1.2,espcl=2] % color,colorT=red!10
				{$x$/.6, $f(x)$/1.2}
				{$-\infty$, 1, $+\infty$};
			\tkzTabVar%[color=red]
				{+/,+/$1$,-/};
			\tikzset{arrow style/.style = {opacity=1, ->}}
			\draw[->, thick, shorten > = 3mm, shorten < = 3mm] ($(N21)+(0,-.7em)$) -- ($(N32)+(0,.5em)$) node [midway, yshift=1ex] {{\scriptsize\textbf{?}}} ;
			\node(proofOblig) at (T01) {};
			\end{scope}
		\draw[->, thick] (start) to [in = 180, out = 0] (proofOblig) ;
		\end{tikzpicture}
		\caption{Un exemple d'interaction: émettre une conjecture qu'il faudra montrer en ligne de commande}
		\label{fig:tv:interactions:conjecture}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\begin{tikzpicture}
		\begin{scope}
				\tkzTab[lgt=1.2,espcl=2]
				{$x$/.6, $f(x)$/1}
				{$-\infty$, $+\infty$}
				{ , };
				\node(start) at (T21) {};
		\end{scope}
	% Version à gauche de zéro.
	%\begin{scope}
	%	\tkzTabInit[lgt=1.2,espcl=2]
	%		{$x$/.6, $1-x$/.6, $f'(x)$/.6, $f(x)$/1.2}
	%		{$-\infty$, 0,, $+\infty$};
	%	\tkzTabLine{,-,1,,,,};
	%	\tkzTabLine{,+,z,,,,};
	%	\tkzTabVar{+/,-/,,,};
	%	\node(onArray) at (T02) {};
	%\end{scope}
	%
	% Version à droite de 1.
			\begin{scope}[xshift=5cm]
			\tkzTabInit[lgt=1.2,espcl=2]
				{$x$/.6, $1-x$/.6, $f'(x)$/.6, $f(x)$/1}
				{$-\infty$, 1, $+\infty$};
			\tkzTabLine{,,z,-,,};
			\tkzTabLine{,,z,,,};
			\tkzTabVar{,,,};
			\node(onArray) at (T01) {};
			\end{scope}
		\draw[->, thick] (start) to [in = 180, out = 0] (onArray) ;
	\end{tikzpicture}
		\caption{Autre exemple: ajouter des étapes intermédiaires de raisonnement dont la preuve par l'outil est immédiate, ce qui évite de passer par la ligne de commande}
		\label{fig:tv:interactions:intermediaire}
	\end{subfigure}
	\caption{Différentes interactions avec l'interface envisagée}
	\label{fig:tv:interactions}
\end{figure}

\end{document}

