
Axiom real : Type.

Axiom nat_to_real : nat -> real.
Coercion nat_to_real : nat >-> real.

Axiom add : real -> real -> real.
Infix "+" := add.

Axiom add_0_l : forall x, 0 + x = x.
Axiom add_0_r : forall x, x + 0 = x.

Axiom sub : real -> real -> real.
Infix "-" := sub.

Axiom sub_0_r : forall x, x - 0 = x.

Axiom mult : real -> real -> real.
Infix "*" := mult.

Axiom mult_0_l : forall x, 0 * x = 0.
Axiom mult_0_r : forall x, x * 0 = 0.
Axiom mult_S_l : forall n x, (S n) * x = n * x + x.
Axiom mult_S_r : forall n x, x * (S n) = n * x + x.

Axiom mult_assoc : forall x y z, x * (y * z) = (x * y) * z.

Lemma mult_1_l : forall x, 1 * x = x.
Proof.
  intro x.
  rewrite mult_S_l.
  rewrite mult_0_l.
  rewrite add_0_l.
  reflexivity.
Qed.

Lemma mult_1_r : forall x, x * 1 = x.
Proof.
  intro x.
  rewrite mult_S_r.
  rewrite mult_0_l.
  rewrite add_0_l.
  reflexivity.
Qed.

Lemma add_fact : forall x, x + x = 2 * x.
Proof.
  intro x.
  rewrite mult_S_l.
  rewrite mult_1_l.
  reflexivity.
Qed.

Axiom mult_distrib : forall a b c, (a + b) * c = a * c + b * c.
Axiom mult_distrib_sub : forall a b c, (a - b) * c = a * c - b * c.

Axiom add_nat : forall a b : nat, a + b = (a + b)%nat.
Axiom mult_nat : forall a b : nat, a * b = (a * b)%nat.

Axiom order : real -> real -> Prop.
Notation "x < y" := (order x y) (at level 70, y at next level).
Notation "x < y < z" := (x < y /\ y < z) (at level 70, y, z at next level).
Notation "x < y < z < aa" := (x < y /\ y < z /\ z < aa) (at level 70, y, z, aa at next level).

Definition croissante_entre f a b :=
  forall x y, a < x < y < b -> f x < f y.

Notation "f 'croissante' 'sur' ] a , b [" :=
  (croissante_entre f a b) (at level 42).

Axiom deriv : (real -> real) -> (real -> real).
Notation "f '’'" := (deriv f) (at level 0).

Axiom deriv_add : forall f g, (fun x => f x + g x)’ = (fun x => f’ x + g’ x).
Axiom deriv_sub : forall f g, (fun x => f x - g x)’ = (fun x => f’ x - g’ x).
Axiom deriv_mult : forall f g, (fun x => f x * g x)’ = (fun x => f’ x * g x + f x * g’ x).
Axiom deriv_const : forall a, (fun x => a)’ = (fun x => 0).
Axiom deriv_id : (fun x => x)’ = (fun x => 1).

Axiom extensionality_real : forall (f g : real -> real), (forall x, f x = g x) -> f = g.

Lemma deriv_scal : forall a,
  (fun x => a * x)’ = (fun x => a).
Proof.
  intro a.
  rewrite deriv_mult.
  rewrite deriv_const.
  rewrite deriv_id.
  apply extensionality_real.
  intro x.
  rewrite mult_0_l.
  rewrite add_0_l.
  rewrite mult_1_r.
  reflexivity.
Qed.

Notation "x ∈ ] a , b [" := (a < x < b) (at level 0).

Axiom deriv_pos : forall f a b,
  (forall x, x ∈ ]a, b[ -> 0 < f’ x) ->
  f croissante sur ]a, b[.

Ltac auto_deriv :=
  repeat first [
      rewrite deriv_sub
    | rewrite deriv_add
    | rewrite deriv_scal
    | rewrite deriv_mult
    | rewrite deriv_id
    | rewrite deriv_const ].

Ltac auto_simpl :=
  repeat first [
        rewrite add_0_l
      | rewrite add_0_r
      | rewrite sub_0_r
      | rewrite mult_0_l
      | rewrite mult_0_r
      | rewrite mult_1_l
      | rewrite mult_1_r
      (*| rewrite add_fact*)
      | rewrite mult_assoc
      | rewrite add_nat
      | rewrite mult_nat ];
  simpl.

Ltac factorise :=
  repeat (
      repeat rewrite <- mult_assoc;
      repeat rewrite <- mult_distrib;
      repeat rewrite <- mult_distrib_sub;
      auto_simpl).







Definition f x := 3*x*x - 2*x*x*x.

Lemma f_croissante_sur_0_1 : f croissante sur ]0, 1[.
Proof.
  apply deriv_pos.
  intros t I.
  unfold f.
  auto_deriv.
  auto_simpl.
  factorise.

Qed.




