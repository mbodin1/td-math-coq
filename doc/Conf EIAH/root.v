
Require Import Reals Lra.

Open Scope R_scope.

Lemma Rge_not_ge_dec : forall a b, { a >= b } + { ~ a >= b }.
Proof.
  intros a b. destruct (Rge_gt_dec a b); [ left | right ]; auto.
  apply Rgt_not_ge; auto.
Qed.

Notation "√ x" := (sqrt x) (at level 0) : R_scope.
Notation "|( x )|" := (Rabs x) : R_scope.

Tactic Notation "rephrase" simple_intropattern(H) "as" constr(P) :=
  let H' := fresh "H" in
  assert (H' : P); [ solve [ auto | lra ] | clear H; rename H' into H ].

Tactic Notation "test" constr(P) :=
  let D := fresh "dec" in
  let H := fresh "H" in
  assert (D : {P} + {~P});
  [ pose Rge_not_ge_dec; solve [ auto | lra ] | destruct D as [H|H] ].

Lemma root : forall x, 5 * √(x²) - 1 = 0 -> x = 1 / 5 \/ x = - 1 / 5.
Proof.
  intros x E.
  Search sqrt.
  rewrite sqrt_Rsqr_abs in E.
  rephrase E as (|( x )| = 1/5).
  test (x >= 0).
  - Search |( _ )|.
    rewrite Rabs_right in E; auto.
  - rewrite Rabs_left in E; lra.
Qed.
