# Compilation

Nécessite l'installation de Pygments.
```bash
sudo apt install python3-pygments
```

Nécessite l'installation de xelatex
```bash
sudo apt install texlive-xetex
```

La compilation a été testée sous XeLaTeX :
```bash
 xelatex -shell-escape Rapport_de_stage.tex
```
puis 
```bash
 pdflatex Rapport_de_stage.tex 
 ```