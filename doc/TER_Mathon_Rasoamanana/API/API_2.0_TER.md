#  API

[**– RÈGLES –	2**](\#–-règles-–)

[**– GLOSSAIRE –	2**](\#–-glossaire-–)

[\- ENTRÉE \-	2](\#--entrée--)

[\- SORTIE \-	2](\#--sortie--)

[\- CODES ERREURS \-	2](\#--codes-erreurs--)

[**– FONCTIONS –	3**](\#–-fonctions-–)

[\- init()	3](\#init())

[\- function\_definition()	4](\#function\_definition())

[\- validate()	4](\#validate())

[\- deriv()	5](\#deriv())

[\- compare\_values()	5](\#compare\_values())

[\- simplify()	6](\#simplify())

[\- develop()	7](\#develop())

[\- interval()	7](\#interval())

[\- suppose\_var()	8](\#suppose\_var())

[\- is\_positive()	8](\#is\_positive())

[\- print\_env()	9](\#print\_env())

[\- defined()	9](\#defined())

[\- suppose()	10](\#suppose())

[**– FONCTIONS NON MAÎTRISÉES –	11**](\#–-fonctions-non-maîtrisées-–)

[\- proof\_assistant()	11](\#proof\_assistant())

[**– FONCTIONS AVEC ERREURS INTERNES –	12**](\#–-fonctions-avec-erreurs-internes-–)

[\- valeur\_interdite()	12](\#valeur\_interdite())

[\- extremum\_local()	12](\#extremum\_local())

[\- signe\_deriv()	13](\#signe\_deriv())

[\- roots()	13](\#roots())

[\- fresh()	14](\#fresh())

[**–  FONCTIONS QUI N’APPARAISSENT PAS –	15**](\#–-fonctions-qui-n’apparaissent-pas-–)

[\- define\_new\_function()	15](\#define\_new\_function())

[\- interesting\_values()	15](\#interesting\_values())

[\- replace()	16](\#replace())

[**– PROPOSITIONS DE FONCTIONS À INCLURE–	16**](\#–-propositions-de-fonctions-à-inclure–)

[\- definition\_domaine(f(x), min, max)	16](\#definition\_domaine(f(x),-min,-max))

[\- Reinit() ou reboot()	16](\#reinit()-ou-reboot())

# – RÈGLES –  {#–-règles-–}

. Toutes les expressions qui passent en argument des fonctions (sauf la fonction *validate*() ) doivent passer par la fonction *validate*() .

# – GLOSSAIRE – {#–-glossaire-–}

## \- ENTRÉE \- {#--entrée--}

…

## \- SORTIE \- {#--sortie--}

. *valid* : Indique l’état de la validation.  
plusieurs états sont à considérer, les voici :   
Si tout est ok : 2  
En cas d’erreur (cf CODE ERREURS)

. *message* : Dépend de *valid*, si *valid* est différent de 2 alors le message indique le type d’erreur.  
Si *valid* est 2 alors cela dépend de la fonction.

## \- CODES ERREURS \- {#--codes-erreurs--}

1.1 : erreur de syntaxe  
(ex : \[1.1, message\] )

1.2 : erreur de typage.  
(ex : \[1.2, message, var\] )

1.3 : variable déjà définie..  
(ex : \[1.3, message, ligne, charac\] )

1.4 : variable déjà initialisée.  
(ex : \[1.4, message\] )

1.5 : erreur inconsistante..  
(ex : \[1.5,message\] )

1.9 : erreur interne.  
(ex : \[1.9,message\] )

# – FONCTIONS – {#–-fonctions-–}

**. Description**  
Décris ce que fait la fonction.  
**. Fonction.s appelée.s**  
Liste des fonctions qu’elle appelle en interne au module.  
**. Fonction.s appelante.s**  
Liste des fonctions qui l’appelle en interne au module.  
**. Fonction avec arguments théorique**  
Appel de la fonction avec des arguments théoriques.  
**. Définition des arguments en entré**  
Définition des arguments théoriques en entrée de la fonction.  
**. Retour théorique**  
Retour sans erreur de la fonction illustrée par des valeurs théoriques.  
**. Définition des valeurs en sortie**  
Définitions des valeurs retournées par la fonction.  
**. Exemple d’utilisation**  
Un exemple de cas pratique sans erreur.  
**. Retour erreur**  
Un exemple de cas pratique avec erreur.

- ## init()  {#init()}

**. Description**  
Permet d’initialiser le module “tableau de variation”.

**. Entrée théorique avec les arguments**  
*init()*

**. Définition des arguments en entré**  
Pas d’argument

**. Retour théorique**  
*→ \[valid, new\_env, message\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*new\_env* : Nouvel environnement  
*message* : "Require Import Tabvar." pour indiquer l’importation correct de TabVar ou retourne "Already initialised" si TabVar a déjà été importé.

**. Cas pratique**  
*init()*  
*→ \[ 2, env1, "Require Import Tabvar." \]*

**. Retour error**  
*init()*  
*→ \[ 1.4, "Already initialised" \]*

- ## function\_definition() {#function_definition()}

**. Description**  
Permet de définir la fonction principale sur laquelle on se base pour remplir le tableau de variation. Permet aussi de définir la variable mise en paramètre (i.e pas besoin de définir la variable de la fonction principal grâce à la fonction suppose\_var() ).

**. Entrée théorique avec les arguments**  
function\_definition( env, f, var, expr)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel.  
*f* : (string) Nom de la fonction.  
*var* : (string) Le nom de l’argument de la fonction.  
Remarque: On ne peut pas utiliser x comme argument car c’est déjà déclaré comme variable globale.  
*expr* : (string) Expression de la fonction.

**. Retour théorique**  
→ \[valid, *new\_env* , expr\]

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*new\_env* : nouvel environnement  
expr : expression qui est maintenant défini

**. Cas pratique**  
function\_definition(env, "f", "t","2\*t\*t-4\*t+2")  
→\[2, Array(6), 'Definition f (t : real) := 2×t×t − 4×t \+ 2.'\]

**. Retour error**  
*function\_definition(0, "f", "x", "3+e")*  
*→ \[ 1.2, "Unknown variable e.", "e" \]*

- ## validate() {#validate()}

**. Description**  
Permet de vérifier si une fonction est correctement écrite et ainsi elle peut être lisible par l’API. 

**. Entrée théorique avec les arguments**  
*validate( env , expr)*

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel.  
*expr* : (string) Expression à valider.

**. Retour théorique**  
*→ \[ valid, message \]*

**. Définition des valeurs en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) retourne l’expression si cette dernière est correcte, sinon retourne un message d’erreur.

**. Exemple illistratif**  
*validate(env, "5\*x+8")*  
*→ \[ 2, "5×x \+ 8" \]*

**. Exemple en cas d’erreur**  
*validate(env, "5x+8")*  
*→ \[ 1.1, "Parser error at line 1, character 3", 1, 3 \]*

- ## deriv() {#deriv()}

**. Description**  
Retourne une dérivé de la fonction mis en argument en fonction d’une variable

**. Entrée théorique avec les arguments**  
*deriv(env, expr, var)*

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel.  
*expr* : (string) Expression à dériver.  
*var* : (string) Variable qui permet de dériver l’expression en fonction d’elle.

**. Retour théorique**  
*→ \[valid, message\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation.  
*message :*  (string) Affiche la dérivé de la fonction *expr* (argument en entrée).

**. Cas pratique**  
*deriv(45,"5\*x+6","x")*  
→ \[ 2, "5" \]

**. Retour error**

- ## compare\_values() {#compare_values()}

**. Description**  
Compare la valeur dans la première chaîne de caractère à la valeur de la seconde chaîne de caractère. Comparer *expr1* à *expr2* en retournant le résultat de comparaison qui peut être “\<”, “\>”, “=” ou “?”.

**. Entrée théorique avec les arguments**  
*compare\_values(env, expr1, expr2)*

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel.  
*expr1* : (string) Expression comparée.  
*expr2* : (string) Expression comparante.

**. Retour théorique**  
*→ \[valid, symb\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*symb* : symbole de comparaison de la première expression par rapport à la deuxième 

**. Cas pratique**  
compare\_values(42, "sqrt(1)+48", "sin 42 \- 2")  
 → \[ 2, "\>" \]

**. Retour error**

- ## simplify() {#simplify()}

**. Description**  
Simplifie une expression en une expression équivalente sur son domaine de définition.

**. Entrée théorique avec les arguments**  
*simplify(env, expr)*

**. Définition des arguments en entré**  
. *env* : environnement actuel  
*expr* : (string) correspond a une valeur ou à une fonction 

**. Retour théorique**  
→ *Array \[ valid, expr \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*expr* : (string) expression simplifiée

**. Cas pratique**  
simplify(42, "log(1)+1")  
→ Array \[ 2, "1" \]

**. Retour error**  
si on rentre x²/x il renvoie x alors que la fonction d’entrée n’est pas définie en 0 alors que x oui. simplify peut définir alors que la fonction de base ne peut pas l'être

- ## develop() {#develop()}

**. Description**  
Développer une expression arithmétique en une expression équivalente.

**. Entrée théorique avec les arguments**  
*develop(env, expr)*

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel.  
*expr* : (string) Expression à traiter.

**. Retour théorique**  
→ *Array \[ valid, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) expression développée

**. Cas pratique**  
develop(42, "((a+b)\*c)\*d")  
→ Array \[ 2, "a×c×d \+ b×c×d" \]

**. Retour error**

- ## interval()  {#interval()}

**. Description**  
Détermine (une sur-approximation de) l'intervalle d'une expression.

**. Entrée théorique avec les arguments**  
interval(env, expr)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*expr :* (string) Expression à traiter

**. Retour théorique**  
*→ Array \[ valid, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) intervalle dans laquelle l’expression est définie

**. Cas pratique**  
interval(env,"15\*x-8")  
*→*Array \[2,  ' \]–∞ ; \+∞ \[' \]

**. Retour error**

- ## suppose\_var()  {#suppose_var()}

**. Description**  
Déclare une variable.

**. Entrée théorique avec les arguments**  
suppose\_var(env, x)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*x :* (string) Variable à déclarer

**. Retour théorique**  
*→ Array \[ valid, new\_env, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*new\_env* : Nouvel environnement  
*message* : (string) indique que la variable *x* (argument en entrée) est bien un réel.

**. Cas pratique**  
suppose\_var(env, "5")  
*→ Array\[2, Array(6), 'Variable 5 : real.'\]* 

**. Retour error**

- ## is\_positive() {#is_positive()}

**. Description**  
Indique si le signe d'une expression est positif (zéro étant considéré positif).

**. Entrée théorique avec les arguments**  
is\_positive(env, expr)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*expr :* (string) expression à traiter

**. Retour théorique**  
*→ Array \[ valid, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche true si l’expression e (en entrée) est positive, false si c’est négatif et “?” si on ne sait pas.  

**. Cas pratique**  
is\_positive(env, "-9+5")  
*→ Array\[2, false'\]*

**. Retour error**

- ## print\_env()  {#print_env()}

**. Description**  
Affiche l'environnement actuel (pour aider le débogage).

**. Entrée théorique avec les arguments**  
print\_env(env)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel

**. Retour théorique**  
*→ Array \[ valid, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche tous les éléments de l'environnement.

**. Cas pratique**  
print\_env(env)  
*→ Array\[2, 'x : real (\]–∞ ; \+∞\[) ; '\]*

**. Retour error**

- ## defined()  {#defined()}

**. Description**  
Indique si une expression est bien définie (y) dans l'environnement, si elle n'est pas définie (n), ou si on ne sait pas (?).

**. Entrée théorique avec les arguments**  
defined(env, expr)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*expr :* (string) Expression à traiter

**. Retour théorique**  
*→ Array \[ valid, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche “y” si l’expression est définie, “n” si elle n’y est pas et “?” si on ne     sait pas.

**. Cas pratique**  
defined(env, "12\*x\*x-6\*x+15")  
*→ Array\[2, 'y'\]*

**. Retour error**

- ## suppose()  {#suppose()}

**. Description**  
Suppose une propriété comme étant vraie dans l'environnement.

**. Entrée théorique avec les arguments**  
suppose(env, prop)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*prop :* (string) propriété

**. Retour théorique**  
*→ Array \[ valid, new\_env, message \]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*new\_env* : Nouvel environnement  
*message* : (string) affiche la propriété ou l'hypothèse.

**. Cas pratique**  
suppose(env, "x \= 5")  
*→ Array\[2, Array(6), 'Hypothesis H : x \= 5.'\]*

**. Retour error** 

# – FONCTIONS NON MAÎTRISÉES –  {#–-fonctions-non-maîtrisées-–}

- ## proof\_assistant()  {#proof_assistant()}

**. Description**  
Exécute des commandes comme si on les avait données à l'assistant de preuve.

**. Entrée théorique avec les arguments**  
proof\_assistant(env, cmd)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*cmd :* (string) commande d’assistant de preuve à traiter 

**. Retour théorique**  
***→**Array\[valid, new\_env,  mess\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*new\_env* : Nouvel environnement  
*mess*

**. Cas pratique**  
tabvar.proof\_assistant (env, "Variable y : real. Hypothesis (H1 : y \> 3\) (H2 : y \< 4). Lemma prop : (y \> 2). Proof. Qed.")

→Array\[2, Array(6), Array(4)\]

**. Retour error**

# – FONCTIONS AVEC ERREURS INTERNES –  {#–-fonctions-avec-erreurs-internes-–}

- ## valeur\_interdite()  {#valeur_interdite()}

**. Description**  
Indique si une valeur définie est une valeur interdite de la fonction.

**. Entrée théorique avec les arguments**  
valeur\_interdite(env, v, f)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*v :* (string) valeur à vérifier  
*f :* (string) fonction à traiter

**. Retour théorique**  
*→ Array\[valid, message\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche true si la valeur v (en entrée) est une valeur interdite, false si ça n’en est pas une et “?” si on ne sait pas.  

**. Cas pratique**

**. Retour error**

- ## extremum\_local()  {#extremum_local()}

**. Description**  
Indique si une valeur définie est un extremum local de la fonction.

**. Entrée théorique avec les arguments**  
extremum\_local(env, v, f)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*v :* (string) valeur à vérifier  
*f :* (string) fonction à traiter

**. Retour théorique**  
*→ Array\[valid, message\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche true si l’expression e (en entrée) est un extremum local, false si ça n’en est pas un et “?” si on ne sait pas. 

**. Cas pratique**  
**. Retour error**

- ## signe\_deriv()  {#signe_deriv()}

**. Description**  
Indique le signe de la dérivée de la fonction à une valeur définie.

**. Entrée théorique avec les arguments**  
signe\_deriv(env, v, f)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
 *v :* (string) valeur à vérifier  
 *f :* (string) dérivée de la fonction à traiter

**. Retour théorique**  
*→ Array\[valid, message\]*

**. Définition des arguments en sortie**  
*valid* : indique l’état de la validation  
*message* : (string) affiche “+” si positif, “-” si négatif, “?” si on ne sait pas. 

**. Cas pratique**

**. Retour error**

- ## roots()  {#roots()}

**. Description**  
Essaye de trouver des valeurs qui annulent l'expression.

**. Entrée théorique avec les arguments**  
roots(env, x, e)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*x :* (string) valeur à vérifier  
*e :* (string) expression à traiter

**. Retour théorique**  
**. Définition des arguments en sortie**  
**. Cas pratique**  
**. Retour error**

- ## fresh()  {#fresh()}

**. Description**  
Renvoie une variable fraîche dans l'environnement.

**. Entrée théorique avec les arguments**  
. fresh(env, x)

**. Définition des arguments en entré**  
. *env :* correspond à l'environnement actuel  
  *x :* (string) Variable à déclarer

**. Retour théorique**  
**. Définition des arguments en sortie**  
**. Cas pratique**  
**. Retour error**

# –  FONCTIONS QUI N’APPARAISSENT PAS – {#–-fonctions-qui-n’apparaissent-pas-–}

- ## define\_new\_function()  {#define_new_function()}

(manquant)  
**. Description**  
Définit une nouvelle fonction à ajouter à tabvar lors de l'initialisation.

**. Entrée théorique avec les arguments**  
define\_new\_function(f, expr)

**. Définition des arguments en entré**  
*f :* nom de la fonction  
*expr :* (string) expression de la fonction

**. Retour théorique**  
**. Définition des arguments en sortie**  
**. Cas pratique**  
**. Retour error**

- ## interesting\_values()  {#interesting_values()}

(manquant)  
**. Description**  
Renvoie des valeurs potentiellement intéressantes (par exemple des valeurs interdites ou des points d'annulation).

**. Entrée théorique avec les arguments**  
intersting\_values(env, x, expr)

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*x :*   
*expr :* (string) Expression à traiter

**. Retour théorique**  
**. Définition des arguments en sortie**  
**. Cas pratique**  
**. Retour error**

- ## replace()  {#replace()}

(manquant)  
**. Description**  
Remplace une variable par une autre dans l'expression.

**. Entrée théorique avec les arguments**  
*replace(env, expr, x, ex)*

**. Définition des arguments en entré**  
*env :* correspond à l'environnement actuel  
*expr :* (string) Expression à traiter  
*x :* (string) Variable à remplacer  
*ex :* (string) Nouvelle variable remplaçant la précédente

**. Retour théorique**  
**. Définition des arguments en sortie en sortie**  
**. Cas pratique**  
**. Retour error**

# – PROPOSITIONS DE FONCTIONS À INCLURE– {#–-propositions-de-fonctions-à-inclure–}

- ## definition\_domaine(f(x), min, max)  {#definition_domaine(f(x),-min,-max)}


- ## Reinit() ou reboot()  {#reinit()-ou-reboot()}

réinitialisation de l’espace de travail  
