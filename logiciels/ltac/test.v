
(** Quelques tactiques pour tester l'interaction avec Jscoq. *)

(** Les tactiques en Coq sont séparées en deux types : celles qui manipulent l'environnement et celles qui créé des termes. *)

(** * Manipulation de l'environnement. *)

(** Ne fait aucun changement. *)
Ltac test_idtac := idtac.

(** Ne fait aucun changement, mais affiche un message. *)
Ltac test_message := idtac "message de test".

Require Import List.
Import ListNotations.

(** Pareil, mais le message est un terme. *)
Ltac test_affichage_terme :=
  let l := constr:([1;2]) in
  idtac l.

(** Une tactique qui échoue. *)
Ltac test_echec := fail.

(** Une tactique qui échoue avec un message d'erreur. *)
Ltac test_echec_message := fail "erreur".

(** Exemple d'utilisation. *)
Lemma example : True.
  test_message.
Abort.

(** * Construction de termes. *)

(** Une tactique qui renvoit un terme. *)
Ltac test_terme := constr:(Some 2).

(** Une tactique qui renvoit une fonction. *)
Ltac test_function := constr:(fun x => 2*x).

(** Exemple d'utilisation. *)
Definition example_terme :=
  ltac:(let t := test_function in exact t).
