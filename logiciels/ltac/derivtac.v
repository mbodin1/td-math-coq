(** Tactics to handle derivation of simple functions. *)

(* Trying with the standard library. *)
From Coq Require Import Reals Ranalysis Lra FunctionalExtensionality.
Open Scope R_scope.
  
Ltac show_derivable :=
  repeat first [
      apply derivable_plus
    | apply derivable_minus
    | apply derivable_mult
    | apply derivable_div
    | apply derivable_opp
    | apply derivable_scal
    | apply derivable_inv
    | apply derivable_comp
    | lazymatch goal with
      | |- derivable (fun x : R => ?u (@?v x)) =>
        apply derivable_comp with (f2 := u) (f1 := v)
      | |- derivable ?f => unfold f
      end ];
  solve [
      apply derivable_const
    | apply derivable_id
    | apply derivable_pow
    | apply derivable_cos
    | apply derivable_sin
    | apply derivable_sinh
    | apply derivable_cosh
    | apply derivable_exp
    | auto; lra ].

Definition test x := cos (x*x).

(* The provided function is derivable in all of R. *)
Lemma derivable_test : derivable test.
Proof. show_derivable. Defined.

Definition test' :=
  derive test derivable_test.

Eval compute in test'. (* = proj1_sig derivable_square, not super nice. *)

Record is_derivative f f' : Type := build_is_derivative {
    is_derivative_derivable : derivable f ;
    is_derivative_correct : f' = derive f is_derivative_derivable
  }.

(* Remove multiplications by 1, 0, double opposites, etc. *)
Ltac simplify_trivial f :=
  let opp f :=
    lazymatch f with
    | fun x : R => - @?u x => constr:(u)
    | _ => constr:(fun x : R => - f x)
    end in
  let f :=
    lazymatch f with
    | fun x : R => - @?u x =>
      let u := simplify_trivial u in
      opp u
    | fun x : R => @?u x + @?v x =>
      let u := simplify_trivial u in
      let v := simplify_trivial v in
      let check u v k :=
        lazymatch u with
        | fun _ : R => 0 => constr:(v)
        | fun _ : R => -0 => constr:(v)
        | _ => k
        end in
      check u v ltac:(check v u constr:(fun x : R => u x + v x))
    | fun x : R => @?u x * @?v x =>
      let u := simplify_trivial u in
      let v := simplify_trivial v in
      let check u v k :=
        lazymatch u with
        | fun _ : R => 0 => constr:(fun x : R => 0)
        | fun _ : R => -0 => constr:(fun x : R => 0)
        | fun _ : R => 1 => constr:(v)
        | fun _ : R => -1 => opp v
        | fun _ : R => -(1) => opp v
        | _ => k
        end in
      check u v ltac:(check v u constr:(fun x : R => u x * v x))
    | _ => constr:(f)
    end in
  let f := eval cbv beta in f in
  constr:(f).

(** Simplify the expression of a function. *)
Ltac simplify_function f :=
  let f := eval cbv beta in f in
  let f := simplify_trivial f in
  constr:(f).

(* For now, we voluntarily elide cases whose domain is not the whole real set,
  like division, logarithms, etc. *)

(** Given the expression of a function, return the expression of its derivative, or fail. *)
Ltac build_derivative f :=
  let t :=
    lazymatch f with
    (* Constants *)
    | fun _ : R => ?c => constr:(fun _ : R => 0 : R)
    (* Identity *)
    | fun x : R => x => constr:(fun _ : R => 1 : R)
    (* Addition *)
    | fun x : R => @?u x + @?v x =>
        let u' := build_derivative u in
        let v' := build_derivative v in
        constr:(fun x : R => u' x + v' x)
    (* Soustraction *)
    | fun x : R => @?u x - @?v x =>
        let u' := build_derivative u in
        let v' := build_derivative v in
        constr:(fun x : R => u' x - v' x)
    (* Multiplication *)
    | fun x : R => @?u x * @?v x =>
        let u' := build_derivative u in
        let v' := build_derivative v in
        constr:(fun x : R => u' x * v x + u x * v' x)
    (* Multiplication *)
    | fun x : R => @?u x * @?v x =>
        let u' := build_derivative u in
        let v' := build_derivative v in
        constr:(fun x : R => u' x * v x + u x * v' x)
    (* Cosine *)
    | fun x : R => cos (@?u x) =>
        let u' := build_derivative u in
        constr:(fun x : R => - u' x * sin x)
    (* Sine *)
    | fun x : R => sin (@?u x) =>
        let u' := build_derivative u in
        constr:(fun x : R => u' x * cos x)
    (* Exponential *)
    | fun x : R => exp (@?u x) =>
        let u' := build_derivative u in
        constr:(fun x : R => u' x * exp x)
    (* Hyperbolic cosine *)
    | fun x : R => cosh (@?u x) =>
        let u' := build_derivative u in
        constr:(fun x : R => u' x * sinh x)
    (* Hyperbolic sine *)
    | fun x : R => sinh (@?u x) =>
        let u' := build_derivative u in
        constr:(fun x : R => u' x * cosh x)
    (* Default case *)
    | fun x : R => @?c x => fail "Don't know how to handle such an expression."
    end in
  let t := simplify_function t in
  t.

Goal True.
  let f' := build_derivative (fun y : R => cos (y*y)) in
  pose (g' := f').
Abort.

Ltac build_goal_is_derivative f :=
  let f' := build_derivative f in
  constr:(is_derivative f f').

Section Derive_Lemma.
Transparent derivable_cos.
Lemma derive_cos : forall x, derive cos derivable_cos x = - sin x.
Proof.
  intro x. unfold derivable_cos. reflexivity.
Qed.

Goal ltac:(let g := build_goal_is_derivative (fun y : R => cos (y*y)) in exact g).
  refine (build_is_derivative (fun y : R => cos (y*y)) _ ltac:(show_derivable) _).
  apply functional_extensionality.
  intro x.
  simpl.
  Fail reflexivity.
Abort.


(* Triying with Coquelicot *)
From Coquelicot Require Import Coquelicot. (* Requires: opam install coq-coquelicot *)

(* If [f] is a real function, then [Derive f] is the derivative function (at least at
  the points where it is defined). *)
Check Derive.

(* is_derive and ex_derive are just other names for the same concept in more general structures. *)

(* is_derive_Reals: links with derivable_pt_lim *)

(* Some lemmas that could be useful. *)
Check Derive_const.
Check Derive_id.
