
Contenu du dossier :
- [jscoq_vm](jscoq_vm/) Un pointeur vers la version de JsCoq que nous avons étudié.
- [jscoq](jscoq/) Une tentative de modification de JsCoq pour créer un paquet esy, afin de simplifier sa compilation.
- [ocaml-tabvar](ocaml-tabvar/) Un prototype d'interface web en OCaml.
	+ Le but était de tracer des tableaux de variation directement en OCaml.
	+ A été globalement abandonné (date d'avant le stage).
	+ Sert maintenant à simuler un Coq simplifié pour le prototype du projet TER.
- [ltac](ltac/) Des tactiques Ltac afin de manipuler des tableaux de variations.
	+ À s'inspirer pour communiquer avec Coq.
- [tabvar_TER](tabvar_TER/Prototype_tabvar_avec_API/) Une interface mélant du code Coq avec un tableau de variation graphique.

