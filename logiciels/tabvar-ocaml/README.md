
# Description

Prototype pour une interface pour manipuler des tableaux de variations.

# Compilation

La méthode préférée de compilation est l'utilisation d'`esy`.
Dans les deux cas, il sera nécessaire d'avoir les programmes `gmp` et `cc` installés sur le système.

## Avec `esy`

```bash
# Installation d'esy
npm install esy
# Installation des dépendances et compilation
esy
```

## Avec `opam`

```bash
opam install dune odoc bignum ppx_inline_test lwt_ppx js_of_ocaml js_of_ocaml-ppx js_of_ocaml-lwt tyxml tyxml-ppx js_of_ocaml-tyxml
dune build
```

