
(* Un type pour les variables. *)
type var = string

(* Fonctions *)
type fexpr =
  | Sqrt (* Racine carrée *)
  | Ln | Log (* Log (base e ou 10) *)
  | Sin | Cos | Tan (* Fonctions trigonométriques. *)
  | Asin | Acos | Atan (* Fonctions trigonométriques réciproques. *)
  | Sinh | Cosh | Tanh (* Fonctions trigonométriques hyperboliques. *)
  | Asinh | Acosh | Atanh (* Fonctions trigonométriques hyperboliques réciproques. *)
  | Local of var (* Une fonction définie localement. *)

(* Un type pour représenter les valeurs écrites dans le tableau, telles qu'elles
  sont écrites.
  Ce type stocke aussi comment l'afficher. Notamment pour la somme et le produit,
  on aurait pu choisir de représenter *)
type expr =
  | Const of Tenadic.t (* Une valeur numérique constante *)
  | Infinity of bool (* Si vrai, infini positif, si faux, négatif. *)
  | Paren of expr (* Une expression avec des parenthèses. *)
  | Sum of (bool * expr) list (* Somme ou soustraction, en fonction du booléen de chaque expression. Par exemple l'opposé est juste Sum [(false, e)] *)
  | Prod of (bool * expr) list (* Produit. Comme pour l'addition, un booléen positif est une multiplication, négatif une division. *)
  | Pow of expr * expr (* Passage à l'exposant *)
  | Var of var (* Une variable locale. *)
  | Call of fexpr * expr list (* Appel de fonction. *)

