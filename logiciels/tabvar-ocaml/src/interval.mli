
(* An interval defined as minimum and maximum values.
  None means infinity, the boolean states whether the number is included within the interval. *)
type bound = (Tenadic.t * bool) option
type t = bound * bound

(* Union and intersection of intervals. *)
val inter : t -> t -> t
val union : t -> t -> t

(* State whether two intervals are the same. *)
val eq : t -> t -> bool

(* Minimum of two interval bounds, the boolean being the sign of None (true meaning positive). *)
val min_bound : bool -> bound -> bound -> bound

(* Maximum of two interval bounds, the boolean being the sign of None. *)
val max_bound : bool -> bound -> bound -> bound

(* Intervals are meant to be with their lower bound smaller than their upper bound.
  In cases where this could break, this function should normalise empty intervals to ]0, 0[. *)
val normalise : t -> t

(* State whether the interval is the empty set. *)
val is_empty : t -> bool

(* State whether a given number is included in the interval. *)
val contains : t -> Tenadic.t -> bool

(* Interval arithmetic.
  The following functions always return an over-approximation of the result of the
  base operation if starting from the provided interval. *)

(* Opposite *)
val opp : t -> t

(* Addition *)
val add : t -> t -> t

(* Multiplication *)
val mult : t -> t -> t

(* Division *)
val div : t -> t -> t

