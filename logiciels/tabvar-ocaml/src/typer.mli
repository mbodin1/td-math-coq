
(* Typecheck an expression.
  It currently returns the same value, but could in the future return an annotated term. *)
val check_expr : Env.t -> Value.expr -> Value.expr

(* Typecheck the expressions within a property. *)
val check_prop : Env.t -> ProofAssistant.property -> ProofAssistant.property

(* Typecheck an expression. *)
val typecheck : Env.t -> Value.expr -> Type.t

(* An error happenned during the typing.
  The first string is the message, the second the identifier or sub-expression at fault. *)
exception Error of string * string

