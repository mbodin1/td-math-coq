
type bound = (Tenadic.t * bool) option
type t = bound * bound


(* Minimum of two interval bounds, inf being the sign of None. *)
let min_bound inf m1 m2 =
  match inf, m1, m2 with
  | true, None, m | true, m, None -> m
  | false, None, _ | false, _, None -> None
  | _, Some (v1, included1), Some (v2, included2) ->
    let d = Tenadic.sub v1 v2 in
    if Tenadic.is_zero d then Some (v1, included1 || included2)
    else if Tenadic.sign d then m2 else m1

(* Maximum of two interval bounds, inf being the sign of None. *)
let max_bound inf m1 m2 =
  match inf, m1, m2 with
  | true, None, _ | true, _, None -> None
  | false, None, m | false, m, None -> m
  | _, Some (v1, included1), Some (v2, included2) ->
    let d = Tenadic.sub v1 v2 in
    if Tenadic.is_zero d then Some (v1, included1 || included2)
    else if Tenadic.sign d then m1 else m2

let is_empty = function
  | (Some (v1, included1), Some (v2, included2)) ->
    let d = Tenadic.sub v1 v2 in
    if Tenadic.is_zero d then (not included1 || not included2)
    else Tenadic.sign d
  | _ -> false

let normalise i =
  if is_empty i then
    (Some (Tenadic.zero, false), Some (Tenadic.zero, false))
  else i

let inter (min1, max1) (min2, max2) =
  normalise (max_bound false min1 min2, min_bound true max1 max2)

let union (min1, max1) (min2, max2) =
  (min_bound false min1 min2, max_bound true max1 max2)

let contains (min, max) n =
  let ok_with order = function
    | None -> true
    | Some (v, included) ->
      if Tenadic.eq n v then included
      else order n v in
  ok_with Tenadic.(fun n v -> sign (sub n v)) min
  && ok_with Tenadic.(fun n v -> sign (sub v n)) max

let eq (min1, max1) (min2, max2) =
  let compare m1 m2 =
    match m1, m2 with
    | None, None -> true
    | Some (v1, included1), Some (v2, included2) ->
      included1 = included2 && Tenadic.eq v1 v2
    | _, _ -> false in
  (is_empty (min1, max1) && is_empty (min2, max2))
  || (compare min1 min2 && compare max1 max2)


let opp (imin, imax) =
  let opp = function
    | None -> None
    | Some (v, included) ->
      Some (Tenadic.opp v, included) in
  (opp imax, opp imin)

let add (imin, imax) (imin', imax') =
  let merge m m' =
    match m, m' with
    | None, _ | _, None -> None
    | Some (v1, incl1), Some (v2, incl2) ->
      Some (Tenadic.add v1 v2, incl1 && incl2) in
  (merge imin imin', merge imax imax')

(* The following functions are interval definitions for mult and div. *)

(* Sign of a bound.
  What to return in case of infinite is given in argument. *)
let sign inf = function
  | None -> inf
  | Some (v, _included) -> Tenadic.sign v

(* Closest bound to zero. *)
let closest m1 m2 =
  match m1, m2 with
  | Some (v1, _included1), Some (v2, _included2) ->
    if Tenadic.(sign (sub (abs v1) (abs v2))) then m2 else m1
  | None, m | m, None -> m

(* Bound farther away to zero. *)
let away m1 m2 =
  match m1, m2 with
  | Some (v1, _included1), Some (v2, _included2) ->
    if Tenadic.(sign (sub (abs v1) (abs v2))) then m1 else m2
  | None, _ | _, None -> None

(* Multiplication on reals extended with infinity. *)
let multiply m1 m2 =
  match m1, m2 with
  | None, _ | _, None -> None
  | Some (v1, included1), Some (v2, included2) ->
    Some (Tenadic.mult v1 v2, included1 && included2)

(* Division on reals extended with infinity.
  The direction indicates which direction to round. *)
let divide dir m1 m2 =
  match m1, m2 with
  | None, _ -> None
  | _, None -> Some (Tenadic.zero, true)
  | _, Some (v, _included) when Tenadic.is_zero v -> None
  | Some (v1, included1), Some (v2, included2) ->
    let v =
      let open Tenadic in
      let open Bigint in
      assert (v2.mantisse <> zero) ;
      let m = v1.mantisse / v2.mantisse in
      let mantisse =
        if rem v1.mantisse v2.mantisse = zero then m
        else
          match dir, Tenadic.sign v2 with
          | true, true | false, false -> m + of_int 1
          | _, _ -> m - of_int 1 in
      { mantisse ; exponent = v1.exponent - v2.exponent } in
    Some (v, included1 && included2)

(* Apply an operation to the combination of bounds of two interval. *)
let extremums op (min1, max1) (min2, max2) =
  [op min1 min2; op min1 max2; op max1 min2; op max1 max2]

let mult (imin, imax) (imin', imax') =
  match sign false imin, sign true imax, sign false imin', sign true imax' with
  | true, false, _, _ | _, _, true, false -> assert false
  | true, true, true, true -> (multiply imin imin', multiply imax imax')
  | true, true, false, false
  | false, false, true, true ->
    (multiply (closest imin imax) (closest imin' imax'),
     multiply (away imin imax) (away imin' imax'))
  | false, false, false, false -> (multiply imax imax', multiply imin imin')
  | true, true, false, true -> (multiply imax imin', multiply imax imax')
  | false, true, true, true -> (multiply imin imax', multiply imax imax')
  | _, _, _, _ ->
    let l = extremums multiply (imin, imax) (imin', imax') in
    let z = Some (Tenadic.zero, true) in
    (List.fold_left (min_bound false) z l,
     List.fold_left (max_bound true) z l)

let div (imin, imax) (imin', imax') =
  match sign false imin, sign true imax, sign false imin', sign true imax' with
  | true, false, _, _ | _, _, true, false -> assert false
  | _, _, false, true -> (None, None) (* Possible division by zero. *)
  | false, true, _, _ ->
    let li = extremums (divide false) (imin, imax) (imin', imax') in
    let la = extremums (divide true) (imin, imax) (imin', imax') in
    let z = Some (Tenadic.zero, true) in
    (List.fold_left (min_bound false) z li,
     List.fold_left (max_bound true) z la)
  | true, true, true, true
  | false, false, false, false ->
    normalise
      (divide false (closest imin imax) (away imin' imax'),
       divide true (away imin imax) (closest imin' imax'))
  | false, false, true, true
  | true, true, false, false ->
    normalise
      (divide false (away imin imax) (closest imin' imax'),
       divide true (closest imin imax) (away imin' imax'))

