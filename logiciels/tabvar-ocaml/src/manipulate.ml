
open Value


let rec interval env =
  let even_power e n =
    assert (n >= 0 && n mod 2 = 0) ;
    let i = interval env e in
    let to_pos = Option.map (fun (v, i) -> (Tenadic.abs v, i)) in
    if Interval.contains i Tenadic.zero then (
      let (mi, ma) = i in
      let mi = to_pos mi in
      let m = Interval.max_bound true mi ma in
      let m = Option.map (fun (v, i) -> (Tenadic.power_int v n, i)) m in
      (Some (Tenadic.zero, true), m)
    ) else (
      let (mi, ma) = i in
      let mi = to_pos mi in
      let ma = to_pos ma in
      let mi' = Interval.min_bound true mi ma in
      let ma' = Interval.max_bound true mi ma in
      (mi', ma')
    ) in
  let open Value in function
  | Const n -> (Some (n, true), Some (n, true))
  | Infinity true -> (Some (Tenadic.large, false), None)
  | Infinity false ->
    (None, Some (Tenadic.opp Tenadic.large, false))
  | Paren e -> interval env e
  | Sum es ->
    List.fold_left (fun i (s, e) ->
      let i' =
        let i' = interval env e in
        if s then i' else Interval.opp i' in
      Interval.add i i') (interval env (Const Tenadic.zero)) es
  | Prod [(true, e1); (true, e2)] when e1 = e2 -> even_power e1 2
  | Prod es ->
    List.fold_left (fun i (b, e) ->
        let i' = interval env e in
        if b then Interval.mult i i' else Interval.div i i')
      (interval env (Const Tenadic.one)) es
  | Pow (e, Const n) when Tenadic.even n && Tenadic.to_int n <> None ->
    (match Tenadic.to_int n with
     | None -> assert false
     | Some n -> even_power e n)
  | Pow (e, Const n) when
      let nm = Tenadic.(sub n one) in Tenadic.(even nm) && Tenadic.to_int nm <> None ->
    (match Tenadic.(to_int (sub n one)) with
     | None -> assert false
     | Some nm -> Interval.mult (interval env e) (even_power e nm))
  | Pow (e, Const n) when Tenadic.even n ->
    ignore e (* This is not great, but hopefully the cases above will trigger more often. *) ;
    (Some (Tenadic.zero, true), None)
  | Pow (e1, e2) ->
    ignore (e1, e2) (* Not much to be done here… *) ;
    (None, None)
  | Var x -> Env.get_interval env x
  | Call (f, args) ->
    let args = List.map (interval env) args in
    match f, args with
    | Sqrt, [(min, max)] ->
      let min =
        match min with
        | None -> (Tenadic.zero, true)
        | Some (v, _) when Bigint.(v.mantisse <= zero) -> (Tenadic.zero, true)
        | Some (v, included) ->
          let v =
            let open Bigint in
            if v.exponent < zero then None
            else Some (v.mantisse * pow (of_int 10) v.exponent) in
          match Option.bind v Bigint.to_int with
          | None ->
            (* Maximum square roots in integers. *)
            let max_sq = Float.to_int (sqrt (Float.of_int max_int)) - 1 in
            (* Just checking that there are no issues with squaring it. *)
            assert (max_sq * max_sq > 0) ;
            (Bigint.{ mantisse = of_int max_sq ; exponent = zero }, false)
          | Some v ->
            let sq = Float.to_int (sqrt (Float.of_int v)) in
            let m = Bigint.{ Tenadic.mantisse = of_int sq ; Tenadic.exponent = zero } in
            (m, included && sq * sq = v) in
      let max =
        match max with
        | None -> None
        | Some (v, _) when Bigint.(v.mantisse <= zero) -> Some (Tenadic.zero, true)
        | Some (v, included) ->
          let v =
            let open Bigint in
            if v.exponent < zero then None
            else Some (v.mantisse * pow (of_int 10) v.exponent) in
          match Option.bind v Bigint.to_int with
          | None -> None
          | Some v ->
            let sq = Float.to_int (sqrt (Float.of_int v)) in
            if sq * sq = v then
              Some (Bigint.{ Tenadic.mantisse = of_int sq ; Tenadic.exponent = zero }, included)
            else
              Some (Bigint.{ Tenadic.mantisse = of_int sq + of_int 1 ; Tenadic.exponent = zero }, false) in
      (Some min, max)
    | (Sin | Cos), _ ->
      (Some (Tenadic.opp Tenadic.one, true),
       Some (Tenadic.one, true))
    | Atan, _ ->
      (Some (Bigint.{ Tenadic.mantisse = of_int (-2); Tenadic.exponent = zero}, false),
       Some (Bigint.{ Tenadic.mantisse = of_int 2; Tenadic.exponent = zero}, false))
    | Atanh, _ ->
      (Some (Tenadic.opp Tenadic.one, false),
       Some (Tenadic.one, false))
    | _f, _args -> (None, None)


(* Greatest common divisor of two big integers. *)
let rec gcd a b =
  let open Bigint in
  if a = zero then b else gcd (rem b a) a

(* Given a list of bool * 'a, remove pairs of (true, x); (false, x) within the list. *)
let annihil_pairs l =
  let l =
    List.sort (fun (b1, x1) (b2, x2) ->
      match compare x1 x2 with
      | 0 -> -compare b1 b2
      | r -> -r) l in
  let rec aux acc = function
    | [] -> acc
    | (b, x) :: l ->
      match acc with
      | (b', x') :: acc' when x = x' && b <> b' -> aux acc' l
      | _ -> aux ((b, x) :: acc) l in
  aux [] l

let rec unfold env = function
  | (Const _ | Infinity _) as e -> e
  | Paren e -> Paren (unfold env e)
  | Sum es -> Sum (List.map (fun (s, e) -> (s, unfold env e)) es)
  | Prod es -> Prod (List.map (fun (s, e) -> (s, unfold env e)) es) 
  | Pow (e1, e2) -> Pow (unfold env e1, unfold env e2)
  | Var x ->
    (match Env.get_value env x with
     | None -> Var x
     | Some ([], e) -> e
     | Some (_args, _e) ->
       (* This variable is a function, but it is not applied. *)
       Var x)
  | Call (f, args) ->
    let args = List.map (unfold env) args in
    match f with
    | Local fn ->
      (match Env.get_value env fn with
       | None -> Call (f, args)
       | Some (xs, e) ->
         if List.length xs = List.length args then
           (* This is a bit dirty.
             A proper way to do it would be to store the initial environment of function
             within the environment.
             It should be fine for the kind of functions we manipulate and the constraints
             on the environment (can't redefine a variable, even a local one). *)
           let env' =
             let open Env in
             List.fold_left2 (fun env x a ->
               match define env x (Type.from_arity 0) (Some ([], a)) with
               | None -> assert false
               | Some env -> env) (create ()) xs args in
           unfold env (unfold env' e)
         else Call (f, args))
    | _ -> Call (f, args)

let rec simpl =
  let expr_is_zero = function
    | Const n -> Tenadic.is_zero n
    | _ -> false in
  let expr_is_one = function
    | Const n -> Tenadic.is_one n
    | _ -> false in function
  | Const n -> Const (Tenadic.normalise n)
  | Infinity s -> Infinity s
  | Paren e -> simpl e
  | Sum es ->
    let es = List.map (fun (s, e) -> (s, simpl e)) es in
    let es =
      List.concat_map (function
        | (true, Sum es) -> es
        | (false, Sum es) -> List.map (fun (s, e) -> (not s, e)) es
        | (s, e) -> [(s, e)]) es in
    let (es, cs) =
      List.partition_map (function
        | (true, Const n) -> Either.Right n
        | (false, Const n) -> Either.Right (Tenadic.opp n)
        | (s, e) -> Either.Left (s, e)) es in
    let c = List.fold_left Tenadic.add Tenadic.zero cs in
    let es = annihil_pairs es in
    if es = [] then Const c
    else (
      if Tenadic.is_zero c then (
        match es with
        | [] -> assert false
        | [(true, e)] -> e
        | es -> Sum es
      ) else Sum ((true, Const c) :: es)
    )
  | Prod es ->
    let es = List.map (fun (b, e) -> (b, simpl e)) es in
    let es =
      List.concat_map (function
        | (true, Prod es) -> es
        | (false, Prod es) -> List.map (fun (b, e) -> (not b, e)) es
        | (b, e) -> [(b, e)]) es in
    let (es, cs) =
      List.partition_map (function
        | (true, Const n) -> Either.Right (Either.Left n)
        | (false, Const n) -> Either.Right (Either.Right n)
        | (b, e) -> Either.Left (b, e)) es in
    let (cs1, cs2) = List.partition_map (fun lr -> lr) cs in
    let c1 = List.fold_left Tenadic.mult Tenadic.one cs1 in
    let c2 = List.fold_left Tenadic.mult Tenadic.one cs2 in
    (* The original expression is morally equivalent to [es * c1 / c2]. *)
    let (c1, c2) =
      let open Tenadic in
      let open Bigint in
      let exp_pow c =
        if c.exponent >= zero then {
          mantisse = c.mantisse * pow (of_int 10) c.exponent ;
          exponent = zero
        } else c in
      let c1, c2 = exp_pow c1, exp_pow c2 in
      let c = gcd c1.mantisse c2.mantisse in
      let c = if c = zero then of_int 1 else c in
      let m1, m2 = c1.mantisse / c, c2.mantisse / c in
      let (m1, m2) =
        if m2 = zero then (
          if m1 > zero then (of_int 1, m2)
          else if m1 < zero then (of_int (-1), m2)
          else (
            assert (m1 = zero) ;
            (of_int 1, of_int 1)
          )
        ) else (m1, m2) in
      let e = c1.exponent - c2.exponent in
      let (e1, e2) = if e >= zero then (e, zero) else (zero, -e) in
      ({ mantisse = m1 ; exponent = e1 }, { mantisse = m2 ; exponent = e2 }) in
    let es = annihil_pairs es in
    if Bigint.(c1.mantisse = zero) then Const Tenadic.zero
    else (
      match es, Tenadic.is_one c1, Tenadic.is_one c2 with
      | [], _, false -> Prod [(true, Const c1); (false, Const c2)]
      | [], _, true -> Const c1
      | [(true, e)], true, true -> e
      | _, false, false -> Prod ((true, Const c1) :: (false, Const c2) :: es)
      | _, false, true -> Prod ((true, Const c1) :: es)
      | _, true, false -> Prod ((false, Const c2) :: es)
      | _, true, true -> Prod es
    )
  | Pow (Pow (e1, e2), e3) -> simpl (Pow (e1, Prod [(true, e2) ; (true, e3)]))
  | Pow (e1, e2) ->
    let e2 = simpl e2 in
    if expr_is_zero e2 then Const Tenadic.one
    else if expr_is_one e2 then simpl e1
    else
      let e1 = simpl e1 in
      if expr_is_one e1 then Const Tenadic.one
      else Pow (e1, e2)
  | Var x -> Var x
  | Call (f, args) ->
    let args = List.map simpl args in
    match f, args with
    | Sqrt, [Const n] when Tenadic.is_zero n || Tenadic.is_one n -> Const n
    | (Ln | Log), [Const n] when Tenadic.is_one n -> Const Tenadic.zero
    | _, _ -> Call (f, args)

let rec develop = function
  | Const n -> Const n
  | Infinity s -> Infinity s
  | Paren e -> develop e
  | Sum es -> Sum (List.map (fun (s, e) -> (s, develop e)) es)
  | Prod es ->
    let es = List.map (fun (b, e) -> (b, develop e)) es in
    let rec aux = function
      | [] -> [(true, Const Tenadic.one)]
      | (b, Sum es1) :: es ->
        let l = aux es in
        List.concat_map (fun (s', e') ->
          List.map (fun (s, e) ->
            (s = s', Prod [(b, e) ; (true, e')])) es1) l
      | (b, e) :: es ->
        let l = aux es in
        List.map (fun (s, e') -> (s, Prod [(b, e) ; (true, e')])) l in
    Sum (aux es)
  | Pow (e, Const n) when n = Tenadic.two -> Prod [(true, e); (true, e)]
  | Pow (e, Const n) when n = Tenadic.three -> Prod [(true, e); (true, e); (true, e)]
  | Pow (e, Sum es) ->
    let e = develop e in
    develop (Prod (List.map (fun (b, e') -> (b, Pow (e, develop e'))) es))
  | Pow (e1, e2) -> Pow (develop e1, develop e2)
  | Var y -> Var y
  | Call (f, args) ->
    match f, List.map develop args with
    | (Ln | Log) as f, [Prod es] -> develop (Sum (List.map (fun (b, e) -> (b, Call (f, [e]))) es))
    | Tan, [e] -> develop (Prod [(true, Call (Sin, [e])) ; (false, Call (Cos, [e]))])
    | Tanh, [e] -> develop (Prod [(true, Call (Sinh, [e])) ; (false, Call (Cosh, [e]))])
    | f, args -> Call (f, args)


(* Return the comparison symmetric to the one provided. *)
let sym_comparison =
  let open ProofAssistant in function
  | Lt -> Gt
  | Gt -> Lt
  | Le -> Ge
  | Ge -> Le
  | Eq -> Eq
  | Neq -> Neq

(* Negation of a property. *)
let rec negation =
  let open ProofAssistant in function
  | True -> False
  | False -> True
  | Parenthesis p -> negation p
  | Not p -> p
  | And ps -> Or (List.map negation ps)
  | Or ps -> And (List.map negation ps)
  | Implication (ps, p) -> And (negation p :: ps)
  | Comparison (e1, Lt, e2) -> Comparison (e1, Ge, e2)
  | Comparison (e1, Le, e2) -> Comparison (e1, Gt, e2)
  | Comparison (e1, Gt, e2) -> Comparison (e1, Le, e2)
  | Comparison (e1, Ge, e2) -> Comparison (e1, Lt, e2)
  | Comparison (e1, Eq, e2) -> Comparison (e1, Neq, e2)
  | Comparison (e1, Neq, e2) -> Comparison (e1, Eq, e2)
  | Forall (x, t, p) -> Exists (x, t, negation p)
  | Exists (x, t, p) -> Forall (x, t, negation p)

(* Inner function used to define both suppose and force_suppose.
  It takes as argument the corresponding set_interval function, the operators of its
  return monad, and what to do for the False case. *)
let suppose_gen (set_interval : Env.t -> Value.var -> Interval.t -> _) ret bind false_case m p =
  let rec aux m =
    let open ProofAssistant in function
    | True -> ret m
    | False -> false_case m
    | And ps -> List.fold_left (fun mo p -> bind mo (fun m -> aux m p)) (ret m) ps
    | Parenthesis p -> aux m p
    | Not p -> aux m (negation p)
    | Or _ | Implication _ | Forall _ | Exists _ -> ret m (* We just ignore this new information. *)
    | Comparison (e1, c, e2) ->
      let learn_direct m e1 c e2 =
        let force_out : Interval.bound -> Interval.bound = function
          | None -> None
          | Some (n, _) -> Some (n, false) in
        let i2 =
          let i2 = interval m e2 in
          match c with
          | Lt -> (None, force_out (snd i2))
          | Gt -> (force_out (fst i2), None)
          | Le -> (None, snd i2)
          | Ge -> (fst i2, None)
          | Eq -> i2
          | _ -> (None, None) in
        match simpl e1 with
        | Value.Var x -> set_interval m x i2
        | e1 ->
          let i1 = interval m e1 in
          if Interval.(is_empty (inter i1 i2)) then false_case m
          else ret m in
      bind (learn_direct m e1 c e2) (fun m ->
        bind (learn_direct m e2 (sym_comparison c) e1) (fun m ->
          if e1 = e2 then
            match c with
            | Lt | Gt | Neq -> false_case m
            | Le | Ge | Eq -> ret m
          else ret m)) in
  aux m p

let suppose = suppose_gen Env.set_interval Option.some Option.bind (fun _m -> None)
let force_suppose = suppose_gen Env.force_set_interval (fun x -> x) (fun x f -> f x) (fun m -> m)


type comparison = Lt | Eq | Gt

let compare_values env e1 e2 =
  let normalise e = simpl (develop (simpl e)) in
  let e1 = normalise e1 in
  let e2 = normalise e2 in
  if e1 = e2 then Some Eq
  else
    let compare n1 n2 =
      let open Bigint in
      let m = (Tenadic.sub n1 n2).mantisse in
      if m < zero then Lt
      else if m > zero then Gt
      else Eq in
    let i1 = interval env e1 in
    let i2 = interval env e2 in
    (* Return true if we can definitively assert that e1 < e2. *)
    let test_lt i1 i2 =
      match i1, i2 with
      | (_min1, Some (max1, included1)), (Some (min2, included2), _max2) ->
        (match compare max1 min2 with
         | Lt -> true
         | Eq when not included1 || not included2 -> true
         | _ -> false)
      | _, _ -> false in
    (* Return true if we can definitively assert that e1 = e2. *)
    let test_eq i1 i2 =
      match i1, i2 with
      | (Some (min1, true), Some (max1, true)), (Some (min2, true), Some (max2, true)) ->
        if min1 = max1 && max1 = min2 && min2 = max2 then true
        else false
      | _, _ -> false in
    match test_eq i1 i2, test_lt i1 i2, test_lt i2 i1 with
    | true, false, false -> Some Eq
    | false, true, false -> Some Lt
    | false, false, true -> Some Gt
    | false, false, false -> None
    | _, _, _ -> None (* We are here in an incoherent environment. *)

let equivalent env e1 e2 =
  match compare_values env e1 e2 with
  | None -> None
  | Some Eq -> Some true
  | Some (Lt | Gt) -> Some false

let deriv x e =
  let square e = Prod [(true, e); (true, e)] in
  let rec aux = function
  | Const _ -> Const Tenadic.zero
  | Infinity _ -> assert false
  | Paren e -> aux e
  | Sum es -> Sum (List.map (fun (s, e) -> (s, aux e)) es)
  | Prod [] -> Const Tenadic.zero
  | Prod ((true, e) :: es) ->
    Sum [(true, Prod ((true, aux e) :: es)) ; (true, Prod [(true, e) ; (true, aux (Prod es))])]
  | Prod ((false, e) :: es) ->
    Sum [(false, Prod ((true, aux e) :: (false, square e) :: es)) ;
         (true, Prod [(false, e) ; (true, aux (Prod es))])]
  | Pow (e1, e2) ->
    Prod [(true, Pow (e1, e2)) ;
          (true, Sum [(true, Prod [(true, aux e2); (true, Call (Ln, [e1]))]) ;
                      (true, Prod [(true, e2); (true, aux e1); (false, e1)])])]
  | Var y -> Const (if String.equal x y then Tenadic.one else Tenadic.zero)
  | Call (f, args) ->
    match f, args with
    | Sqrt, [e] -> Prod [(true, aux e); (false, Const Tenadic.two); (false, Call (Sqrt, [e]))]
    | Ln, [e] -> Prod [(true, aux e); (false, e)]
    | Log, [e] -> Prod [(true, aux e); (false, Call (Ln, [Const Tenadic.ten])); (false, e)]
    | Sin, [e] -> Prod [(true, aux e); (true, Call (Cos, [Var x]))]
    | Cos, [e] -> Sum [(false, Prod [(true, aux e); (true, Call (Sin, [Var x]))])]
    | Tan, args -> aux (Prod [(true, Call (Sin, args)); (false, Call (Cos, args))])
    | Sinh, [e] -> Prod [(true, aux e); (true, Call (Cosh, [Var x]))]
    | Cosh, [e] -> Prod [(true, aux e); (true, Call (Sinh, [Var x]))]
    | Tanh, args -> aux (Prod [(true, Call (Sinh, args)); (false, Call (Cosh, args))])
    | _, _ -> failwith "Don't know how to derive this function." in
  simpl (aux e)

let rec defined env =
  let both r1 r2 =
    match r1, r2 with
    | Some false, _ | _, Some false -> Some false
    | None, _ | _, None -> None
    | Some true, Some true -> Some true in function
  | Const _ -> Some true
  | Infinity _ -> Some false
  | Paren e -> defined env e
  | Sum es ->
    List.fold_left (fun r (_s, e) ->
      both r (defined env e)) (Some true) es
  | Prod es ->
    List.fold_left (fun r (b, e) ->
      both (both r (defined env e))
        (if b then Some true else (
          let i = interval env e in
          if Interval.contains i Tenadic.zero then (
            if Interval.eq i (Some (Tenadic.zero, true), Some (Tenadic.zero, true)) then
              (* We know for sure that there is a division by zero. *)
              Some false
            else None
          ) else Some true))) (Some true) es
  | Pow (e1, e2) ->
    both (defined env e1)
      (both (defined env e2)
        (None (* TODO *)))
  | Var x -> Some (not (Interval.is_empty (Env.get_interval env x)))
  | Call (f, args) ->
    both
      (List.fold_left both (Some true) (List.map (defined env) args))
      (match f, args with
       | Sqrt, [e] ->
         (match interval env e with
          | _, Some (n, false) when Tenadic.(sign (opp n)) (* Includes 0 *) -> Some false
          | _, Some (n, true) when not (Tenadic.sign n) (* Excludes 0 *) -> Some false
          | None, _ -> None
          | Some (n, _), _ -> if Tenadic.sign n then Some true else None)
       | (Ln | Log), [e] ->
         (match interval env e with
          | _, Some (n, _) when Tenadic.(sign (opp n)) (* Includes 0 *) -> Some false
          | None, _ -> None
          | Some (n, _), _ -> if Tenadic.(sign (opp n)) then None else Some true)
       | (Sin | Cos), [_e] -> Some true
       | Tan, [_e] -> None (* TODO *)
       | (Asin | Acos), [e] ->
         let i = interval env e in
         let i' = Interval.inter i (Some (Tenadic.(opp one), false), Some (Tenadic.one, false)) in
         if Interval.is_empty i' then Some false
         else if Interval.eq i i' then Some true
         else None
       | Atan, [_e] -> Some true
       | (Sinh | Cosh | Tanh), [_e] -> Some true
       | _, _ -> None)

(* Whether an expression contains a variable. *)
let rec contains x = function
  | Const _ | Infinity _ -> false
  | Paren e -> contains x e
  | Sum es | Prod es -> List.exists (fun (_, e) -> contains x e) es
  | Pow (e1, e2) -> contains x e1 || contains x e2
  | Var y -> x = y
  | Call (_f, args) -> List.exists (contains x) args

(* Like remarkable, but instead of comparing the expression to zero, compare it with e0.
  If e0 is an infinity, this function will mainly focus on definedness. *)
let rec remarkable_with env e0 x = function
  | Const _ -> []
  | Infinity _ -> []
  | Paren e -> remarkable_with env e0 x e
  | Sum es ->
    let (es_x, es_wx) =
      List.partition_map (fun (s, e) ->
        if contains x e then Either.Left (s, e) else Either.Right (s, e)) es in
    (match es_x with
     | [] -> []
     | [(s, e)] ->
       let e' =
         if s then
           (* Intuitively, that means that e + es_wx = e0, and thus e = e0 - es_wx. *)
           Sum ((true, e0) :: List.map (fun (s, e) -> (not s, e)) es_wx)
          else
            (* es_wx - e = e0 and thus e = es_wx - e0. *)
            Sum ((false, e0) :: es_wx) in
       remarkable_with env e' x e
     | es -> List.concat_map (fun (_, e) -> remarkable_with env (Infinity true) x e) es)
  | Prod es ->
    let (es_x, es_wx) =
      List.partition_map (fun (b, e) ->
        if contains x e then Either.Left (b, e) else Either.Right (b, e)) es in
    (match es_x with
     | [] -> []
     | [(b, e)] ->
       if b then
         (* Intuitively, that means that e * es_wx = e0, and thus e = e0 / es_wx. *)
         remarkable_with env (Prod ((true, e0) :: List.map (fun (b, e) -> (not b, e)) es_wx)) x e
        else
          (* es_wx / e = e0 and thus e = es_wx / e0.
            We also add the zeroes for e. *)
          (remarkable_with env (Const Tenadic.zero) x e
           @ remarkable_with env (Prod ((false, e0) :: es_wx)) x e)
     | es ->
       List.concat_map (fun (_, e) -> remarkable_with env (Const Tenadic.zero) x e) es)
  | Pow (e1, e2) ->
    remarkable_with env (Const Tenadic.zero) x e1
    @ remarkable_with env (Const Tenadic.zero) x e2
  | Var y -> if x = y then [e0] else []
  | Call (f, args) ->
    match f, args with
    | Sqrt, [e] ->
      remarkable_with env (Const Tenadic.zero) x e
      @ remarkable_with env (Pow (e0, Const Tenadic.two)) x e
    | Ln, [e] -> remarkable_with env (Const Tenadic.zero) x e
    | Log, [e] ->
      remarkable_with env (Const Tenadic.zero) x e
      @ remarkable_with env (Pow (Const Tenadic.ten, e0)) x e
    | Sin, [e] -> remarkable_with env (Call (Asin, [e0])) x e
    | Cos, [e] -> remarkable_with env (Call (Acos, [e0])) x e
    | Tan, [e] -> remarkable_with env (Call (Atan, [e0])) x e
    | Asin, [e] -> remarkable_with env (Call (Sin, [e0])) x e
    | Acos, [e] -> remarkable_with env (Call (Cos, [e0])) x e
    | Atan, [e] -> remarkable_with env (Call (Tan, [e0])) x e
    | Sinh, [e] -> remarkable_with env (Call (Asinh, [e0])) x e
    | Cosh, [e] -> remarkable_with env (Call (Acosh, [e0])) x e
    | Tanh, [e] -> remarkable_with env (Call (Atanh, [e0])) x e
    | Asinh, [e] -> remarkable_with env (Call (Sinh, [e0])) x e
    | Acosh, [e] -> remarkable_with env (Call (Cosh, [e0])) x e
    | Atanh, [e] -> remarkable_with env (Call (Tanh, [e0])) x e
    | Local f, args ->
      List.concat_map (fun e -> remarkable_with env (Infinity true) x e) args
      @ if Env.get_value env f = None then []
        else remarkable_with env e0 x (unfold env (Call (Local f, args)))
    | _, _ -> []

let remarkable env x e =
  remarkable_with env (Const Tenadic.zero) x e

