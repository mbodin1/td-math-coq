
(* Declaration meant to mimick proof assistant declarations. *)

(* Comparison operators. *)
type comparison =
  | Lt
  | Gt
  | Le
  | Ge
  | Eq
  | Neq

(* Properties.
  The goal is here to represent the property as it is written. *)
type property =
  | True
  | False
  | And of property list
  | Or of property list
  | Not of property
  | Implication of property list * property
  | Parenthesis of property
  | Comparison of Value.expr * comparison * Value.expr
  | Forall of Value.var * Type.t * property
  | Exists of Value.var * Type.t * property


(* Vernacular Coq commands. *)
type command =
  | Definition of Value.var * Value.var list * Value.expr
  | Variable of Value.var * Type.t
  | Hypothesis of Value.var * property
  | ProvenLemma of Value.var * property
  | Print of Value.var
  | Check of Value.expr

