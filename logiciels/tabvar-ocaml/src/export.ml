
open Value

(* Convert the function name into a string. *)
let function_name = function
  | Sqrt -> "sqrt"
  | Ln -> "ln"
  | Log -> "log"
  | Sin -> "sin"
  | Cos -> "cos"
  | Tan -> "tan"
  | Asin -> "asin"
  | Acos -> "acos"
  | Atan -> "atan"
  | Sinh -> "sinh"
  | Cosh -> "cosh"
  | Tanh -> "tanh"
  | Asinh -> "asinh"
  | Acosh -> "acosh"
  | Atanh -> "atanh"
  | Local f -> f

(* Calling convention of a function *)
type calling =
  | Spaces (* f arg1 arg2 *)
  | Parentheses (* f (arg1, arg2) *)
  | Special (* Things like sqrt. *)

(* Return the calling convention of a function. *)
let calling_convention = function
  | Sqrt -> Special
  | Ln | Log -> Spaces
  | Sin | Cos | Tan -> Spaces
  | Asin | Acos | Atan -> Spaces
  | Sinh | Cosh | Tanh -> Spaces
  | Asinh | Acosh | Atanh -> Spaces
  | Local _f -> Parentheses

(* A term that won't need any parenthesis. *)
let is_closed = function
  | Const n -> Tenadic.sign n
  | Infinity _ | Var _ | Paren _ | Sum [(true, _)] -> true
  | _ -> false

(* Add parentheses to avoid any misinterpretation of the formula. *)
let rec add_parentheses ?(on_division=true) =
  let force_paren e =
    if is_closed e then e
    else Paren e in function
  | Const n -> Const n
  | Infinity s -> Infinity s
  | Paren e -> Paren (add_parentheses e)
  | Sum es ->
    Sum (List.map (fun (s, e) ->
      match add_parentheses e with
      | Sum _ as e -> (s, Paren e)
      | (Const n | Prod ((_, Const n) :: _)) as e
         when s = false && Tenadic.(sign (opp n)) -> (false, Paren e)
      | e -> (s, e)) es)
  | Prod es ->
    let es = List.map (fun (b, e) -> (b, add_parentheses e)) es in
    let es =
      List.map (function
        | (false, (Paren e | e)) when not on_division -> (false, e)
        | (b, (Sum _ as e)) -> (b, Paren e)
        | (b, (Prod _ as e)) -> (b, Paren e)
        | (b, e) -> (b, e)) es in
    Prod es
  | Pow (e1, e2) ->
    let e1 = add_parentheses e1 in
    let e2 = add_parentheses e2 in
    Pow (force_paren e1, e2)
  | Var x -> Var x
  | Call (f, args) ->
    let args = List.map add_parentheses args in
    let args =
      if calling_convention f = Spaces then
        (List.map force_paren args)
      else args in
    Call (f, args)


(* Decimal separator, e.g. in pi = 3.14 *)
let decimal_separator = ','

(* Print out a number. *)
let string_of_num n =
  let open Tenadic in
  let open Bigint in
  let sign = if Tenadic.sign n then "" else "−" in
  let mantisse = abs n.mantisse in
  let base =
    if n.exponent >= zero then
      mantisse * pow (of_int 10) n.exponent
    else
      mantisse / pow (of_int 10) (-n.exponent) in
  let decimals =
    if n.exponent >= zero then ""
    else
      let dec = rem mantisse (pow (of_int 10) (-n.exponent)) in
      if dec = zero then ""
      else
        let dec = to_string dec in
        let fills = -n.exponent - of_int (String.length dec) in
        let fills = to_int_exn fills in
        Printf.sprintf {|%c%s%s|} decimal_separator
          (String.make fills '0') dec in
  Printf.sprintf {|%s%s%s|} sign (to_string base) decimals


module LaTeX = struct

(* Convert the function name into a LaTeX expression. *)
let function_name = function
  | Sqrt -> {|\sqrt|}
  | Ln -> {|\ln|}
  | Log -> {|\log|}
  | Sin -> {|\sin|}
  | Cos -> {|\cos|}
  | Tan -> {|\tan|}
  | Asin -> {|\arcsin|}
  | Acos -> {|\arccos|}
  | Atan -> {|\arctan|}
  | Sinh -> {|\sinh|}
  | Cosh -> {|\cosh|}
  | Tanh -> {|\tanh|}
  | Asinh -> {|\mathrm{asinh}\,|}
  | Acosh -> {|\mathrm{acosh}\,|}
  | Atanh -> {|\mathrm{atanh}\,|}
  | Local f -> Printf.sprintf {|\mathit{%s}|} f

let rec convert = function
  | Const n -> string_of_num n
  | Infinity s ->
    let sign = if s then '+' else '-' in
    Printf.sprintf {|%c\infty|} sign
  | Paren e -> Printf.sprintf {|\left\(%s\right\)|} (convert e)
  | Sum [] -> "{0}"
  | Sum ((s, e) :: es) ->
    "{" ^ (if s then "" else "-") ^ convert e
    ^ String.concat "" (List.map (function
        | (true, e) -> " + " ^ convert e
        | (false, e) -> " - " ^ convert e) es) ^ "}"
  | Prod es ->
    let (up, down) =
      List.partition_map (fun (b, e) -> if b then Either.Left e else Either.Right e) es in
    let up = if up = [] then [Const Bigint.{ mantisse = of_int 1 ; exponent = zero }] else up in
    let convert_prod es =
      "{" ^ String.concat {| \times |} (List.map convert es) ^ "}" in
    if down = [] then convert_prod up
    else Printf.sprintf {|\frac{%s}{%s}|} (convert_prod up) (convert_prod down)
  | Pow (e1, e2) -> Printf.sprintf {|{%s}^{%s}|} (convert e1) (convert e2)
  | Var x -> Printf.sprintf {|\mathit{%s}|} x
  | Call (f, args) ->
    let args = List.map convert args in
    let args =
      match calling_convention f with
      | Spaces -> {|\,|} ^ String.concat "" (List.map (fun a -> {|\,|} ^ a) args)
      | Parentheses -> "(" ^ String.concat ", " args ^ ")"
      | Special -> String.concat "" (List.map (Printf.sprintf {|{%s}|}) args) in
    Printf.sprintf {|%s%s|} (function_name f) args

end

let to_LaTeX e = LaTeX.convert (add_parentheses e)

module Html = struct

open Js_of_ocaml_tyxml
open Tyxml_js

let function_name = function
  | Sqrt -> {|√|}
  | f -> function_name f

let rec convert =
  let rec add_between b = function
    | [] -> []
    | [x] -> [x]
    | x :: l -> x :: b :: add_between b l in function
  | Const n -> [%html "<span>"[Html.txt (string_of_num n)]"</span>"]
  | Infinity s ->
    let sign = if s then "+" else {|−|} in
    [%html "<span>"[Html.txt sign ; Html.txt {|∞|}]"</span>"]
  | Paren e ->
    [%html "<span>"[
      [%html "<span class = 'parenthesis'>"[Html.txt "("]"</span>"] ;
      convert e ;
      [%html "<span class = 'parenthesis'>"[Html.txt ")"]"</span>"]
    ]"</span>"]
  | Sum [] -> [%html "<span>0</span>"]
  | Sum ((s, e) :: es) ->
    let sign = if s then [] else [Html.txt {|−|}] in
    let esc =
      List.concat_map (function
        | (true, e) -> [Html.txt " + "; convert e]
        | (false, e) -> [Html.txt {| − |}; convert e]) es in
    [%html "<span>"(sign @ [convert e] @ esc)"</span>"]
  | Prod es ->
    let (up, down) =
      List.partition_map (fun (b, e) -> if b then Either.Left e else Either.Right e) es in
    let up = if up = [] then [Const Bigint.{ mantisse = of_int 1 ; exponent = zero }] else up in
    let convert_prod es =
      [%html "<span>"(add_between (Html.txt {|×|}) (List.map convert es))"</span>"] in
    if down = [] then convert_prod up
    else
      [%html
      "<span class = 'fraction'>"
        "<span class = 'num'>"[convert_prod up]"</span>"
        "<span class = 'fracbar'></span>"
        "<span class = 'den'>"[convert_prod down]"</span>"
      "</span>"]
  | Pow (e1, e2) -> [%html "<span>"[convert e1]"<sup>"[convert e2]"</sup></span>"]
  | Var x -> [%html "<span>"[Html.txt x]"</span>"]
  | Call (f, args) ->
    let args = List.map convert args in
    let fnode = Html.txt (function_name f) in
    let args =
     match calling_convention f with
     | Spaces ->
       [%html "<span class = 'funargsp'>"(Html.txt " " :: add_between (Html.txt " ") args)"</span>"]
     | Parentheses ->
       [%html "<span class = 'funargpt'>" (
          (Html.txt " (")
          :: (add_between (Html.txt ", ") args)
          @ [Html.txt ")"]
        )"</span>"]
     | Special ->
       match f with
       | Sqrt ->
         [%html "<span class = 'sqrt'>"(add_between (Html.txt ", ") args)"</span>"]
       | _ -> assert false in
    [%html "<span class = 'funcall'>"[fnode; args]"</span>"]

end

let to_html e = Html.convert (add_parentheses ~on_division:false e)

let to_string e =
  let rec aux = function
    | Const n -> string_of_num n
    | Infinity s -> (if s then "+" else {|−|}) ^ {|∞|}
    | Paren e -> Printf.sprintf {|(%s)|} (aux e)
    | Sum [] -> "0"
    | Sum ((s, e) :: es) ->
      let sign = if s then "" else {|−|} in
      let esc =
        List.map (function
          | (true, e) -> " + " ^ aux e
          | (false, e) -> {| − |} ^ aux e) es in
      sign ^ aux e ^ String.concat "" esc
    | Prod es ->
      let (up, down) =
        List.partition_map (fun (b, e) -> if b then Either.Left e else Either.Right e) es in
      let up = if up = [] then [Const Bigint.{ mantisse = of_int 1 ; exponent = zero }] else up in
      let convert_prod es = String.concat {|×|} (List.map aux es) in
      (match down with
      | [] -> convert_prod up
      | [e] when is_closed e -> Printf.sprintf {|%s/%s|} (convert_prod up) (convert_prod down)
      | down -> Printf.sprintf {|%s / (%s)|} (convert_prod up) (convert_prod down))
    | Pow (e1, e2) -> Printf.sprintf {|%s^%s|} (aux e1) (aux e2)
    | Var x -> x
    | Call (f, args) ->
      let args = List.map aux args in
      let args =
        match calling_convention f with
        | Spaces -> String.concat "" (List.map (fun a -> " " ^ a) args)
        | Parentheses | Special -> Printf.sprintf {|(%s)|} (String.concat ", " args) in
      Printf.sprintf {|%s%s|} (function_name f) args in
  aux (add_parentheses e)

let fexpr_to_string = function_name

let comparison_to_string =
  let open ProofAssistant in function
    | Le -> "<="
    | Ge -> ">="
    | Lt -> "<"
    | Gt -> ">"
    | Eq -> "="
    | Neq -> "<>"

let rec property_to_string =
  let open ProofAssistant in function
  | True -> "True"
  | False -> "False"
  | And ps -> "(" ^ String.concat {| /\ |} (List.map property_to_string ps) ^ ")"
  | Or ps -> "(" ^ String.concat {| \/ |} (List.map property_to_string ps) ^ ")"
  | Parenthesis p -> property_to_string p
  | Not p -> "~ " ^ property_to_string p
  | Implication (ps, p) ->
    String.concat "" (List.map (fun p -> property_to_string p ^ " -> ") ps) ^ property_to_string p
  | Comparison (e1, c, e2) ->
    Printf.sprintf "%s %s %s" (to_string e1) (comparison_to_string c) (to_string e2)
  | Forall (x, t, p) ->
    Printf.sprintf "forall (%s : %s), %s" x (Type.print t) (property_to_string p)
  | Exists (x, t, p) ->
    Printf.sprintf "exists (%s : %s), %s" x (Type.print t) (property_to_string p)

let interval_to_string (min, max) =
  let first =
    match min with
    | None -> {|]−∞|}
    | Some (n, included) ->
      Printf.sprintf "%c%s" (if included then '[' else ']') (string_of_num n) in
  let second =
    match max with
    | None -> {|+∞[|}
    | Some (n, included) ->
      Printf.sprintf "%s%c" (string_of_num n) (if not included then '[' else ']') in
  Printf.sprintf "%s ; %s" first second

