
(* Convert the expression into a LaTeX expression. *)
val to_LaTeX : Value.expr -> string

(* Convert the expression into a DOM node. *)
val to_html : Value.expr -> Html_types.phrasing Js_of_ocaml_tyxml.Tyxml_js.Html.elt

(* Convert the expression into a string. *)
val to_string : Value.expr -> string

(* Print-out a function. *)
val fexpr_to_string : Value.fexpr -> string

(* Print-out a property. *)
val property_to_string : ProofAssistant.property -> string

(* Output a readable representation of the interval. *)
val interval_to_string : Interval.t -> string

