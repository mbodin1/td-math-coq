
open Value

(* Replace each variable and function by their definition, if any. *)
val unfold : Env.t -> expr -> expr

(* Simplify an expression. *)
val simpl : expr -> expr

(* Expand an expression. *)
val develop : expr -> expr

(* Derivate an expression along the provided variable. *)
val deriv : var -> expr -> expr

(* State whether the two expressions are equivalent (Some true), differ (Some false),
  or don't know (None). *)
val equivalent : Env.t -> expr -> expr -> bool option

(* A type to express comparison betweeen values. *)
type comparison = Lt | Eq | Gt

(* Compare if possible two expressions. *)
val compare_values : Env.t -> expr -> expr -> comparison option

(* State whether the provided expression is defined within this environment
  (no division by 0, etc.).
  Return None if this is not known (example: an expression with a division
  by something too complicated). *)
val defined : Env.t -> expr -> bool option

(* Compute an overapproximation of the interval within which the provided expression
  definitively is. *)
val interval : Env.t -> Value.expr -> Interval.t

(* Update the environment to assume that a property holds.
  It might ignore some properties if too complicated.
  If it detects that it makes the environment inconsistent, it returns None.
  This function is in Manipulate and not Env as the property may contain expressions
  to analyse. *)
val suppose : Env.t -> ProofAssistant.property -> Env.t option

(* Like suppose, but do not perform any consistency check. *)
val force_suppose : Env.t -> ProofAssistant.property -> Env.t

(* Given a variable (defined in the current environment) and an expression dependent on
  this variable, return a list of values that would be good to investigate for the value
  of this variable.
  These values could be remarkable because they change the sign or the definedness of
  the expression, but no guarantee of completeness or correctness is provided.
  In particular, not all these values will be defined. *)
val remarkable : Env.t -> Value.var -> Value.expr -> Value.expr list

