%{ (** Module Parser. **)

(* Pairs of ordered type, ordred with lexicographic order. *)
module Pair (A : Set.OrderedType) (B : Set.OrderedType) = struct
  type t = A.t * B.t
  let compare (a1, b1) (a2, b2) =
    match compare a1 a2 with
    | 0 -> compare b1 b2
    | r -> r
end

module SSSet = Set.Make (Pair (String) (String))

(* Recognise associated open and close parentheses. *)
let accepted_parentheses l r =
  let all_accepted =
    let add s (l, r) =
      List.fold_left (fun s p ->
        let l = p ^ l in
        let r = p ^ r in
        let s = SSSet.add (l, r) s in
        let s = SSSet.add ("\\left" ^ l, "\\right" ^ r) s in
        s) s [""; "\\"] in
    let l = [("(", ")") ; ("[", "]") ; ("{", "}")] in
    List.fold_left add SSSet.empty l in
  let remove_space =
    Str.global_replace (Str.regexp "[ \t\n\r]") "" in
  let l = remove_space l in
  let r = remove_space r in
  SSSet.mem (l, r) all_accepted

open Value

(* Recognise a base function from its name. *)
let recognise_function = function
  | "sqrt" -> Sqrt
  | "ln" -> Ln
  | "log" -> Log
  | "sin" -> Sin
  | "cos" -> Cos
  | "tan" -> Tan
  | "asin" | "arcsin" | "sin⁻¹" | "sin^{-1}" -> Asin
  | "acos" | "arccos" | "cos⁻¹" | "cos^{-1}" -> Acos
  | "atan" | "arctan" | "tan⁻¹" | "tan^{-1}" -> Atan
  | "sinh" -> Sin
  | "cosh" -> Cos
  | "tanh" -> Tan
  | "asinh" | "arcsinh" | "sinh⁻¹" | "sinh^{-1}" -> Asinh
  | "acosh" | "arccosh" | "cosh⁻¹" | "cosh^{-1}" -> Acosh
  | "atanh" | "arctanh" | "tanh⁻¹" | "tanh^{-1}" -> Atanh
  | f -> Local f

(* As the grammar for function calls is technically ambiguous, we parse
  "f (x)" as the parenthesised expression "(e)" applied to the function f.
  This parenthesis is in practise unwanted, so this function removes it. *)
let remove_single_par = function
  | [ Paren e ] -> [e]
  | args -> args

open ProofAssistant

(* From a list of predicate, extract the last element as the conclusion of all the others. *)
let make_impl l =
  let rec aux = function
  | [] -> assert false
  | [p] -> (p, [])
  | p :: l ->
    let (p', l) = aux l in
    (p', p :: l) in
  let (p, l) = aux l in
  if l = [] then p
  else Implication (l, p)

(* Some places accept a type provided to variables, but we only accept real numbers. *)
let check_real str =
  if str <> "real" then
    invalid_arg (Printf.sprintf "Got: %s. Expected: real." str)

%}

%token            EOF
%token<string>    OPAR CPAR
%token            COMMA
%token            PLUS MINUS
%token            TIMES DIVIDE
%token            EXP
%token<Tenadic.t> EXPV
%token            INFINITY
%token<string>    IDENT
%token<Tenadic.t> NUM
%token            DEFINITION VARIABLE HYPOTHESIS LEMMA PROOF QED PRINT CHECK
%token            DOT COLON DEF
%token            TRUE FALSE AND OR NOT IMPL
%token            LT GT LE GE EQ NEQ
%token            FORALL EXISTS
%token            REQUIRE IMPORT EXPORT

%start<Value.expr>                    expr_full
%start<ProofAssistant.property>       property_full
%start<ProofAssistant.command list>   command_full

%%

full (v):
  | v = v; EOF   { v }

expr_full: e = full (expr)              { e }
property_full: p = full (property)      { p }
command_full: cs = full (list(command)) { List.concat cs }

%inline parenthesised(payload):
  | l = OPAR; p = payload; r = CPAR
    { if not (accepted_parentheses l r) then
        invalid_arg (Printf.sprintf "Parenthesis %s closed with %s." l r) ;
      p }

%inline empty:
  | /* empty */ { }

expr:
  | e = expr_sum                    { e }
  | s = infinity                    { Infinity s }

expr_sum:
  | l = expr_sum_ne_list                        { Sum l }
  | e = expr_prod; l = expr_sum_ne_list         { Sum ((true, e) :: l) }
  | e = expr_prod                               { e }

expr_sum_list:
  | l = loption(expr_sum_ne_list)   { l }

expr_sum_ne_list:
  | PLUS; e = expr_prod; l = expr_sum_list    { (true, e) :: l }
  | MINUS; e = expr_prod; l = expr_sum_list   { (false, e) :: l }

expr_prod:
  | es = expr_prod_atom; l = expr_prod_ne_list  { Prod (List.map (fun e -> (true, e)) es @ l) }
  | es = expr_prod_atom                         { match es with
                                                  | [e] -> e
                                                  | _ -> Prod (List.map (fun e -> (true, e)) es) }

expr_prod_list:
  | l = loption(expr_prod_ne_list)  { l }

expr_prod_ne_list:
  | TIMES; es = expr_prod_atom; l = expr_prod_list    { List.map (fun e -> (true, e)) es @ l }
  | DIVIDE; es = expr_prod_atom; l = expr_prod_list   { List.map (fun e -> (false, e)) es @ l }

expr_prod_atom:
  | e = expr_fun                                              { [e] }
  | e = expr_exp; es = expr_prod_atom_not_num                 { e :: es }
  | n = NUM; es = expr_prod_atom_not_num                      { Const n :: es }
  | x = IDENT                                                 { [Var x] }
  | e = expr_base_not_num_ident; es = expr_prod_atom_not_num  { e :: es }

expr_prod_atom_not_num:
  | (* empty *)                                               { [] }
  | e = expr_base_not_num_par                                 { [e] }
  | e = expr_exp; es = expr_prod_atom_not_num                 { e :: es }
  | e = expr_base_not_num_ident; es = expr_prod_atom_not_num  { e :: es }

expr_exp:
  | e1 = expr_base; EXP; e2 = expr_exp  { Pow (e1, e2) }
  | e1 = expr_base; EXP; e2 = expr_base { Pow (e1, e2) }
  | e = expr_base; n = EXPV             { Pow (e, Const n) }

expr_fun:
  (* This case is a bit tricky as "f (e)" could both the seen as a function call
    with expr = "(e)" and a parenthesised list of argument with just "e".
    Of course, these are the same, but to avoid any issues, we state that a
    parenthesised list of argument must have at least one comma. *)
  | f = fcall; args = nonempty_list(expr_base)                    { Call (f, remove_single_par args) }
  | f = fcall; parenthesised(empty)                               { Call (f, []) }
  | f = fcall;
    args = parenthesised(e = expr; COMMA; es = separated_nonempty_list(COMMA, expr) { e :: es })
    { Call (f, args) }

expr_base:
  | n = NUM                           { Const n }
  | e = expr_base_not_num             { e }

expr_base_not_num:
  | x = IDENT                         { Var x }
  | e = expr_base_not_num_ident       { e }

expr_base_not_num_ident:
  | e = parenthesised(expr)           { Paren e }

expr_base_not_num_par:
  | x = IDENT                         { Var x }

fcall:
  | f = IDENT     { recognise_function f }

infinity:
  | option(PLUS) INFINITY { true }
  | MINUS INFINITY        { false }

property:
  | p = property_quantif  { p }

property_quantif:
  | FORALL; l = nonempty_list (
        x = parenthesised(x = IDENT; COLON; r = IDENT { check_real r ; x }) { x }
      | x = IDENT                                                           { x });
    COMMA; p = property_quantif  { List.fold_left (fun p x -> Forall (x, Type.real, p)) p l }
  | EXISTS; l = nonempty_list (
        x = parenthesised(x = IDENT; COLON; r = IDENT { check_real r ; x }) { x }
      | x = IDENT                                                           { x });
    COMMA; p = property_quantif  { List.fold_left (fun p x -> Exists (x, Type.real, p)) p l }
  | p = property_impl                               { make_impl p }


(*
property_impl:
  | p = property_or                                                 { p }
  | l = nonempty_list(p = property_or; IMPL { p }); p = property_or { Implication (l, p) }
*)

property_impl:
  | p = property_or                           { [ p ] }
  | p = property_or; IMPL; l = property_impl  { p :: l }

property_or:
  | p = property_and                                                      { p }
  | p = property_and; OR; l = separated_nonempty_list (OR, property_and)  { Or (p :: l) }

property_and:
  | p = property_not                                                      { p }
  | p = property_not; AND; l = separated_nonempty_list(AND, property_not) { And (p :: l) }

property_not:
  | p = property_base           { p }
  | NOT; p = property_not       { Not p }

property_base:
  | TRUE                                  { True }
  | FALSE                                 { False }
  | e1 = expr; c = comparison; e2 = expr  { Comparison (e1, c, e2) }
  | p = parenthesised (property)          { Parenthesis p }

comparison:
  | LT  { Lt }
  | GT  { Gt }
  | LE  { Le }
  | GE  { Ge }
  | EQ  { Eq }
  | NEQ { Neq }

command:
  | c = command_base; DOT   { c }

command_base:
  | REQUIRE; option(IMPORT | EXPORT {}); id = IDENT                   { ignore id; [] }
  | DEFINITION; f = IDENT; args = list(argument); DEF; e = expr       { [Definition (f, args, e)] }
  | VARIABLE; x = IDENT; COLON; r = IDENT
    { check_real r ; [Variable (x, Type.real)] }
  | VARIABLE; l = list (parenthesised(x = IDENT; COLON; r = IDENT { check_real r ; x }))
    { List.map (fun x -> Variable (x, Type.real)) l }
  | HYPOTHESIS; x = IDENT; COLON; p = property                        { [Hypothesis (x, p)] }
  | HYPOTHESIS; l = list (parenthesised(x = IDENT; COLON; p = property { (x, p) }))
    { List.map (fun (x, p) -> Hypothesis (x, p)) l }
  | LEMMA; x = IDENT; COLON; p = property                             { [ProvenLemma (x, p)] }
  | PROOF | QED                                                       { [] }
  | PRINT; x = IDENT                                                  { [Print x] }
  | CHECK; e = expr                                                   { [Check e] }

argument:
  | x = IDENT                                                             { x }
  | x = parenthesised(IDENT)                                              { x }
  | x = parenthesised(x = IDENT; COLON; r = IDENT { check_real r ; x })   { x }

