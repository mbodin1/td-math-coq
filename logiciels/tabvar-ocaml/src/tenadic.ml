
type t = {
    mantisse : Bigint.t ;
    exponent : Bigint.t
  }

let zero = Bigint.{ mantisse = zero ; exponent = zero }
let one = Bigint.{ mantisse = of_int 1 ; exponent = zero }
let two = Bigint.{ mantisse = of_int 2 ; exponent = zero }
let three = Bigint.{ mantisse = of_int 3 ; exponent = zero }
let ten = Bigint.{ mantisse = of_int 10 ; exponent = zero }

let large = Bigint.{ mantisse = of_int 1 ; exponent = of_int 1000 }

let is_zero n = Bigint.(n.mantisse = zero)

let is_one n =
  let open Bigint in
  if n.exponent > zero then false
  else
    let m = pow (of_int 10) (-n.exponent) in
    m = n.mantisse

let rec add n1 n2 =
  let open Bigint in
  if n1.exponent > n2.exponent then add n2 n1
  else (
    let shift = n2.exponent - n1.exponent in
    assert (shift >= zero) ;
    let m2 = n2.mantisse * pow (of_int 10) shift in {
      mantisse = n1.mantisse + m2 ;
      exponent = n1.exponent
    }
  )

let opp n = Bigint.{ n with mantisse = -n.mantisse }

let sub n1 n2 =
  add n1 (opp n2)

let abs n =
  { n with mantisse = Bigint.abs n.mantisse }

let mult n1 n2 = Bigint.{
    mantisse = n1.mantisse * n2.mantisse ;
    exponent = n1.exponent + n2.exponent
  }

let rec normalise n =
  if is_zero n then zero
  else if Bigint.(rem n.mantisse (of_int 10) = zero) then
    normalise Bigint.{
      mantisse = n.mantisse / of_int 10 ;
      exponent = n.exponent + of_int 1
    }
  else n

let integer n =
  let n = normalise n in
  Bigint.(n.exponent >= zero)

let even n =
  let n = normalise n in
  if Bigint.(n.exponent < zero) then false
  else if Bigint.(n.exponent > zero) then true
  else (
    assert Bigint.(n.exponent = zero) ;
    Bigint.(rem n.mantisse (of_int 2) = zero)
  )

let sign n = Bigint.(n.mantisse >= zero)

let eq n1 n2 =
  is_zero (sub n1 n2)

let rec power_int n e =
  assert (e >= 0) ;
  let rec pow n = function
    | 0 -> one
    | 1 -> n
    | e ->
      let n2 = pow n (e / 2) in
      let n3 = mult n2 n2 in
      if e mod 2 = 0 then n3
      else (mult n n3) in
  pow n e

let to_int n =
  if not (integer n) then None
  else (
    let n = normalise n in
    match Bigint.to_int n.exponent with
    | None -> None
    | Some e ->
      assert (e >= 0) ;
      if e >= 100 then None
      else (
        let rec pow n = function
          | 0 -> n
          | e -> pow Bigint.(n * of_int 10) (e - 1) in
        Bigint.to_int (pow n.mantisse e)
      )
  )

