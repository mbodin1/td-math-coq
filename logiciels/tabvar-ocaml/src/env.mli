(* Environnement (function definitions) *)

(* Environment *)
type t

(* Create an empty environment. *)
val create : unit -> t

(* Add a new definition to the environment.
  Return None if already defined. *)
val define : t -> Value.var -> Type.t -> (Value.var list * Value.expr) option -> t option

(* Return a fresh variable.
  If the seed is provided, the variable name will try to look like it. *)
val fresh : ?seed:Value.var -> t -> Value.var

(* Get the type of a variable in an environnment. *)
val get_type : t -> Value.var -> Type.t option

(* Get the value of a variable, if present. *)
val get_value : t -> Value.var -> (Value.var list * Value.expr) option

(* Learning that a variable is included within this interval.
  If it detects that it makes the environment inconsistent, it returns None. *)
val set_interval : t -> Value.var -> Interval.t -> t option

(* Like set_interval, but doesn't do any consistency check. *)
val force_set_interval : t -> Value.var -> Interval.t -> t

(* Get the interval associated to a variable. *)
val get_interval : t -> Value.var -> Interval.t

(* Print out an environment, for debug purposes. *)
val print : t -> string

