
(* Number of arguments (only real numbers) or property. *)
type t =
  | Reals of int
  | Property

let from_arity a = Reals a
let property = Property

let real = from_arity 0

let print = function
  | Reals i -> String.concat "" (List.init i (fun _ -> "real → ")) ^ "real"
  | Property -> "property"

let get_fun = function
  | Reals i -> (List.init i (fun _ -> real), real)
  | Property -> ([], Property)

