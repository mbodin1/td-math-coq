
open Value

exception Error of string * string

let real = Type.from_arity 0
let real_fun1 = Type.from_arity 1

(* Return the type of a variable in the environment. *)
let get_type env x =
  match Env.get_type env x with
  | None -> raise (Error (Printf.sprintf "Unknown variable %s." x, x))
  | Some t -> t

(* Return the type of a function expression. *)
let check_fun env = function
  | Sqrt -> real_fun1
  | Ln | Log -> real_fun1
  | Sin | Cos | Tan -> real_fun1
  | Asin | Acos | Atan -> real_fun1
  | Sinh | Cosh | Tanh -> real_fun1
  | Asinh | Acosh | Atanh -> real_fun1
  | Local f -> get_type env f

(* Return the type of an expression. *)
let rec typecheck env = function
  | Const _ -> real
  | Infinity _ -> real
  | Paren e -> typecheck env e
  | Sum es ->
    List.iter (fun (_, e) -> check_expected real env e) es ;
    real
  | Prod es ->
    List.iter (fun (_, e) -> check_expected real env e) es ;
    real
  | Pow (e1, e2) ->
    check_expected real env e1 ;
    check_expected real env e2 ;
    real
  | Var x -> get_type env x
  | Call (f, args) ->
    let (ts, tr) = Type.get_fun (check_fun env f) in
    let lts = List.length ts in
    let largs = List.length args in
    if lts <> largs then (
      let f = Export.fexpr_to_string f in
      raise (Error (Printf.sprintf "The function %s expects %i arguments but was provided %i."
        f lts largs, f))
    ) ;
    List.iter2 (fun t a -> check_expected t env a) ts args ;
    tr

(* Check that the type of an expression is indeed what was expected. *)
and check_expected t_goal env e =
  let t = typecheck env e in
  if t <> t_goal then
    raise (Error (Printf.sprintf "Type %s expected, got %s instead."
      (Type.print t_goal) (Type.print t), Export.to_string e))

let check_expr env e =
  check_expected (Type.from_arity 0) env e ;
  e

let rec check_prop env =
  let open ProofAssistant in function
  | True -> True
  | False -> False
  | And ps -> And (List.map (check_prop env) ps)
  | Or ps -> Or (List.map (check_prop env) ps)
  | Not p -> Not (check_prop env p)
  | Implication (ps, p) -> Implication (List.map (check_prop env) ps, check_prop env p)
  | Parenthesis p -> Parenthesis (check_prop env p)
  | Comparison (e1, ((Eq | Neq) as c), e2) ->
    let t1 = typecheck env e1 in
    let t2 = typecheck env e2 in
    if t1 <> t2 then
      raise (Error (Printf.sprintf "Mismatch type for comparison: %s and %s"
        (Type.print t1) (Type.print t2), Export.to_string e1))
    else Comparison (e1, c, e2)
  | Comparison (e1, c, e2) -> Comparison (check_expr env e1, c, check_expr env e2)
  | Forall (x, t, p) | Exists (x, t, p) ->
    let env' =
      match Env.define env x t None with
      | Some env' -> env'
      | None ->
        raise (Error (Printf.sprintf "Variable %s is already defined." x,
          Export.property_to_string p)) in
    check_prop env' p

