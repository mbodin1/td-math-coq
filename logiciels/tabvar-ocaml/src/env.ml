
module SMap = Map.Make (String)

(* We store the type, a known interval for the value, and an optionnal expression. *)
type t = (Type.t * Interval.t * (Value.var list * Value.expr) option) SMap.t

(* Check whether an identifier is indeed valid. *)
let valid x =
  let lexbuf = Lexing.from_string x in
  lexbuf.Lexing.lex_curr_p <- {
    Lexing.pos_fname = "" ;
    Lexing.pos_lnum = 1 ;
    Lexing.pos_bol = 0 ;
    Lexing.pos_cnum = 0
  } ;
  (match Lexer.read lexbuf with
   | Parser.IDENT _ -> true
   | _ -> false)
  &&
  (match Lexer.read lexbuf with
   | Parser.EOF -> true
   | _ -> false)

(* Raise an error if the identifier is not valid. *)
let check_valid x =
  if not (valid x) then
    invalid_arg (Printf.sprintf "The identifier `%s' is not valid." x)

let print m =
  SMap.fold (fun x (t, i, e) str ->
    let e =
      match e with
      | None -> ""
      | Some (args, e) ->
        String.concat "" (List.map (fun x -> " " ^ x) args) ^ " ↦ " ^ Export.to_string e in
    let i =
      if t = Type.property then ""
      else Printf.sprintf " (%s)" (Export.interval_to_string i) in
    Printf.sprintf "%s : %s%s%s ; %s" x (Type.print t) i e str) m ""

let fresh ?(seed = "x") m =
  check_valid seed ;
  let seed = if seed = "" then "_" else seed in
  if SMap.mem seed m then (
    let rec aux i =
      let x = Printf.sprintf "%s%i" seed i in
      if SMap.mem x m then aux (1 + i)
      else x in
    aux 0
  ) else seed

let create () = SMap.empty

(* Perform some sanity-checks on an expression definition. *)
let check_expr = function
  | None -> ()
  | Some (xs, _e) ->
    (* Checking that there is no duplication of variable names. *)
    let xs' = List.sort_uniq compare xs in
    assert (List.length xs = List.length xs')

let define m x t e =
  check_valid x ;
  if SMap.mem x m then None
  else (
    check_expr e ;
    Some (SMap.add x (t, (None, None), e) m)
  )

let get_type m x =
  check_valid x ;
  match SMap.find_opt x m with
  | None -> None
  | Some (t, _i, _e) -> Some t

let get_value m x =
  check_valid x ;
  match SMap.find_opt x m with
  | None -> None
  | Some (_t, _i, None) -> None
  | Some (_t, _i, Some e) -> Some e

let set_interval m x i =
  check_valid x ;
  match SMap.find_opt x m with
  | None -> failwith ("Unknown variable " ^ x ^ ".")
  | Some (t, i', e) ->
    let i = Interval.inter i i' in
    if Interval.is_empty i then None
    else Some (SMap.add x (t, i, e) m)

(* Like set_interval, but without the consistency check. *)
let force_set_interval m x i =
  check_valid x ;
  match SMap.find_opt x m with
  | None -> failwith ("Unknown variable " ^ x ^ ".")
  | Some (t, i', e) ->
    let i = Interval.inter i i' in
    SMap.add x (t, i, e) m

let get_interval m x =
  check_valid x ;
  match SMap.find_opt x m with
  | None -> failwith ("Unknown variable " ^ x ^ ".")
  | Some (_t, i, _e) -> i

