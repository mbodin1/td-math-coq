
(* Un type pour les nombres 10-adiques.
  Sa valeur est mantisse × 10^exponent, mais a typiquement été entré comme « 123,456 » ou
  « 1_234.5 » ou variantes. *)
type t = {
    mantisse : Bigint.t ;
    exponent : Bigint.t
  }

(* Des nombres 10-adiques correspondant à 0, 1, 2, 3, et 10. *)
val zero : t
val one : t
val two : t
val three : t
val ten : t

(* Un très grand nombre 10-adique (arbitraire). *)
val large : t

(* Les nombres 10-adiques ont plusieurs représentations.
  Cette fonction teste l'égalité à 0 (qui n'est pas forcément exactement la même
  que le zero ci-dessus). *)
val is_zero : t -> bool

(* De même, teste l'égalité à 1. *)
val is_one : t -> bool

(* Teste l'égalité entre deux 10-adiques. *)
val eq : t -> t -> bool

(* Teste si le nombre donné est entier. *)
val integer : t -> bool

(* Teste si le nombre donné est entier et pair. *)
val even : t -> bool

(* Retourne un nombre identique, mais normalisé. *)
val normalise : t -> t

(* Addition *)
val add : t -> t -> t

(* Opposé *)
val opp : t -> t

(* Soustraction *)
val sub : t -> t -> t

(* Multiplication *)
val mult : t -> t -> t

(* Valeur absolue *)
val abs : t -> t

(* Retourne true si le nombre est positif. *)
val sign : t -> bool

(* Élève un nombre à la puissance. *)
val power_int : t -> int -> t

(* Essaye de traduire un nombre en entier. *)
val to_int : t -> int option

