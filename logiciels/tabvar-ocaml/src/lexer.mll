{ (** Module Lexer. **)

  open Lexing
  open Parser

  exception SyntaxError of string

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = lexbuf.lex_curr_pos ; pos_lnum = 1 + pos.pos_lnum }

  let current_position lexbuf =
    let pos = lexbuf.lex_curr_p in
    (pos.pos_lnum, (pos.pos_cnum - pos.pos_bol + 1))

  let string_to_list s = List.init (String.length s) (String.get s)

  (* Removing spaces in a number *)
  let prepare_num s =
    let l = string_to_list s in
    let l = List.filter (fun c -> c >= '0' && c <= '9') l in
    String.of_seq (List.to_seq l)
}

let space = ' ' | '\t'
let newline = '\n' | '\r' | "\r\n"

let letter = ['a'-'z' 'A'-'Z']

let digit = ['0'-'9']
let digit_or_sep = digit (' ' | " " (* non-breakable space. *) | '_')*
let dec_sep = '.' | ',' | "·"

let ident = (letter | '_') (letter | digit | '_' | "'")*


rule read = parse

  | (("\\left"?) as l) space* (('\\'? ('(' | '[' | '{')) as p)  { OPAR (l ^ p) }
  | (("\\right"?) as r) space* (('\\'? (')' | ']' | '}')) as p) { CPAR (r ^ p) }

  | ','                         { COMMA }

  | '+'                         { PLUS }
  | '-' | "−" | "–"             { MINUS }
  | '*' | "×" | "\\times" | "·" { TIMES }
  | '/' | "÷"                   { DIVIDE }
  | '^'                         { EXP }

  | "⁰"                         { EXPV Tenadic.zero }
  | "¹"                         { EXPV Tenadic.one }
  | "²"                         { EXPV Tenadic.two }
  | "³"                         { EXPV Tenadic.three }
  | "⁴"                         { EXPV Bigint.{ mantisse = Bigint.of_int 4 ; exponent = zero } }
  | "⁵"                         { EXPV Bigint.{ mantisse = Bigint.of_int 5 ; exponent = zero } }
  | "⁶"                         { EXPV Bigint.{ mantisse = Bigint.of_int 6 ; exponent = zero } }
  | "⁷"                         { EXPV Bigint.{ mantisse = Bigint.of_int 7 ; exponent = zero } }
  | "⁸"                         { EXPV Bigint.{ mantisse = Bigint.of_int 8 ; exponent = zero } }
  | "⁹"                         { EXPV Bigint.{ mantisse = Bigint.of_int 9 ; exponent = zero } }

  | "∞" | "\\infty"             { INFINITY }

  | (digit_or_sep+) as num      { NUM Bigint.{ mantisse = of_string (prepare_num num) ; exponent = zero } }
  | ((digit_or_sep+) as num) dec_sep ((digit_or_sep+) as dec)
    { NUM Bigint.{
        mantisse = of_string (prepare_num (num ^ dec)) ;
        exponent = - of_int (String.length (prepare_num dec))
      } }

  | "Definition"                { DEFINITION }
  | "Variable" | "Variables"    { VARIABLE }
  | "Hypothesis" | "Hypotheses" { HYPOTHESIS }

  | "Lemma" | "Theorem" | "Property" | "Remark" | "Fact" | "Corollary" | "Proposition"
    { LEMMA }

  | "Proof"                     { PROOF }
  | "Qed" | "Admitted"          { QED }

  | "Print"                     { PRINT }
  | "Check"                     { CHECK }

  | "Require"                   { REQUIRE }
  | "Import"                    { IMPORT }
  | "Export"                    { EXPORT }

  | "."                         { DOT }
  | ":"                         { COLON }
  | ":="                        { DEF }

  | "forall"                    { FORALL }
  | "exists"                    { EXISTS }

  | "True"                      { TRUE }
  | "False"                     { FALSE }
  | "/\\" | "∧"                 { AND }
  | "\\/" | "∨"                 { OR }
  | '~' | "¬" | "￢"            { NOT }
  | "->" | "→" | "=>" | "⇒"     { IMPL }

  | "<"                         { LT }
  | ">"                         { GT }
  | "<=" | "≤"                  { LE }
  | ">=" | "≥"                  { GE }
  | "="                         { EQ }
  | "<>" | "≠"                  { NEQ }

  | ident as id                 { IDENT id }

  | "(*"                        { comment lexbuf ; read lexbuf }
  | space+                      { read lexbuf }
  | newline                     { next_line lexbuf ; read lexbuf }
  | eof                         { EOF }
  | _                           { raise (SyntaxError ("Unexpected character: `" ^
                                    lexeme lexbuf ^ "'.")) }

and comment = parse         

  | "(*"                    { comment lexbuf ; comment lexbuf }
  | "*)"                    { () }

  | newline                 { next_line lexbuf ; comment lexbuf }
  | eof                     { raise (SyntaxError "Incomplete comment.") }
  | _                       { comment lexbuf }

