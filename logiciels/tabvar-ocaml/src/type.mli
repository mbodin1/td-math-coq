
(* Representation of the types used in this program. *)
type t

(* Print a type. *)
val print : t -> string

(* Get the list of arguments types of a function (or empty list for a scalar) and its return type.
  For scalar types, the list is empty and the type is returned as-is. *)
val get_fun : t -> t list * t

(* Given an arity of real numbers arguments, return the type of functions from reals to reals. *)
val from_arity : int -> t

(* The scalar type of real numbers. *)
val real : t

(* The type of properties. *)
val property : t

