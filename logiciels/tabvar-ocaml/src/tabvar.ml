(* Fichier principal *)

open Js_of_ocaml

(* Codes de retours des différentes fonctions de l'API. *)
let ok = Js.number_of_float 2.
let parse_error_code = Js.number_of_float 1.1
let type_error_code = Js.number_of_float 1.2
let already_defined_error_code = Js.number_of_float 1.3
let already_initialised_error_code = Js.number_of_float 1.4
let inconsistent_error_code = Js.number_of_float 1.5
let not_defined_code = Js.number_of_float 1.6
let unknown_error = Js.number_of_float 1.9

(* Création de tableaux JavaScript à types hétérogènes, pour le retour de valeurs. *)
let array1 v = Js.array [|v|]
let array2 v1 v2 =
  let a = array1 v1 in
  Js.array_set a 1 (Obj.magic v2) ;
  a
let array3 v1 v2 v3 =
  let a = array2 v1 v2 in
  Js.array_set a 2 (Obj.magic v3) ;
  a
let array4 v1 v2 v3 v4 =
  let a = array3 v1 v2 v3 in
  Js.array_set a 3 (Obj.magic v4) ;
  a

(* Message d'erreur générique. *)
let error ?(err = unknown_error) e =
  array2 err (Js.string ("Error: " ^ Printexc.to_string e))

(* Erreur de lexing/parsing. *)
let parse_error kind (line, character) =
  let msg = Printf.sprintf "%s error at line %i, character %i" kind line character in
  let to_num i = Js.number_of_float (Float.of_int i) in
  array4 parse_error_code (Js.string msg) (to_num line) (to_num character)

(* Erreur de typage. *)
let type_error msg expression =
  array3 type_error_code (Js.string msg) (Js.string expression)

(* Variable déjà définie. *)
let already_defined_error x =
  let msg = Printf.sprintf "Error: %s is already defined." x in
  array3 already_defined_error_code (Js.string msg) (Js.string x)

(* Protège du code pour le faire renvoyer une erreur propre en cas de problème interne. *)
let protect k =
  try k ()
  with e -> error e

(* Lit une chaine de caractère JavaScript. *)
let parse parse_f str k =
  let str = Js.to_string str in
  let lexbuf = Lexing.from_string str in
  lexbuf.Lexing.lex_curr_p <- {
    Lexing.pos_fname = "" ;
    Lexing.pos_lnum = 1 ;
    Lexing.pos_bol = 0 ;
    Lexing.pos_cnum = 0
  } ;
  match parse_f Lexer.read lexbuf with
  | e -> k e
  | exception Parser.Error ->
    let p = Lexer.current_position lexbuf in
    parse_error "Parser" p
  | exception Lexer.SyntaxError msg ->
    let p = Lexer.current_position lexbuf in
    parse_error ("Lexer: " ^ msg) p

(* Convertit une chaine de caractères JavaScript en une expression. *)
let parse_expr env str k =
  parse Parser.expr_full str (fun e ->
    match Typer.check_expr env e with
    | e -> k e
    | exception Typer.Error (msg, e) -> type_error msg e)

(* Convertit une chaine de caractères JavaScript en une propriété. *)
let parse_prop env str k =
  parse Parser.property_full str (fun p ->
    match Typer.check_prop env p with
    | p -> k p
    | exception Typer.Error (msg, e) -> type_error msg e)

(* Lit une suite de commande dans une chaine de caractères JavaScript. *)
let parse_commands str k =
  parse Parser.command_full str k


(* Déclarer une variable. *)
let suppose_var env x t =
  protect (fun () ->
    let x = Js.to_string x in
    match Env.define env x t None with
    | None -> already_defined_error x
    | Some env ->
      let coq =
        Printf.sprintf "Variable %s : real." x in
      array3 ok env (Js.string coq))

(* Supposer une propriété vraie dans l'environnement. *)
let suppose_as env h p =
  protect (fun () ->
    parse_prop env p (fun p ->
      match Manipulate.suppose env p with
      | None ->
        array2 inconsistent_error_code (Js.string "This is not possible in the current environment.")
      | Some env ->
        let h = Env.fresh ~seed:h env in
        match Env.define env h Type.property None with
        | None -> assert false
        | Some env ->
          let coq =
            Printf.sprintf "Hypothesis %s : %s." h (Export.property_to_string p) in
          array3 ok env (Js.string coq)))


let initialised = ref false

let _ =
  Js.export "tabvar"
    (object%js

        (* Initialisation. *)
        method init_js =
          protect (fun () ->
            if !initialised then
              array2 already_initialised_error_code (Js.string "Already initialised")
            else (
              initialised := true ;
              array3 ok (Env.create ()) (Js.string "Require Import Tabvar.")
            ))

        (* Validation d'une expression, et affichage de l'expression parsée. *)
        method validate_js env e =
          protect (fun () ->
            parse_expr env e (fun e -> array2 ok (Js.string (Export.to_string e))))

        (* Conversion d'une expression en un nœud HTML affichable. *)
        method expr_to_dom_js env e =
          protect (fun () ->
            parse_expr env e (fun e -> array2 ok (Export.to_html e)))

        (* Entre deux expressions, déterminer la plus grande si c'est possible. *)
        method compare_values_js env e1 e2 =
          protect (fun () ->
            parse_expr env e1 (fun e1 ->
              parse_expr env e2 (fun e2 ->
                let e1 = Manipulate.unfold env e1 in
                let e2 = Manipulate.unfold env e2 in
                match Manipulate.compare_values env e1 e2 with
                | None -> array2 ok (Js.string "?")
                | Some Manipulate.Eq -> array2 ok (Js.string "=")
                | Some Manipulate.Lt -> array2 ok (Js.string "<")
                | Some Manipulate.Gt -> array2 ok (Js.string ">"))))

        (* Simplifier une expression en une expression équivalente sur son domaine de définition. *)
        method simplify_js env e =
          protect (fun () ->
            parse_expr env e (fun e ->
              let e = Manipulate.unfold env e in
              let e = Manipulate.simpl e in
              let e = Export.to_string e in
              array2 ok (Js.string e)))

        (* Développer une expression arithmétique en une expression équivalente. *)
        method develop_js env e =
          protect (fun () ->
            parse_expr env e (fun e ->
              let e = Manipulate.(simpl (develop (simpl (unfold env e)))) in
              let e = Export.to_string e in
              array2 ok (Js.string e)))

        (* Dériver une expression selon une variable. *)
        method deriv_js env e x =
          protect (fun () ->
            let x = Js.to_string x in
            parse_expr env e (fun e ->
              let e = Manipulate.unfold env e in
              let e' = Manipulate.deriv x e in
              let e' = Export.to_string e' in
              array2 ok (Js.string e')))

        (* Remplace x par ex dans l'expression e. *)
        method replace_js env e x ex =
          protect (fun () ->
            let x = Js.to_string x in
            parse_expr env e (fun e ->
              parse_expr env ex (fun ex ->
                let env_only_x =
                  match Env.(define (create ()) x Type.(from_arity 0) (Some ([], ex))) with
                  | None -> failwith (Printf.sprintf "Can't define singleton environment for %s." x)
                  | Some env -> env in
                let e = Manipulate.unfold env_only_x e in
                array2 ok (Js.string (Export.to_string e)))))

        (* Déterminer (une sur-approximation de) l'interval d'une expression. *)
        method interval_js env e =
          protect (fun () ->
            parse_expr env e (fun e ->
              let i = Manipulate.interval env e in
              array2 ok (Js.string (Export.interval_to_string i))))

        (* Déclarer une variable. *)
        method suppose_var_js env x = suppose_var env x (Type.from_arity 0)
        method suppose_fun_js env x = suppose_var env x (Type.from_arity 1)

        (* Définir une fonction à un argument. *)
        method function_definition_js env f x e =
          protect (fun () ->
            let f = Js.to_string f in
            let x = Js.to_string x in
            match Env.define env x (Type.from_arity 0) None with
            | None -> already_defined_error x
            | Some env_with_x ->
              parse_expr env_with_x e (fun e ->
                match Env.define env f (Type.from_arity 1) (Some ([x], e)) with
                | None -> already_defined_error f
                | Some env ->
                  let coq =
                    Printf.sprintf "Definition %s (%s : real) := %s." f x (Export.to_string e) in
                  array3 ok env (Js.string coq)))

        (* Renvoit une variable fraiche dans l'environnement. *)
        method fresh_js env x =
          protect (fun () ->
            let x = Js.to_string x in
            let x = Env.fresh ~seed:x env in
            array2 ok (Js.string x))

        (* Afficher l'environnement actuel (pour aider le déboguage). *)
        method print_env_js env =
          protect (fun () -> array2 ok (Js.string (Env.print env)))

        (* Supposer une propriété vraie dans l'environnement. *)
        method suppose_js env p = suppose_as env "H" p
        method suppose_as_js env h p = suppose_as env (Js.to_string h) p

        (* Indique si une expression est bien définie (y) dans l'environnement, si elle n'est pas
          définie (n), ou si on ne sait pas (?). *)
        method defined_js env e =
          protect (fun () ->
            parse_expr env e (fun e ->
              match Manipulate.defined env e with
              | None -> array2 ok (Js.string "?")
              | Some true -> array2 ok (Js.string "y")
              | Some false -> array2 ok (Js.string "n")))

        (* Étant donné une variable x et une expression e qui dépend de cette variable, renvoit
          un tableau de valeur de x potentiellement intéressantes (par exemple des valeurs interdites
          ou des points d'annulation). *)
        method interesting_values_js env x e =
          protect (fun () ->
            let x = Js.to_string x in
            parse_expr env e (fun e ->
              let l = Manipulate.remarkable env x e in
              let l = List.map Manipulate.simpl l in
              let l = List.filter (fun e -> Manipulate.defined env e <> Some false) l in
              let l = List.map Export.to_string l in
              let l = List.sort_uniq compare l in
              let l = List.map Js.string l in
              array2 ok (Js.array (Array.of_list l))))

        (* Exécute des commandes comme si on les avait donné à l'assistant de preuve. *)
        method proof_assistant_js env cmd =
          protect (fun () ->
            parse_commands cmd (fun commands ->
              match
                List.fold_left (fun current command ->
                  match current with
                  | Either.Right return -> Either.Right return
                  | Either.Left (env, messages) ->
                    let open ProofAssistant in
                    match command with
                    | Definition (f, args, e) ->
                      (match
                        List.fold_left (fun current a ->
                          match current with
                          | Either.Right x -> Either.Right x
                          | Either.Left env ->
                            match Env.define env a (Type.from_arity 0) None with
                            | None -> Either.Right a
                            | Some env -> Either.Left env) (Either.Left env) args with
                       | Either.Right x ->
                         Either.Right (already_defined_error x)
                       | Either.Left env_with_args ->
                         match Typer.check_expr env_with_args e with
                         | exception Typer.Error (msg, e) -> Either.Right (type_error msg e)
                         | e ->
                           let t = Type.from_arity (List.length args) in
                           match Env.define env f t (Some (args, e)) with
                           | None -> Either.Right (already_defined_error f)
                           | Some env ->
                             let msg = Printf.sprintf "%s has been successfully defined." f in
                             Either.Left (env, msg :: messages))
                    | Variable (x, t) ->
                      (match Env.define env x t None with
                       | None -> Either.Right (already_defined_error x)
                       | Some env ->
                         let msg = Printf.sprintf "%s has been successfully assumed." x in
                         Either.Left (env, msg :: messages))
                    | Hypothesis (h, p) ->
                      (match Env.define env h Type.property None with
                       | None -> Either.Right (already_defined_error h)
                       | Some env ->
                         match Manipulate.suppose env p with
                         | None ->
                           let msg =
                             Printf.sprintf
                               "Hypothesis %s is incompatible with the current environment." h in
                           Either.Right (array2 inconsistent_error_code (Js.string msg))
                         | Some env ->
                           let msg = Printf.sprintf "%s has been successfully assumed." h in
                           Either.Left (env, msg :: messages))
                    | ProvenLemma (h, p) ->
                      (match Env.define env h Type.property None with
                       | None -> Either.Right (already_defined_error h)
                       | Some env ->
                         let env = Manipulate.force_suppose env p in
                         let msg = Printf.sprintf "%s has been successfully defined." h in
                         Either.Left (env, msg :: messages))
                    | Print x ->
                      (match Env.get_value env x with
                       | None ->
                         let msg = Printf.sprintf "%s is not a defined object." x in
                         Either.Right (array2 not_defined_code (Js.string msg))
                       | Some (args, body) ->
                         let msg =
                           let args =
                             List.map (fun x ->
                               let ty = Type.print (Type.from_arity 0) in
                               Printf.sprintf "(%s : %s)" x ty) args in
                           let fun_part =
                             if args = [] then "" else
                               Printf.sprintf "fun %s => " (String.concat " " args) in
                           let ty = Type.from_arity (List.length args) in
                           Printf.sprintf "%s = %s%s : %s" x fun_part (Export.to_string body) (Type.print ty) in
                         Either.Left (env, msg :: messages))
                    | Check e ->
                      let ty = Typer.typecheck env e in
                      let msg =
                        Printf.sprintf "%s : %s" (Export.to_string e) (Type.print ty) in
                      Either.Left (env, msg :: messages)) (Either.Left (env, [])) commands with
              | Either.Right return -> return
              | Either.Left (env, messages) ->
                array3 ok env (Js.array (Array.of_list List.(map Js.string (rev messages))))
              | exception Typer.Error (msg, e) -> type_error msg e))

     end)

