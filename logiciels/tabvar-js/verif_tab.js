
//Permet les vérifications et les validations du tableau de variation mais aussi l’affichage des validation et des erreurs dues à ce dernier.
function checkAll(){
    try {
				var ret ;
				var variable_name = document.getElementById("var-name") ;
				var function_name = document.getElementById("function-name") ;
        clearMessage("messageBox2Area");
        clearMessage("values-display");

				if (variable_name.value === "") variable_name.value = "x" ;
				if (function_name.value === "") function_name.value = "f" ;

				variable_name = variable_name.value ;
				function_name = function_name.value ;

				ret = tabvar.suppose_fun (environnement_assistant, function_name) ;
				if (ret[0] === retour_ok || ret[0] === 1.3 /* Already defined */) {
					ret = tabvar.suppose_fun (ret[1], function_name + "'") ;

					if (ret[0] === retour_ok || ret[0] === 1.3 /* Already defined */) {
						setClassExplanation (document.getElementById ("function-name-container"), {
							css: "verif-name-ok",
							explain: ""
						}) ;

						ret = tabvar.suppose_var (ret[1], variable_name) ;

						if (ret[0] === retour_ok || ret[0] === 1.3 /* Already defined */) {
							setClassExplanation (document.getElementById ("var-name-container"), {
								css: "verif-name-ok",
								explain: ""
							}) ;
						} else {
							setClassExplanation (document.getElementById ("var-name-container"), {
								css: "verif-name-bad",
								explain: "Erreur lors de la déclaration de la variable : " + ret[1]
							}) ;
						}
					} else {
						setClassExplanation (document.getElementById ("function-name-container"), {
							css: "verif-name-bad",
							explain: "Erreur lors de la déclaration de la dérivée de la fonction : " + ret[1]
						}) ;
					}
				} else {
					setClassExplanation (document.getElementById ("function-name-container"), {
						css: "verif-name-bad",
						explain: "Erreur lors de la déclaration de la fonction : " + ret[1]
					}) ;
				}

        ret = validation_funMain();
        if(ret[0] === retour_ok){
						environnement_assistant = ret[1];
						setClassExplanation (document.getElementById ("function-expr-container"), {
								css: "verif-fun-ok",
								explain: "L'expression est valide."
							}) ;

            ret = validation_funDeriv(environnement_assistant);
            if(ret[0] === retour_ok){
                ret = verification_funDeriv(environnement_assistant);
								if (ret[0] === retour_ok) {
									if (ret[1] === '='){
										setClassExplanation (document.getElementById ("function-derivation-container"), {
												css: "verif-fun-ok",
												explain: `Cette fonction est bien la dérivée de ${function_name}.`
											}) ;
									} else if (ret[1] === '?') {
										setClassExplanation (document.getElementById ("function-derivation-container"), {
												css: "verif-fun-unknown",
												explain: `Il n'a pas été possible de dire si cette fonction est bien la dérivée de ${function_name.value}.`
											}) ;
									} else {
										setClassExplanation (document.getElementById ("function-derivation-container"), {
												css: "verif-fun-bad",
												explain: `Cette fonction n'est pas la dérivée de ${function_name}.`
											}) ;
									}
								} else {
									setClassExplanation (document.getElementById ("function-derivation-container"), {
											css: "verif-fun-bad",
											explain: "Erreur lors de la validation : " + ret[1]
										}) ;
								}

                validation_subFun(environnement_assistant);
                compareValuesInRow(environnement_assistant);
                checkInnerCells(environnement_assistant);

            } else if(ret[0] === 1.1){
                message = "Le caractère "+ (ret[3]-1) + " de la dérivée n'est pas correct.";
								setClassExplanation (document.getElementById ("function-derivation-container"), {
										css: "verif-fun-bad",
										explain: message
									}) ;
            } else {
                message = ret[1];
								setClassExplanation (document.getElementById ("function-derivation-container"), {
										css: "verif-fun-bad",
										explain: message
									}) ;
            }
        } else {
						if (ret[0] === 1.02){
							setClassExplanation (document.getElementById ("function-expr-container"), {
									css: "verif-fun-ok",
									explain: "L'expression semble valide."
								}) ;

							message = "La dérivée ne peut pas etre lue ! (Erreur : " + ret + ")";

							var function_deriv = document.getElementById ("function-derivation") ;
							if (function_deriv !== null) {
								if (function_deriv.value === "")
									message = "Le corps de la dérivée est vide."
							}

							setClassExplanation (document.getElementById ("function-derivation-container"), {
									css: "verif-fun-bad",
									explain: message
								}) ;
						} else {
							message = "La fonction principale ne peut pas etre lue ! (Erreur : " + ret + ")";

							if (ret[0] !== 1.01)
								message = "Erreur lors de la déclaration de fonction : " + ret ;

							var function_expr = document.getElementById ("function-expr") ;
							if (function_expr !== null) {
								if (function_expr.value === "")
									message = "Le corps de la fonction principale est vide."
							}

							setClassExplanation (document.getElementById ("function-expr-container"), {
									css: "verif-fun-bad",
									explain: message
								}) ;

							setClassExplanation (document.getElementById ("function-derivation-container"), {
									css: "verif-fun-unknown",
									explain: "La fonction principale a un problème non résolu."
								}) ;
						}
        }
        return [retour_ok, "Vérification et validation : ok"];
    } catch(error){
        return [1.9, "Erreur interne: " + error];
    }
}

// Fonction qui permet de nettoyer la zone de texte

function clearMessage(elementId) {
    try {
        var areaMessageElement = document.getElementById(elementId);
        if (areaMessageElement !== null) {
            areaMessageElement.innerHTML = "";
            return [2, `Élément '${elementId}' nettoyé.`];
        } else {
            return [1.6, `Élément '${elementId}' inexistant.`];
        }
    } catch (error) {
        return [1.9, `Erreur interne avec l'ID '${elementId}': ` + error];
    }
}

// Gestion des erreurs fait par l'utilisateur dans le tableau de variation
function messageValidExpr(message_tab){
    try {
            if (message_tab[0] === retour_ok){
                messageDisplay("Expression valide : " + message_tab[message_tab.length-1]);
            }
            else{
                messageDisplay("Expression invalide.");
            }
            return [retour_ok, "Message expression : ok"];
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}

// Fonction qui permet de retourner un message dans la zone "messageBox2Area"
function messageDisplay(message) {
    try {
        var messageBox2 = document.getElementById("values-display");
        if(messageBox2 !== null){
            var p = document.createElement("p");
            p.textContent = message;
            messageBox2.appendChild(p);
            return [2 , "Création element et affichage : ok"];
        } else {
            return [1.6, "Élement inexistant."];
        }
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}

function throwIfFailure (ret, hint) {
	if (ret[0] !== retour_ok) throw ret
	else {
		if (hint !== undefined) ret.push (hint) ;
		return ret
	}
}

// Le bloc textuel ajouté automatiquement par l'interface pour définir la fonction textuellement.
var block_assistant_function_definition = null ;
var block_assistant_function_definition_deriv = null ;

// Permet de vérifier si l'expression de la fonction est validée par l'API.
function validation_funMain() {
    try {

				var deriv_var = document.getElementById("var-name") ;
				var function_name = document.getElementById("function-name") ;
				var function_expr = document.getElementById("function-expr") ;
				var function_deriv_expr = document.getElementById("function-derivation") ;

        if(deriv_var !== null && function_name !== null
					&& function_expr !== null && function_deriv_expr !== null){

						var name_var_tabvar = deriv_var.value;
						var function_name_tabvar = function_name.value;
						var function_main = function_expr.value;
						var function_deriv_main = function_deriv_expr.value;
						var proof_assistant_def =
							"Definition " + function_name_tabvar
								+ " (" + name_var_tabvar + " : real) :=" + "\n  "
								+ function_main + "." ;
						var proof_assistant_deriv_def =
							"Definition " + function_name_tabvar + "'"
								+ " (" + name_var_tabvar + " : real) :=" + "\n  "
								+ function_deriv_main + "." ;

						// D'abord, remettre un environnement valide.
						if (block_assistant_function_definition_deriv !== null
							&& block_assistant_function_definition_deriv.valid ()) {
							block_assistant_function_definition_deriv.update ("") ;
						} else if (block_assistant_function_definition_deriv !== null) {
								block_assistant_function_definition_deriv.remove () ;
								block_assistant_function_definition_deriv = null ;
						}

						if (block_assistant_function_definition !== null
							  && block_assistant_function_definition.valid ()) {
							block_assistant_function_definition.update ("") ; // Remet l'environnement d'avant.
						} else if (block_assistant_function_definition !== null) {
								block_assistant_function_definition.remove () ;
								block_assistant_function_definition = null ;
						}

						// Ensuite, tenter de définir la fonction f.
						var env = environnement_assistant ;
						try {
							var supp_func =
								throwIfFailure (tabvar.function_definition (env,
									function_name_tabvar, name_var_tabvar, function_main));
						} catch (error) {
							if (block_assistant_function_definition_deriv !== null){
									block_assistant_function_definition_deriv.remove () ;
									block_assistant_function_definition_deriv = null ;
							}

							if (block_assistant_function_definition !== null){
									block_assistant_function_definition.remove () ;
									block_assistant_function_definition = null ;
							}

							throw [1.01, error] ;
						}

						// Afficher textuellement la définition de la fonction f.
						if (block_assistant_function_definition !== null
							  && block_assistant_function_definition.valid ()) {
							block_assistant_function_definition.update (proof_assistant_def) ;
						} else {
							if (block_assistant_function_definition !== null)
								block_assistant_function_definition.remove () ;

							block_assistant_function_definition =
								addInterfaceCode (proof_assistant_def, env,
									function () { block_assistant_function_definition = null }) ;
						}

						// Puis sa dérivée.
						try {
							var env_with_f = supp_func[1] ;
							supp_func =
								throwIfFailure (tabvar.function_definition (env_with_f,
									function_name_tabvar + "'", name_var_tabvar, function_deriv_main));
						} catch (error) {
							if (block_assistant_function_definition_deriv !== null){
									block_assistant_function_definition_deriv.remove () ;
									block_assistant_function_definition_deriv = null ;
							}

							throw [1.02, error] ;
						}

						// Afficher textuellement la définition de la dérivée.
						environnement_assistant = env_with_f ;
						if (block_assistant_function_definition_deriv !== null
								&& block_assistant_function_definition_deriv.valid ()) {
							block_assistant_function_definition.update (proof_assistant_deriv_def) ;
						} else {
							if (block_assistant_function_definition_deriv !== null)
								block_assistant_function_definition_deriv.remove () ;

							block_assistant_function_definition_deriv =
								addInterfaceCode (proof_assistant_deriv_def, env,
									function () { block_assistant_function_definition_deriv = null }) ;
						}

						// Mettre à jour l'environnement avec les bonnes fonctions.
						environnement_assistant = supp_func[1] ;

						return supp_func ;
        } else {
            return [1.6, "Élement inexistant."];
        }
    } catch (error) {
				if (error[0] === 1.01 || error[0] === 1.02) return error ;
        return [1.9, "Erreur interne: " + error];
    }
}

// Permet de vérifier si la dérivé de la fonction entrée par l'utilisateur est validée par l'API.
function validation_funDeriv(env) {
    try {
			var deriv_var = document.getElementById("var-name") ;
			var function_derivation = document.getElementById("function-derivation") ;
        if(deriv_var !== null && function_derivation !== null){
						var name_var_tabvar = deriv_var.value;
            var fun_deriv_user = function_derivation.value;
						var env = throwIfFailure (tabvar.suppose_var (env, name_var_tabvar))[1] ;
            return tabvar.validate(env, fun_deriv_user);
						// pas de retour de env : on a modifié l'environnement seulement pour ajouter x temporairement.
        } else {
            return [1.6, "Élement inexistant."];
        }
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}

// Vérifie si la dérivée de la fonction est correcte par rapport à la fonction principale.
function verification_funDeriv(env){
    try {

				var deriv_var = document.getElementById("var-name") ;
				var function_expr = document.getElementById("function-expr") ;
				var function_derivation = document.getElementById("function-derivation") ;

        if(deriv_var !== null
					 && function_expr !== null
					 && function_derivation !== null){

            var name_var_tabvar = deriv_var.value;
            var function_main = function_expr.value;
            var fun_deriv_user = function_derivation.value;

						var env =
							throwIfFailure (tabvar.suppose_var (env, name_var_tabvar),
								"Supposition de la variable")[1] ;

            var fun_deriv_tabvar =
							throwIfFailure (tabvar.deriv(env, function_main, name_var_tabvar),
								"calcul de la dérivée")[1];

            var compare_deriv =
							tabvar.compare_values (env,
								throwIfFailure (tabvar.develop(env, fun_deriv_user),
									"Développement de la dérivée donnée")[1],
								throwIfFailure (tabvar.develop(env, fun_deriv_tabvar),
									"Développement de la dérivée calculée")[1]);

            if (compare_deriv[0] === retour_ok){
							return [retour_ok, compare_deriv[1]] ;
            } else {
                messageDisplay(compare_deriv[1]);
                return compare_deriv;
            }

        } else {
            return [1.6, "Élement inexistant."];
        }
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}

// Permet de valider les sous fonctions existantes et de les mettre dans le tableau subFun.
// Associe une classe CSS à chaque cellule en fonction de leur validité.
function validation_subFun(env) {
    try {
				var deriv_var = document.getElementById("var-name") ;
        var tableau = document.getElementById("tableauvar");
        if (deriv_var !== null && tableau !== null) {
						var name_var_tabvar = deriv_var.value;

						var env = throwIfFailure (tabvar.suppose_var (env, name_var_tabvar))[1] ;

            var subFun = []; // Tableau des sous-fonctions
            for (var i = 1; i < tableau.rows.length; i++) {
                var row = tableau.rows[i];

                var colorCell = row.cells[0];
								var text = getCellContent (colorCell) ;

                // Validation de la sous-fonction
                if (tabvar.validate(env, text)[0] === retour_ok) {
										// Adding a scope to avoid mixing each iteration of the variables's values.
										(function (text) {
											setClassExplanation (colorCell, {
													css: "verif-fun-ok",
													explain: "L'expression est valide.",
													check: function (fresh){
														const def =
															"Definition " + fresh ("f") + " (" + name_var_tabvar + " : real) :=\n  "
															+ text + "." ;
														return def ;
													}
												});
										}) (text) ;
                    subFun.push(text);
                } else {
										setClassExplanation (colorCell,
											{ css: "verif-fun-bad", explain: "L'expression n'est pas valide." });
                }
            }
            messageDisplay(subFun);
            return [2, "Sous-fonction(s) validée(s)."];
        } else {
            return [1.6, "Élément inexistant."];
        }
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}


// Fonction pour comparer les valeurs dans la première ligne du tableau.
function compareValuesInRow(env) {
    try {
        var values = getValues(1);
        tableau = document.getElementById("tableauvar");
        if(tableau !== null){

            var cell = tableau.rows[0].cells[1];
						// La premiere valeur est celle de reference donc toujours bien placée.
            if (tabvar.validate(env, values[0])[0] === retour_ok) {
								setClassExplanation (cell,
									{ css: "verif-val-ok",
										explain: "L'expression est valide.",
										check: function (fresh){
											const def =
												"Definition " + fresh ("lim") + " :=\n  "
												+ values[0] + "." ;
											return def ;
										}
									});
						} else {
								setClassExplanation (cell,
									{ css: "verif-val-bad", explain: "L'expression n'est pas valide." });
						}

            for (var i = 0; i < values.length; i++) {
                var result = tabvar.compare_values(env, values[i], values[i+1]);
								if (result[0] !== retour_ok) result[1] = "bad" ;
                cell = tableau.rows[0].cells[i*2 + 3];
								var check ;
								// Adding a scope.
								(function (value_before, comp, value_after){
									check =
										function (fresh){
											var fresh_name = fresh ("comparison") ;
											var lemma =
												"Lemma " + fresh_name + " : " + value_before + " " + comp + " " + value_after + "." ;
											lemma += "\nProof. Qed." ;
											return lemma ;
										} ;
								})(values[i], result[1], values[i+1]) ;
								switch (result[1]){
									case ">":
										setClassExplanation (cell,
											{ css: "verif-val-bad",
												explain: "La valeur semble mal placée.",
												check: check
											}) ;
									break;
									case "<":
										setClassExplanation (cell,
											{ css: "verif-val-ok",
												explain: "La valeur semble bien placée.",
												check: check
											});
									break;
									case "?":
										setClassExplanation (cell,
											{ css: "verif-val-unknown",
												explain: "Il manque des informations pour vérifier que la position de cette valeur est correcte." });
									break;
									case "=":
										setClassExplanation (cell,
											{ css: "verif-val-eq",
												explain: "Cette valeur est la même que la valeur précédente.",
												check: check
											});
									break;
									default:
										if (tabvar.validate(env, values[i + 1])[0] !== retour_ok) {
											setClassExplanation (cell,
												{ css: "verif-val-veryBad", explain: "L'expression n'est pas valide." });
										} else if (tabvar.validate(env, values[i])[0] !== retour_ok) {
											setClassExplanation (cell,
												{ css: "verif-val-unknown", explain: "L'expression précédente est mal formée." }) ;
										} else {
											setClassExplanation (cell,
												{ css: "verif-val-veryBad", explain: "Quelque chose d'imprévu est arrivé." });
										}
                }
            }
            return [2, "Les valeurs ont été comparées."]
        } else {
            return [1.6, "Élement inexistant."];
        }
    } catch (error) {
        return [1.9, "Erreur interne: " + error];
    }
}

// Vérifie une question étant donné une hypothèse dans un environnement.
// Retourne un object tel qu'attendu par setClassExplanation.
function checkCondition (env, variable, analysed_function, condition, hypotheses, question) {
	if (env === null)
		return {
			css: "verif-assert-unknown",
			explain: "Environnement problématique."
		};
	var supp = tabvar.suppose (env, condition) ;
	if (supp[0] !== retour_ok)
		return {
			css: "verif-assert-unknown",
			explain: `Erreur en supposant l'hypothèse ${condition} : ${supp}`
		};
	var env_base = supp[1] ;

	var env = env_base ;
	var valid_hypotheses = [] ;
	for (var i = 0; i < hypotheses.length; i++){
		supp = tabvar.suppose (env, hypotheses[i]) ;
		if (supp[0] === retour_ok) {
			valid_hypotheses.push (hypotheses[i]) ;
			env = supp[1] ;
		}
	}

	var info_minimal = function (goal){
		var ret = tabvar.minimal_hypotheses (env_base, valid_hypotheses, goal) ;
		if (ret[0] !== retour_ok) return ["", ""] ;
		var hyps = ret[2] ;
		if (hyps.length === 0) return ["", ""] ;

		if (hyps.length === 1) {
			return [
				("\n\nL'hypothèse suivante a été utilisée pour arriver à cette conclusion : "
				+ valid_hypotheses[hyps[0]]),
				valid_hypotheses[hyps[0]]
			] ;
		}

		var message = "\n\nLes hypothèses suivantes ont été utilisées pour arriver à cette conclusion : " ;
		var pred = "" ;
		for (var i = 0; i < hyps.length; i++) {
			if (i !== 0) {
				message += ", " ;
				pred += " /\\ " ;
			}
			message += valid_hypotheses[hyps[i]] ;
			pred += valid_hypotheses[hyps[i]] ;
		}
		return [message, pred] ;
	}

	switch (question){
		case "pos":
		case "neg":
			var test = tabvar.is_positive (env, analysed_function) ;
			if (test[0] !== retour_ok)
				return { css: "verif-assert-unknown", explain: `Erreur lors de l'analyse de signe : ${test}` };
			var TrueSign = test[1] ;
			var statement = 
				(analysed_function + " " + (TrueSign ? "> 0" : "< 0"))
				+ " \\/ " + analysed_function + " = 0" ;
			var [msg_hyps, hyps] = (TrueSign !== "?") ? info_minimal (statement) : ["", ""] ;

			var check = function (fresh) {
					var fresh_name_lemma =
						fresh ((TrueSign ? "positive_" : "negative_")
							+ analysed_function.replace (/[^a-zA-Z_0-9]/g, "_")) ;
					var lemma =
						"Lemma " + fresh_name_lemma + " : forall " + variable + ",\n  "
						+ condition + (hyps === "" ? "" : (" /\\ " + hyps)) + " ->\n  "
						+ statement + "." ;
					lemma += "\nProof. Qed." ;
					return lemma ;
				} ;
			if ( (TrueSign === true && question === "pos")
					|| (TrueSign === false && question === "neg") )
				return {
					css: "verif-assert-ok",
					explain: "Suffisament d'informations ont été fournies pour vérifier ce résultat." + msg_hyps,
					check: check
				};
			else if ( (TrueSign === true && question === "neg")
					|| (TrueSign === false && question === "pos") )
				return {
					css: "verif-assert-bad",
					explain: "Le signe de l'expression ici ne semble pas être correct." + msg_hyps,
					check: check
					};
			else {
				console.assert (TrueSign === "?") ;
				return {
					css: "verif-assert-unknown",
					explain: "Il manque des informations pour valider ce signe."
				} ;
			}
			break;
		case "idkPN":
			return { css: "verif-assert-void", explain: "Rien n'est à vérifier ici." };
			break;
		case "VI":
			var test = tabvar.defined (env, analysed_function) ;
			if (test[0] !== retour_ok)
				return { css: "verif-assert-unknown", explain: `Erreur lors de l'analyse de l'expression : ${test}` };
			var defined = test[1];
			if (defined)
				return { css: "verif-assert-bad", explain: `L'expression ${analysed_function} semble bien valide ici.` }; // TODO: Ajouter une commande Check pour vérifier ?
			else return { css: "verif-assert-ok", explain: `L'expression ${analysed_function} n'est effectivement pas définie ici.` };
			break;
		case "NI":
			return { css: "verif-assert-unknown", explain: "Pas encore implémenté." }; // TODO: On a besoin de la valeur de la case d'à côté.
			break;
		case "idkVI0NI":
			return { css: "verif-assert-void", explain: "Rien n'est à vérifier ici." };
			break;
		case "up":
		case "down":
		case "flat":
			var ret = tabvar.deriv (env, analysed_function, variable) ;
			if (ret[0] === retour_ok) {
				var d = ret[1] ;

				var test = tabvar.compare_values (env, d, "0") ;

				if (test[0] !== retour_ok)
					return {
						css: "verif-assert-unknown",
						explain: "Erreur lors de l'analyse de la fonction : " + test[1]
					};
				var variation = test[1] ;

				var statement = d + " " + variation + " 0" ;
				var [msg_hyps, hyps] = (variation !== "?") ? info_minimal (statement) : ["", ""] ;

				if ( (variation === ">" && question === "up")
						|| (variation === "<" && question === "down")
						|| (variation === "=" && question === "flat") )
					return {
						css: "verif-assert-ok",
						explain: "Suffisament d'informations ont été fournies pour vérifier ce résultat." + msg_hyps,
					} ;
				else if (variation === "?")
					return {
						css: "verif-assert-unknown",
						explain: "Il manque des informations pour valider cette variation."
					} ;
				else
					return {
						css: "verif-assert-bad",
						explain: "Il semblerait que cette variation soit incorrecte." + msg_hyps,
					} ;
			} else {
				return {
					css: "verif-assert-unknown",
					explain: "Erreur lors de l'analyse de la fonction : " + ret[1]
				};
			}
			break;
		case "idkUD":
			return { css: "verif-assert-void", explain: "Rien n'est à vérifier ici." };
			break;
		case "0":
		default:
			var test = tabvar.compare_values (env, analysed_function, question) ;
			if (test[0] !== retour_ok)
				return { css: "verif-assert-unknown", explain: `Erreur lors de la comparaison : ${test}` };

			var comp = test[1] ;
			var statement = analysed_function + " " + comp + " " + question ;
			var [msg_hyps, hyps] = (comp !== "?") ? info_minimal (statement) : ["", ""] ;

			var check = function (fresh) {
					var fresh_name_lemma =
						fresh ("value_" + analysed_function.replace (/[^a-zA-Z_0-9]/g, "_")) ;
					var lemma =
						"Lemma " + fresh_name_lemma + " : forall " + variable + ",\n  "
						+ condition + (hyps === "" ? "" : (" /\\ " + hyps)) + " ->\n  "
						+ statement + "." ;
					lemma += "\nProof. Qed." ;
					return lemma ;
				} ;
			if (comp === "=")
				return {
					css: "verif-assert-ok",
					explain: `L'expression ${analysed_function} est effectivement égale à ${question} ici.` + msg_hyps,
					check: check
				};
			else if (comp === "<" || comp === ">")
				return {
					css: "verif-assert-bad",
					explain: `On a plutôt ${analysed_function} ${comp} ${question} ici.` + msg_hyps,
					check: check
				} ;
			else return {
				css: "verif-assert-unknown",
				explain: "Pas suffisamment d'informations ont été données pour vérifier ce résultat."
			} ;
	}
}

// Étant donné un object avec les attributs ci-dessous, met en forme une cellule du tableau:
// - css: La classe CSS correspondante à l'état de la cellule.
// - explain: Une explication de ce résultat.
// - check (optionnel): Une fonction générant une commande de l'assistant de preuve pour expliquer textuellement le résultat.
//		Cette fonction prend en argument une fonction fresh(seed) permettant de générer de nouveaux identifiants à l'aide d'une graine.
function setClassExplanation (cell, check) {
	cell.setAttribute ("class", check.css) ;

	var dropdown = getDropdownChild (cell) ;

	if (dropdown !== undefined){
		for (const div of dropdown.childNodes.values()){
			if (div.className === "dropdown-explanation") {

				// Clearing the current explanation.
				while (div.firstChild) {
					div.removeChild (div.lastChild);
					if (check.explain === "") div.style = "display: none ;" ;
					else div.style = "" ;
				}

				if (check.explain !== undefined){
					div.textContent = check.explain ;

					if (check.check !== undefined){
						var a = document.createElement ("a") ;
						a.setAttribute ("class", "explanation-link") ;
						a.textContent = "Voir" ;
						a.onclick = function () {
								var txt = check.check(function (seed) {
										if (seed === undefined) seed = "" ;
										var ret = tabvar.fresh (environnement_assistant, seed) ;
										if (ret[0] === retour_ok) {
											return ret[1] ;
										} else {
											return "H" ;
										}
									}) ;
								addInterfaceCode (txt, environnement_assistant) ;
							} ;
						div.appendChild (a) ;
					}
				}

				break ;
			}
		}
	}
}

// Vérifie les valeurs internes du tableau.
function checkInnerCells (env){

		// Le tableau de variation.
    var tableau = document.getElementById("tableauvar");
		// Le nom de la variable considérée.
    var variable = document.getElementById("first-cell").textContent;

		var ret0 = tabvar.suppose_var (env, variable) ;
		var env0 = null;
		if (ret0[0] === retour_ok) env0 = ret0[1];

		var var_values = getValues(1);

    for (var line = 1; line < tableau.rows.length; line++) {
			var row = tableau.rows[line];
			var kind = undefined;
			switch (row.class){
				case "signRow":
					kind = "sign";
					break;
				case "increaseRow":
					kind = "increase";
					break;
			}

			var analysed_function = getCellContent (row.cells[0]) ;

			for (var i = 0; i < var_values.length; i++){

					var LowBound = var_values[i];

					var cell_exact = row.cells[2*i + 1];
					if (cell_exact === undefined) break ;

					hypotheses = [] ;
					for (var y = 1; y < line; y++)
						hypotheses.push (getCellContent (tableau.rows[y])) ;

					setClassExplanation (cell_exact,
						checkCondition (env0, variable, analysed_function,
							variable + " = " + LowBound,
							hypotheses,
							convertTabValues (getCellContent (cell_exact)))) ;

					var cell_between = row.cells[2*i + 2];
					if (i < var_values.length - 1 && cell_between !== undefined) {
						var UpBound = var_values[i+1];
						var supposeLowBound = "( " + variable + " > " + LowBound + " )";
						var supposeUpBound = "( " + variable + " < " + UpBound + " )";
						setClassExplanation (cell_between,
							checkCondition (env0, variable, analysed_function,
								supposeLowBound + " /\\ " + supposeUpBound,
								hypotheses,
								convertTabValues (getCellContent (cell_between)))) ;
					}
			}
		}
}

function verif_hyp_sup( env, col, row) {
    var AllValue = [];
    var tableau = document.getElementById("tableauvar");

    for (var i = row - 1; i > 0; i--) {
        var mathExpression = transform_to_math(col, i);
        AllValue.push(mathExpression);
    }
    var envBis =env;
    if (col%2==0){
        envBis =
					throwIfFailure (tabvar.suppose(envBis,
						tableau.rows[0].cells[0].innerText.trim()
						+ ">" + tableau.rows[0].cells[col-1].innerText.trim()))[1];
        envBis =
					throwIfFailure (tabvar.suppose(envBis,
						tableau.rows[0].cells[0].innerText.trim()
						+ "<" + tableau.rows[0].cells[col+1].innerText.trim()))[1];
    } else {
        envBis =
					throwIfFailure (tabvar.suppose(envBis,
						tableau.rows[0].cells[0].innerText.trim()
						+ "=" + tableau.rows[0].cells[col].innerText.trim()))[1];
    }
    var min_hyp = tabvar.minimal_hypotheses(envBis, AllValue, transform_to_math(col, row));

    return min_hyp;
}


function transform_to_math(col, row) {
    var tableau = document.getElementById("tableauvar");
    var cellValue = tableau.rows[row].cells[col].innerText.trim(); // Récupère la valeur de la cellule courante
    var functionName = tableau.rows[row].cells[0].innerText.trim(); // Récupère le nom de la fonction à partir de la première colonne de la ligne
    var to_string = ""; // Initialisation de la variable de retour

    switch (convertTabValues (cellValue)) {
        case "VI":
            to_string = "True";
            break;
        case "NI":
            to_string = "True";
            break;
        case "0":
            to_string = functionName + " = 0";
            break;
        case "idkVI0NI":
            to_string = "True";
            break;
        case "idkPN":
            to_string = "True";
            break;
        case "pos":
            to_string = functionName + " > 0";
            break;
        case "neg":
            to_string =  functionName + " < 0";
            break;
        case "idkUD":
            to_string = "True";
            break;
        case "up":
            to_string = "True";
            break;
        case "down":
            to_string = "True";
            break;

        default:
            to_string = `False (*Inconnu (${cellValue})*)`;
            break;
    }

    return to_string; // Retourne l'expression mathématique
}

