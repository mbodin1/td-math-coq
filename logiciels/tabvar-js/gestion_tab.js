// Variable globale pour le mode
var isReaderMode = false;

// Fonction pour activer/désactiver le mode lecteur
function toggleReaderMode() {
    // TODO: chercher l'etat courant
    isReaderMode = !isReaderMode; // Inverser le mode

		var setupfield ;
		if (isReaderMode) {
			setupfield = function (input){
				input.setAttribute ("disabled", "disabled") ;
			}
		} else {
			setupfield = function (input){
				input.removeAttribute ("disabled") ;
			}
		}

		setupfield (document.getElementById("var-name")) ;
		setupfield (document.getElementById("function-name")) ;
		setupfield (document.getElementById("function-expr")) ;
		setupfield (document.getElementById("function-derivation")) ;

    var table = document.getElementById("tableauvar");
    table.setAttribute("class", isReaderMode ? "read-mode" : "edit-mode")

    var functions_definitions = document.getElementById("functions-definitions");
    functions_definitions.setAttribute("class", isReaderMode ? "read-mode" : "edit-mode")

    // Désactiver/activer les éléments d'interface utilisateur
    // var allInputs = document.querySelectorAll('button, .dropdown');
    // allInputs.forEach(function (element) {
    //     if (isReaderMode) {
    //         element.setAttribute('disabled', 'true'); // Désactive l'élément
    //     } else {
    //         element.removeAttribute('disabled'); // Réactive l'élément
    //     }
    // });

    if(isReaderMode){
				var ret = checkAll();
				if (ret[0] !== retour_ok) throw ret[1];
    }
    // Gérer les écouteurs d'événements
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// GESTION DES VALEURS /////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Fonction pour ajuster la taille de l'input en fonction de son contenu
function adjustInputWidth(input) {
    // Ajuste la largeur de l'élément input en fonction de la longueur de son contenu (nombre de caractères + 1) multipliée par 8 pixels
    input.style.width = ((input.value.length +1)*8) + 'px';
}

// Convertit le texte effectivement présent dans le tableau par un symbole utilisable dans le reste
// du programme (non soumis aux changements de représentations dans le tableau).
function convertTabValues(v){
	switch (v) {
		case "||":
			return "VI";
			break;
		case "∅":
			return "NI";
			break;
		case "0":
			return "0";
			break;
		case "…?":
			return "idkVI0NI";
			break;
		case "+":
			return "pos";
			break;
		case "-":
			return "neg";
			break;
		case "±?":
			return "idkPN";
			break;
		case "↘":
			return "down";
			break;
		case "↗":
			return "up";
			break;
		case "→":
			return "flat";
			break;
		case "⤮?":
			return "idkUD";
			break;
		default:
			return v;
	}
}

// Fonction qui permet d'afficher les valeurs qui se trouvent dans les lignes
function showValues() {
    // Récupère l'élément du tableau avec l'ID "tableauvar"
    var table = document.getElementById("tableauvar");
    if (!table) { // Vérifie si le tableau existe
        console.error("[1.6, Le tableau avec l'ID 'tableauvar' n'existe pas.]");
        return;  // Arrête la fonction si le tableau n'existe pas
    }

    // Vérifie que le tableau a au moins 3 lignes
    if (table.rows.length < 3) {
        console.error("[1.7, Le tableau doit contenir au moins 3 lignes.]");
        return; // Arrête la fonction si le tableau a moins de 3 lignes
    }

    // Initialise les variables pour les lignes du tableau
    var firstRow = table.rows[0];
    var beforeLastRow = null;
    var lastRow = null;

    // Recherche la ligne contenant la cellule avec l'ID "before-last-cell"
    for (var i = 0; i < table.rows.length; i++) {
        if (table.rows[i].querySelector('#before-last-cell')) {
            beforeLastRow = table.rows[i];
            break;
        }
    }

    // Vérifie si l'avant-dernière ligne a été trouvée
    if (!beforeLastRow) {
        console.error("[1.6, La cellule avec l'ID 'last-cell' n'a pas été trouvée.]");
        return;
    }

    // Recherche la ligne contenant la cellule avec l'ID "last-cell"
    for (var i = 0; i < table.rows.length; i++) {
        if (table.rows[i].querySelector('#last-cell')) {
            lastRow = table.rows[i];
            break;
        }
    }
    // Vérifie si la dernière ligne a été trouvée
    if (!lastRow) {
        console.error("[1.6, La cellule avec l'ID 'before-last-cell' n'a pas été trouvée.]");
        return;
    }

    // Récupère les cellules de chaque ligne sélectionnée
    var cells1 = firstRow.cells;
    var cells2 = beforeLastRow.cells;
    var cells3 = lastRow.cells;

    // Initialise une variable pour stocker les valeurs
    var values1 = [];
    var values2 = [];
    var values3 = [];

    // Parcourt les cellules de la première ligne et récupère les valeurs aux index impairs (1, 3, 5, ...)
    for (let i = 1; i < cells1.length; i += 2) {
        if (cells1[i]) {
            values1.push(getCellContent (cells1[i]));
        } else {
            console.error("[1.6, Cellule manquante dans la première ligne à l'index " + i + ".]");
        }
    }

    for (let i = 2; i < cells2.length - 1; i++) {
        if (cells2[i]) {
						values2.push(convertTabValues (getCellContent (cells2[i]))) ;
        } else {
            console.error("[1.6, Cellule manquante dans l'avant-dernière ligne à l'index " + i + ".]");
        }
    }

    for (let i = 2; i < cells3.length; i += 2) {
        if (cells3[i]) {
						values3.push(convertTabValues ( getCellContent (cells3[i]))) ;
        } else {
            console.error("[1.6, Cellule manquante dans la dernière ligne à l'index " + i + ".]");
        }
    }

    var displayContainer = document.getElementById("values-display");
    displayContainer.innerHTML = ""; // Efface le contenu précédent

    // Crée et ajoute des éléments pour afficher les valeurs
    // var values1Elem = document.createElement("p");
    // values1Elem.textContent = "Valeurs de la premiere ligne :  " + values1.join(", ");
    // displayContainer.appendChild(values1Elem);

    // var values2Elem = document.createElement("p");
    // values2Elem.textContent = "Valeurs de l'avant derniere ligne : " + values2.join(", ");
    // displayContainer.appendChild(values2Elem);

    // var values3Elem = document.createElement("p");
    // values3Elem.textContent = "Valeurs de la derniere ligne : " + values3.join(", ");
    // displayContainer.appendChild(values3Elem);

    return [values1, values2, values3];
}



function getAllRowsValues() {
    var table = document.getElementById("tableauvar");
    var allRowsValues = [];

    for (var i = 0; i < table.rows.length; i++) {
        var rowValues = [];
        var row = table.rows[i];
        for (var j = 0; j < row.cells.length; j++) {
            rowValues.push(row.cells[j].innerText);
        }
        allRowsValues.push(rowValues);
    }

    return allRowsValues;
}

function displayAllRowsValues() {
    var allValues = getAllRowsValues();
    var displayDiv = document.getElementById("values-display");
    displayDiv.innerHTML = "";

    for (var i = 0; i < allValues.length; i++) {
        var rowValues = allValues[i];
        displayDiv.innerHTML += "<p>Ligne " + (i + 1) + ": " + rowValues.join(", ") + "</p>";
    }
}


//Récupération des valeurs spécifiques selon l'index fourni.
function getValues(index) {
    var values = showValues();
    var result;

    // Erreur de validité des valeurs retournées par showValues()
    if (!Array.isArray(values) || values.length !== 3) {
        console.error("[1.5, Les valeurs retournées par showValues() sont invalides.]");
        return;
    }

    switch (index) {
        case 1:
            result = values[0];
            break;
        case 2:
            result = values[1];
            break;
        case 3:
            result = values[2];
            break;
        default:
            // Erreur de paramètres. Si index est invalide.
            console.error("[1.7, Erreur paramètres : 1 pour valeurs de x, 2 pour signe de la dérivée, 3 pour variations de la fonction.]");
            return;
    }
    return result;
}

//Fonction de sauvegarde des valeurs du tableau (Pour les lignes : 1ère, avant dernière, dernière)
function saveValuesToJSON() {
    try {
        var values = showValues();
        if (!values) {
            console.error("[1.9, Erreur interne : Les valeurs retournées par showValues() sont invalides ou non définies]");
            return;
        }

        var jsonData = JSON.stringify(values, null, 2);

        var blob = new Blob([jsonData], { type: 'application/json' });
        var url = URL.createObjectURL(blob);
        var a = document.createElement('a');
        a.href = url;
        a.download = 'tableValues.json';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    } catch (error) {
        console.error("[1.9, Erreur interne lors de la sauvegarde des valeurs en JSON] " + error.message);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// GESTION DES EVENEMENTS //////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function changeVarFun() {
    // Récupère les éléments input pour le nom de la variable et de la fonction
    var varNameInput = document.getElementById("var-name");
    var functionNameInput = document.getElementById("function-name");

    // Vérifier l'existence du champs d'entrée de nom de fonction et de variable
    if (!varNameInput || !functionNameInput) {
        console.error("[1.6, Erreur : Les éléments input avec les ID 'var-name' ou 'function-name' n'existent pas.]");
        return;
    }

    // Ajoute un écouteur d'événements pour détecter les changements dans la zone de texte pour le nom de la variable
    varNameInput.addEventListener("input", function() {
        // Récupère la valeur saisie dans la zone de texte
        var varName = varNameInput.value;

        // Vérifie que les éléments cibles existent
        var derivVar = document.getElementById("deriv-var");
        var firstCellSpan = document.getElementById("first-cell");
        var lastCellVar = document.getElementById("last-cell-var");
        var beforeLastCellVar = document.getElementById("before-last-cell-var");

        if (derivVar && firstCellSpan && lastCellVar && beforeLastCellVar) {
            // Met à jour le contenu de la cellule avec la nouvelle valeur
            derivVar.textContent = varName;
            firstCellSpan.textContent = varName;
            lastCellVar.textContent = varName;
            beforeLastCellVar.textContent = varName;
            adjustInputWidth(varNameInput);
        } else {
            console.error("[1.6, Erreur d'affichage : Un ou plusieurs éléments de destination pour 'var-name' n'existent pas.]");
        }
    });

    // Ajoute un écouteur d'événements pour détecter les changements dans la zone de texte pour le nom de la fonction
    functionNameInput.addEventListener("input", function() {
        // Récupère la valeur saisie dans la zone de texte
        var functionName = functionNameInput.value;

        // Vérifie que les éléments cibles existent
        var derivFun = document.getElementById("deriv-function");
        var lastCellFun = document.getElementById("last-cell-function");
        var beforeLastCellFun = document.getElementById("before-last-cell-function");

        if (derivFun && lastCellFun && beforeLastCellFun) {
            // Met à jour le contenu de la cellule avec la nouvelle valeur
            derivFun.textContent = functionName;
            lastCellFun.textContent = functionName;
            beforeLastCellFun.textContent = functionName;
            adjustInputWidth(functionNameInput);
        } else {
            console.error("[1.6, Erreur d'affichage : Un ou plusieurs éléments de destination pour 'function-name' n'existent pas.]");
        }
    });
}


function minusInfinity(link) {
    var valueElement = link.closest('.dropdown').querySelector('.value');
    valueElement.textContent = "-∞";
}

function plusInfinity(link) {
    var valueElement = link.closest('.dropdown').querySelector('.value');
    valueElement.textContent = "+∞";
}


// Fonction pour retirer les boutons existants dans une cellule
function removeDropdownFromCell(cell) {
    var dropdowns = cell.getElementsByClassName('dropdown');
    while (dropdowns.length > 0) {
        dropdowns[0].remove();
    }
}

/* // Cette fonction faisait des vérifications sur la borne haute du tableau de variation,
   // mais cette dernière n'est plus construite ainsi.
function realUpBound() {
    var upbound = document.getElementById("upBound");
    console.assert (upbound !== null);
    var upBoundText = upbound.innerText.trim();
    var table = document.getElementById("tableauvar");

    for (var i = 1 ; i<table.rows.length-1; i+=1){
        var row = table.rows[i];
        var lastCol = row.cells[row.cells.length-1];
        var lastRow = table.rows[table.rows.length-1];
        var lastRowLastcol = lastRow.cells[table.rows[1].cells.length-1];

        if (row.classList.contains('signRow')) {
            if (upBoundText === "+ ∞") {
                removeDropdownFromCell(lastCol);
            } else {
                if (!lastCol.querySelector('.dropdown')) {
                    lastCol.appendChild(createVIDropdownCell ());
                }
                if (!lastRowLastcol.querySelector('.dropdown')) {
	                lastRowLastcol.appendChild(createValueDropdownCell ());
                }
            }
        }
    }
}
*/

function realLowBound() {
    var lowBound = document.getElementById("lowBound") ;
		console.assert (lowBound !== null) ;
    var lowBoundText = lowBound.innerText.trim();
    var table = document.getElementById("tableauvar");

    for (var i = 1 ; i<table.rows.length-1; i+=1){
        var row = table.rows[i];
        var firstCol = row.cells[1];
        var lastRow = table.rows[table.rows.length-1];
        var lastRowFirstCol = lastRow.cells[1];

        if (row.classList.contains('signRow')) {
            if (lowBoundText === "- ∞") {
                removeDropdownFromCell(firstCol);
            } else {
                if (!firstCol.querySelector('.dropdown')) {
                    firstCol.appendChild(createVIDropdownCell ());
                }
                if (!lastRowFirstCol.querySelector('.dropdown')) {
                    // pas de valeur, à quoi ça correspond du point de vu de coq ?
                    lastRowFirstCol.appendChild(createValueDropdownCell ());
                }
            }
        }
    }
}


// Gère l'interaction d'un champ modifiable.
function changeValue(link) {
    if (isReaderMode) return; // Ne rien faire en mode lecteur
    try {
        // Trouve le contenu du menu déroulant
        var dropdownContent = link.closest('.dropdown-content');
        if (!dropdownContent) {
            console.error("[1.6, Le contenu du menu déroulant n'a pas pu être trouvé.]");
            return;
        }

        // Trouve le bouton "value" associé
        var button = dropdownContent.parentElement.querySelector('.value');
        if (!button) {
            console.error("[1.6, Le bouton 'value' associé n'a pas pu être trouvé.]");
            return;
        }

				// On regarde s'il existe déjà une span "value-content".
				var value_content ;
				for (const inner of button.childNodes.values ()){
					if (inner.className === "value-content") value_content = inner ;
				}

        // Récupère la valeur actuelle
        var currentValue =
					(value_content === undefined) ? button.innerText : value_content.innerText ;
        if (typeof currentValue !== 'string') {
            console.error("[1.2, La valeur actuelle du bouton n'est pas une chaîne de caractères.]");
            return;
        }

        // Crée un élément input
        var input = document.createElement('input');
        input.type = 'text';
				input.spellcheck = 'false' ;
        input.value = currentValue;

				var eventHandler =
					function (ev, valid){
						return function (evnt){
							try {
								if (valid (evnt)){
									if (this.parentElement) {
											updateButtonValue(this.parentElement, this.value);
											if (this.parentElement)
												this.parentElement.removeChild(this);
									} else {
											console.error(`[1.6, Impossible de trouver le parent de l'élément input lors de l'événement ${ev}.]`);
									}
								}
							} catch (error) {
									console.error(`[1.9, Erreur lors de la gestion de l'événement ${ev} : ${error.message}]`);
							}
						}
					} ;

        // Ajoute un gestionnaire d'événements pour détecter lorsque l'utilisateur clique à l'extérieur de la zone de texte
        input.addEventListener('blur', eventHandler ("blur", (_) => true));

        // Ajoute un gestionnaire d'événements pour détecter lorsque la touche "Entrée" est pressée
        input.addEventListener('keydown', eventHandler ("keydown", (ev) => (ev.key === 'Enter')));

        // Efface le contenu du bouton
        button.innerHTML = '';
        button.appendChild(input);
        input.focus(); // Met le focus sur l'input pour que l'utilisateur puisse commencer à saisir immédiatement
    } catch (error) {
        console.error("[1.9, Erreur générale dans la fonction changeValue : " + error.message + "]");
    }
}


// Fonction pour mettre à jour le texte du bouton avec la nouvelle valeur saisie.
function updateButtonValue(button, newValue) {
    try {

			// Pour la première colonne, il faut ajouter la déclaration de f, f', et x.
			var deriv_var = document.getElementById("var-name") ;
			var function_name = document.getElementById("function-name") ;
			var env = environnement_assistant ;
			var ret ;
			if (deriv_var !== null){
				ret = tabvar.suppose_var (env, deriv_var.value) ;
				if (ret[0] === retour_ok) env = ret[1] ;
			}
			if (function_name !== null){
				ret = tabvar.suppose_fun (env, function_name.value) ;
				if (ret[0] === retour_ok) env = ret[1] ;
				ret = tabvar.suppose_fun (env, function_name.value + "'") ;
				if (ret[0] === retour_ok) env = ret[1] ;
			}

			ret = tabvar.expr_to_dom (env, newValue) ;
			if (ret[0] === retour_ok) {
				var value_display = document.createElement ("span") ;
				value_display.className = "value-display" ;
				value_display.appendChild (ret[1]) ;
				button.appendChild (value_display) ;
				var value_content = document.createElement ("span") ;
				value_content.className = "value-content" ;
				value_content.innerText = newValue ;
				button.appendChild (value_content) ;
			} else {
				button.innerText = newValue;
			}
    } catch (error) {
        console.error("[1.9, Erreur interne lors de la mise à jour de la valeur du bouton] " + error.message);
    }
}

// fonction qui permet d'afficher une valeur choisie après un clique sur un drop-down-cell.
function toggleButton(link, value, buttonClass) {
    try {
        // Recherche du bouton par sa classe dans le menu déroulant le plus proche
        var button = link.closest('.dropdown').querySelector(`.${buttonClass}`);
        if (!button) {
            console.error(`[1.9, Erreur interne : Le bouton '${buttonClass}' n'a pas été trouvé]`);
            return;
        }
        // Met à jour le texte du bouton
        button.innerText = value;
    } catch (error) {
        console.error(`[1.9, Erreur interne lors du basculement du bouton '${buttonClass}'] ` + error.message);
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// GESTION DES COLONNES ////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Création d'un bouton drop-down spécialisé pour les questions de croissance/décroissance.
function createIncreaseDropdownCell(){
	return createDropdownCell("button", "⤮?", "increase",
					function() { toggleButton(this, this.innerText, 'increase'); },
					[
						["Croissante", "toggleButton(this, '↗', 'increase')"],
						["Décroissante", "toggleButton(this, '↘', 'increase')"],
						["Constante", "toggleButton(this, '→', 'increase')"],
						["Je ne sais pas", "toggleButton(this, '⤮?', 'increase')"]
					]) ;
}

// Création d'un bouton drop-down spécialisé pour les valeurs entre les variations.
function createValueDropdownCell(){
	return createDropdownCell("div", "0", "value",
					function() { changeValue(this); },
					[
						["Changer", "changeValue(this)"],
            ["Pas de valeur", "changeValue(this)"],
            ["Je ne sais pas", "changeValue(this)"]
					]) ;
}

// Création d'un bouton drop-down spécialisé pour les valeurs spéciales entre les signes.
function createVIDropdownCell(){
	return createDropdownCell("button", "…?", "VI",
					function() { toggleButton(this, this.innerText, 'VI');},
          [
						["Zéro", "toggleButton(this, '0', 'VI')"],
						["Valeur Interdite", "toggleButton(this, '||', 'VI')"],
						["Vide", "toggleButton(this, '∅', 'VI')"],
						["Je ne sais pas", "toggleButton(this, '…?', 'VI')"]
					]);
}

// Création d'un bouton drop-down spécialisé pour les signes.
function createSignDropdownCell(){
	return createDropdownCell("button", "±?", "moreLess",
					function() { toggleButton(this, this.innerText, 'moreLess'); },
					[
						["Négatif", "toggleButton(this, '-', 'moreLess')"],
            ["Positif", "toggleButton(this, '+', 'moreLess')"],
            ["Je ne sais pas", "toggleButton(this, '±?', 'moreLess')"]
					]);
}

// fonction qui permet d'ajouter deux colonnes quand on clique sur le bouton « ➕ » de la premiere ligne.
// La première colonne ajoutée correspond à une colonne valeur et la seconde à un ajout.
function addColumn(cellIndex) {
    if (isReaderMode) {
			// Ne rien faire en mode lecture
			console.error ("[1.9, Tentative d'ajout de colonne en mode lecture.]");
			return;
		}

    var table = document.getElementById("tableauvar");
    if (!table) {
        console.error("[1.6, Le tableau avec l'ID 'tableauvar' n'existe pas.]");
        return;
    }

    var colNumber = 2; // Nombre de colonnes à ajouter
    var buttonCellIndex = cellIndex;

    // Ajout des cellules pour chaque ligne
    for (var rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        var row = table.rows[rowIndex];

        for (var i = 0; i < colNumber; i++) {
            var newCell = createCell();

            if (rowIndex === 0) {
                // Première ligne
                if (i == 0) {
										var options = [
												["Changer", "changeValue(this)"],
												["Supprimer", "deleteColumns(this)"]
											] ;
										var val = "0" ;
										if (row.cells.length === 2) {
											// Cas spécial où c'est la première colonne qui est ajoutée.
											options = [
												["Changer", "changeValue(this)"],
												["-infini", "minusInfinity(this)"]
											] ;
											val = "-∞" ;
										} else {
											// Sinon on prend la valeur précédente (le + 1 correspond à la première colonne, des fonctions).
											val = getCellContent (row.cells[buttonCellIndex - 1]) ;
											if (val === "-∞") val = "0" ;
										}
                    var dropdownCell1 =
											createDropdownCell("div", val, "value",
												function() { changeValue(this); },
												options);
                    newCell.appendChild(dropdownCell1); // Ajout de la cellule contenant le menu déroulant à la première nouvelle colonne
                } else {
                    var button = document.createElement("button");
                    button.setAttribute("type", "button");
                    button.setAttribute("class", "add-value");
                    button.setAttribute("title", "Ajouter une valeur");
                    button.onclick = function () { addColumn(button.parentNode.cellIndex); }
                    newCell.appendChild(button);
                }
            } else if (rowIndex === table.rows.length - 1) {
                // Dernière ligne
                if (i == 1) {
                    newCell.appendChild(createIncreaseDropdownCell ());
                } else if (i==0){
                    newCell.appendChild(createValueDropdownCell ());
                }
            } else {
                // Toutes les autres lignes sauf la première et la dernière
                if (i == 0) {
                    newCell.appendChild(createVIDropdownCell ());
                } else {
                    newCell.appendChild(createSignDropdownCell ());
                }
            }
            row.insertBefore(newCell, row.cells[buttonCellIndex + 1 + i]);
        }
    }
}

// fonction qui permet de créer une nouvelle celule
function createCell(){
    var cell = document.createElement("td");
    return cell
}

// Créé une cellulle du tableau avec valeurs alternatives (éditables via un menu déroulant).
function createDropdownCell(buttonOrDiv, buttonText, buttonClass, buttonOnClick, dropdownList) {

    var dropdownCell = document.createElement("div");
    dropdownCell.className = "dropdown";

    var dropdownButton = document.createElement(buttonOrDiv);
    dropdownButton.className = buttonClass;
    dropdownButton.type = "button";
    dropdownButton.innerText = buttonText;
    dropdownButton.onclick = buttonOnClick;

    var dropdownContent = document.createElement("div");
    dropdownContent.className = "dropdown-content";

    var explanation = document.createElement("div");
    explanation.className = "dropdown-explanation";

		for (var i = 0; i < dropdownList.length; i++){
			var item = document.createElement ("button") ;
			item.setAttribute ("onclick", dropdownList[i][1]) ;
			item.innerText = dropdownList[i][0] ;
			dropdownContent.appendChild (item) ;
		}

    dropdownCell.appendChild(dropdownButton);
    dropdownCell.appendChild(dropdownContent);
    dropdownCell.appendChild(explanation);

    return dropdownCell;
}

// fonction qui permet de supprimer la colonne choisit et celle de droite
function deleteColumns(link) {
    try {
        // Sélectionne la cellule contenant le lien cliqué
        var buttonCell = link.closest('td');
        var buttonCellIndex = buttonCell.cellIndex;
        var table = document.getElementById("tableauvar");

        if (!buttonCell) {
            console.error("[1.6, Cellule contenant le lien cliqué n'a pas été trouvée]");
            return;
        }

        // Supprime les cellules à droite immédiates si elles existent
        for (var i = 0; i < table.rows.length; i++) {
            if (table.rows[i].cells[buttonCellIndex]) {
                table.rows[i].deleteCell(buttonCellIndex);
            }
        }

        // Supprime les cellules à droite suivantes (qui deviennent les nouvelles cellules immédiatement à droite) si elles existent
        for (var i = 0; i < table.rows.length; i++) {
            if (table.rows[i].cells[buttonCellIndex]) {
                table.rows[i].deleteCell(buttonCellIndex);
            }
        }
    } catch (error) {
        console.error("[1.9, Erreur interne lors de la suppression des colonnes] " + error.message);
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// GESTION DES LIGNES //////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Fonction pour ajouter une nouvelle ligne avec des placeholders différents
function addSignRow(element) {
    var table = document.getElementById("tableauvar");
    var currentRow = element.closest('tr');
    var currentRowIndex = currentRow.rowIndex;
    var firstRow = table.rows[0];
    var newRow = table.insertRow(currentRowIndex+1);
    newRow.classList.add("signRow");
    var placeholders = ['k(x)', 'l(x)', 'g(x)', 'h(x)', 'i(x)', 'j(x)'];
    var placeholderIndex = (table.rows.length - 2) % placeholders.length;
    var dropdownFunction =
			createDropdownCell("div", "" + placeholders[placeholderIndex], 'value',
				function() { changeValue(this); },
				[
					["Changer", "changeValue(this)"],
					["Inserer ligne variation", "addVariationRow(this)"],
					["Inserer ligne signe", "addSignRow(this)"],
					["Supprimer", "deleteRow(this)"]
				]);
    var newFunctionDiv = document.createElement('div');
    var newInput = document.createElement('input');
    var xInput = document.createElement('input');

    if (!table) {
        console.error("[1.6, Le tableau avec l'ID 'tableauvar' n'existe pas.]");
        return;
    }

    if (!firstRow) {
        console.error("[1.6, La deuxième ligne du tableau n'existe pas.]");
        return;
    }

    // Crée une nouvelle ligne avec des placeholders différents pour chaque nouvelle ligne
    for (var colIndex = 0; colIndex < firstRow.cells.length; colIndex++) {
        var newCell = newRow.insertCell(colIndex);
        // Attribue le placeholder approprié à la première cellule de la nouvelle ligne
        if (colIndex === 0) {
            newCell.appendChild(dropdownFunction);
						newCell.className = "funcol" ;
        }
        else if(colIndex % 2 == 1) {
            newCell.appendChild(createVIDropdownCell ());
        }
        else {
            newCell.appendChild(createSignDropdownCell ());

        }
    }

    // Création d'une div afin de renseigner le nom de la fonction
    newFunctionDiv.classList.add('enter_function');

    // Créer un nouvel élément input pour le nom de la fonction
    newInput.type = 'text';
		newInput.spellcheck = 'false' ;
    newInput.placeholder = placeholders[placeholderIndex];

    // Ajuster la taille initiale de l'input et ajouter un écouteur d'événements
    newInput.style.width = ((newInput.placeholder.length + 1) * 8) + 'px';
    newInput.addEventListener('input', function() { adjustInputWidth(newInput); });

    // Créer un nouvel élément input pour le x
    xInput.type = 'text';
    xInput.spellcheck = 'false';
    xInput.placeholder = 'x';

    // Ajuster la taille initiale de l'input et ajouter un écouteur d'événements
    xInput.style.width = ((xInput.placeholder.length + 1) * 8) + 'px';
    xInput.addEventListener('input', function() { adjustInputWidth(xInput); });

    // Ajouter les nouveaux éléments input à l'élément div
    newFunctionDiv.appendChild(document.createTextNode(placeholders[placeholderIndex].charAt(0) + '('));
    newFunctionDiv.appendChild(xInput);
    newFunctionDiv.appendChild(document.createTextNode(') = '));
    newFunctionDiv.appendChild(newInput);
}

function addVariationRow(element) {
    var table = document.getElementById("tableauvar");
    var currentRow = element.closest('tr'); // Trouve la ligne parente la plus proche de l'élément cliqué
    var currentRowIndex = currentRow.rowIndex; // Obtenez l'index de la ligne actuelle
    var firstRow = table.rows[0];
    var newRow = table.insertRow(currentRowIndex+1);
    newRow.classList.add("increaseRow");
    var placeholders = ['k(x)', 'l(x)', 'g(x)', 'h(x)', 'i(x)', 'j(x)'];
    var placeholderIndex = (table.rows.length - 2) % placeholders.length; // Sélectionne le placeholder en fonction du nombre de lignes actuelles
    var dropdownFunction =
			createDropdownCell("div", "" + placeholders[placeholderIndex], 'value',
			function() { changeValue(this); },
			[
        ["Changer", "changeValue(this)"],
        ["Inserer ligne variation", "addVariationRow(this)"],
        ["Inserer ligne signe", "addSignRow(this)"],
        ["Supprimer", "deleteRow(this)"]
			]);
    var newFunctionDiv = document.createElement('div');
    var newInput = document.createElement('input');
    var xInput = document.createElement('input');

    if (!table) {
        console.error("[1.6, Le tableau avec l'ID 'tableauvar' n'existe pas.]");
        return;
    }

    if (!firstRow) {
        console.error("[1.6, La deuxième ligne du tableau n'existe pas.]");
        return;
    }

    // Crée une nouvelle ligne avec des placeholders différents pour chaque nouvelle ligne
    for (var colIndex = 0; colIndex < firstRow.cells.length; colIndex++) {
        var newCell = newRow.insertCell(colIndex);
        // Attribue le placeholder approprié à la première cellule de la nouvelle ligne
        if (colIndex === 0) {
            newCell.appendChild(dropdownFunction);
						newCell.className = "funcol" ;
        }
        else if(colIndex % 2 == 1) {
					newCell.appendChild(createValueDropdownCell ());
        } else {
					newCell.appendChild(createIncreaseDropdownCell ());

        }
    }

    // Création d'une div afin de renseigner le nom de la fonction
    newFunctionDiv.classList.add('enter_function');

    // Créer un nouvel élément input pour le nom de la fonction
    newInput.type = 'text';
    newInput.spellcheck = 'false';
    newInput.placeholder = placeholders[placeholderIndex];

    // Ajuster la taille initiale de l'input et ajouter un écouteur d'événements
    newInput.style.width = ((newInput.placeholder.length + 1) * 8) + 'px';
    newInput.addEventListener('input', function() { adjustInputWidth(newInput); });

    // Créer un nouvel élément input pour le x
    xInput.type = 'text';
    xInput.spellcheck = 'false';
    xInput.placeholder = 'x';

    // Ajuster la taille initiale de l'input et ajouter un écouteur d'événements
    xInput.style.width = ((xInput.placeholder.length + 1) * 8) + 'px';
    xInput.addEventListener('input', function() { adjustInputWidth(xInput); });

    // Ajouter les nouveaux éléments input à l'élément div
    newFunctionDiv.appendChild(document.createTextNode(placeholders[placeholderIndex].charAt(0) + '('));
    newFunctionDiv.appendChild(xInput);
    newFunctionDiv.appendChild(document.createTextNode(') = '));
    newFunctionDiv.appendChild(newInput);
}

// supprime les lignes supplementaire
function removeMiddleRows() {
    var table = document.getElementById("tableauvar");
    var rowCount = table.rows.length;

    if (!table) {
        console.error("[1.6, Le tableau avec l'ID 'tableauvar' n'existe pas.]");
        return;
    }

    // Vérifie si le tableau contient au moins trois lignes
    if (rowCount < 3) {
        console.error("[1.7, Le tableau doit avoir au moins trois lignes pour effectuer cette opération.]");
        return;
    }

    // Supprime toutes les lignes sauf la première, l'avant-dernière et la dernière
    for (var i = rowCount - 3; i > 0; i--) {
        table.deleteRow(i);
    }
}

function deleteRow(link) {
    var row = link.closest('tr');
    if (!row) {
        console.error("[1.6, Impossible de trouver la ligne parente (tr) du lien fourni.]");
        return;
    }
    row.remove();
}


// Étant donné une cellule du tableau, retourne son sous-nœud de classe "dropdown" (s'il en existe un).
function getDropdownChild (cell){
	console.assert (cell !== undefined) ;

	for (const div of cell.childNodes.values()){
		if (div.className === "dropdown"){
			return div ;
		}
	}
}

// Retourne le contenu textuel d'une case du tableau de variation.
function getCellContent (cell){
	var dropdown = getDropdownChild (cell) ;

	var ret = "";
	if (dropdown !== undefined){
		// Dans ce cas, le menu déroulant doit être ignoré.
		for (const div of dropdown.childNodes.values ()){
			if (div.className === "dropdown-content"
				|| div.className === "dropdown-explanation") continue ;

			// TODO: Nettoyage…
			var content = undefined ;
			for (const inner of div.childNodes.values ()){
				if (inner.className === "value-display") continue ;
				else if (inner.className === "value-content") {
					content = inner.textContent ;
					break ;
				} else if (inner.className === "value") {
					for (const ininner of inner.childNodes.values ()){
						if (inner.className === "value-display") continue ;
						else if (inner.className === "value-content") {
							content = inner.textContent ;
							break ;
						}
					}
				}
			}
			if (content === undefined) ret += div.textContent ;
			else ret += content ;
		}
	} else {
		ret = cell.textContent ;
	}

	ret = ret.replace(/[\r\n\t ]+/g, " ") ;
	ret = ret.replace(/^[\r\n\t ]*/, "") ;
	ret = ret.replace(/[\r\n\t ]*$/, "") ;

	return ret ;
}

