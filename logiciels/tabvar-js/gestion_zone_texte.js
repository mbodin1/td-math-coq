//------------------------------------------------------------------
//-------------------GESTION DE LA ZONE TEXTUELLE-------------------
//------------------------------------------------------------------

// La zone textuelle est constituée d'une liste de blocs textuels.
// Certains sont ajoutés par l'utilisateurice, d'autres par l'interface.
// À chaque fois, on stocke un objet avec les champs suivants:
//	 kind: la classe de l'élément (voir les différentes classes CSS pouvant être mises sous .zonetextuelle).
//   div: l'élément HTML correspondant.
//   textarea: lorsque l'élément HTML contient une balise <textarea>, cette dernière est stockée ici.
//   env: l'environnement associé à l'assistant de preuve lorsque cet élément a été ajouté.
//   cancel: une fonction à être appellée pour mettre à jour l'interface si on venait à supprimer cet élément.
const allTextualItems = [];

// Éléments en attente d'être ajoutés dans allTextualItems.
// À chaque fois, on stocke un objet avec:
//   div: l'élément HTML correspondant.
//   textarea: sa balise <textarea>.
//   response: un élément HTML correspondant à la réponse (ou undefined).
const currentlyEditingItems = [];

// Cette variable va être modifiée pour pointer dans la bonne direction dans le bloc $(document).ready ci-dessous.
const zoneTextuelle = document.getElementById ("textualItems") ;

const editingItems = document.getElementById ("editingItems") ;

// Redimensionner la zone de texte pour l'adapter au contenu.
function auto_height (element){
	element.style.height = 'auto';
	element.style.height = element.scrollHeight + 'px';
}

function addEditingEvents (textarea){
	textarea.onkeydown = function(event) {
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault();
            onEnter(event);
        }
		};

  textarea.oninput = function() {
				auto_height (textarea) ;

        const textBeforeCursor = textarea.value.substring (0, textarea.selectionStart | 0);

        if (/$/.test (textBeforeCursor)) {
            $(textarea).autocomplete({
                source: keywords,
                position: { my: "left top", at: "left bottom", of: textarea },
                select: function(event, ui) { return true; }
            }).autocomplete("search", "");
        }
    };
}

function createEditingElement (txt){
	if (!txt) txt = "" ;

	const textarea = document.createElement ('textarea') ;
	textarea.value = txt ;
	textarea.setAttribute ('placeholder', 'Assistant de preuve textuel') ;
	textarea.setAttribute ('class', 'entreeAssistantEnCours') ;

	addEditingEvents (textarea) ;

	return textarea ;
}

function insertEditingFirst (elt){
	const div = document.createElement ('div') ;
	div.setAttribute ('class', elt.getAttribute ('class')) ;
	div.appendChild (elt) ;

	currentlyEditingItems.unshift ({ div: div, textarea: elt, response: undefined }) ;
	editingItems.prepend (div) ;
}

function insertEditingLast (elt){
	const div = document.createElement ('div') ;
	div.setAttribute ('class', elt.getAttribute ('class')) ;
	div.appendChild (elt) ;

	currentlyEditingItems.push ({ div: div, textarea: elt, response: undefined }) ;
	editingItems.appendChild (div) ;
}

function is_editable(kind){
	return (kind !== 'retourAssistant' && kind !== 'erreurAssistant')
}

// Déplace tous les éléments de allTextualItems vers currentlyEditingItems jusqu'à l'index donné.
// L'index donné n'est pas touché, mais l'environnement est réglé tel qu'il était avant son insertion.
function unstageUpTo (index){
	console.assert (allTextualItems.length > index, "unstageUpTo: length")
	for (let i = allTextualItems.length - 1; i > index; i--){
		const cancel = allTextualItems[i].cancel ;
		environnement_assistant = allTextualItems[i].env ;

		const div = allTextualItems[i].div ;
		const textarea = allTextualItems[i].textarea ;
		if (is_editable (allTextualItems[i].kind)) {
			console.assert (textarea !== undefined) ;
			insertEditingFirst (createEditingElement (textarea.value));
		}
		div.remove ();
		console.assert (allTextualItems.length === i + 1, "unstageUpTo: pop.")
		allTextualItems.pop () ;

		cancel () ;
	}

	environnement_assistant = allTextualItems[index].env ;
	// allTextualItems[index].cancel () ; // L'environnement n'a pas été supprimé, donc on n'appelle pas la fonction cancel.
}


// Ajoute un message dans l'interaction textuelle.
// Renvoit un objet avec les méthodes suivantes :
//  valid(), pour vérifier si l'objet est toujours valide.
//	remove(), pour supprimer l'élément.
//	update(msg), pour changer son contenu ().
function addTextualItem (kind, txt, env, cancel) {
	if (cancel === undefined) cancel = function () {} ;

	const editable = is_editable (kind) ;

	const index = allTextualItems.length ;

	const id = "textual_idem_" + kind + "_" + index ;

	var element = undefined ;
	var textarea = undefined;

	if (editable){
		element = document.createElement ('label') ;
		textarea = document.createElement ('textarea')
		textarea.setAttribute ('class', kind) ;
		textarea.setAttribute ('placeholder', "") ;
		textarea.value = txt ;
		textarea.setAttribute ('id', id) ;
		element.setAttribute ('for', id) ;
		element.appendChild (textarea) ;
	} else {
		element = document.createElement ('div') ;
		element.appendChild(document.createTextNode(txt)) ;
	}

	element.setAttribute ('class', kind) ;

	allTextualItems.push ({ kind: kind, div: element, textarea: textarea, env: env, cancel: cancel })

	zoneTextuelle.appendChild (element) ;

	if (editable) auto_height (textarea) ;

	const o = {} ;

	o.valid = function() {
		return (allTextualItems.length >= index) && document.body.contains (element);
	}

	o.remove = function() {
		if (!o.valid ()) return ;
		unstageUpTo (index) ;
		console.assert (environnement_assistant === env, "addTextualItem: remove, environnement.") ;
		element.remove () ;
		console.assert (allTextualItems.length === index + 1, "addTextualItem: remove, length.")
		allTextualItems.pop ()
	} ;

	o.update = function(msg) {
		if (!o.valid ()) return ;
		unstageUpTo (index) ;
		if (editable) textarea.value = msg ;
		else element.replaceChildren (document.createTextNode (msg)) ;
	}

	if (editable){
		var oninputevent = function() {
			element.setAttribute ('class', 'entreeAssistantEnCours') ;
			textarea.setAttribute ('class', 'entreeAssistantEnCours') ;
			unstageUpTo (index) ;
			console.assert (allTextualItems.length === index + 1, "addTextualItem: oninput, length.")
			allTextualItems.pop ()

			element.remove () ;
			currentlyEditingItems.unshift ({ div: element, textarea: textarea, response: undefined }) ;
			editingItems.prepend (element) ;

			element.removeEventListener ("input", oninputevent) ;
			addEditingEvents (textarea) ;

			textarea.focus () ;
		}

		element.addEventListener ("input", oninputevent) ;
	}

	return o ;
}

function addUserInputItem (txt, env) {
	return addTextualItem ('entreeAssistant', txt, env) ;
}

function addFeedback (txt, env) {
	return addTextualItem ('retourAssistant', txt, env) ;
}

function addInterfaceCode (txt, env, cancel) {
	return addTextualItem ('insertAssistant', txt, env, cancel) ;
}

function addErrorFeedback (editingItem, txt) {
	if (editingItem.response !== undefined) editingItem.response.remove () ;

	const element = document.createElement ('div') ;

	element.setAttribute ('class', 'erreurAssistant') ;
	element.appendChild(document.createTextNode(txt)) ;

	const div = editingItem.div ;

	div.parentNode.insertBefore (element, div.nextSibling);

	editingItem.response = element ;
}


// Les mots-clé seront proposées pour l'auto-complétion dans la zone dédiée lorsqu'un caractère est tapé.
// Cette procédure nécessite l'utilisation de la bibliothèque JQueryUI et JQuery (importées dans interface.html)

const keywords = [
	"Definition",
	"Variable", "Variables",
	"Hypothesis", "Hypotheses",
  "Lemma", "Theorem", "Property", "Remark", "Fact", "Corollary", "Proposition",
	"Proof",
	"Qed", "Admitted",
	"Print", "Check",
	"True", "False"
];

$(document).ready(function() {
	insertEditingLast (createEditingElement ("")) ;
});

function process_command (item, cmd){
		let env = environnement_assistant ;

    try {
				const ret = tabvar.proof_assistant (env, cmd.trim()) ;

				if (ret[0] === retour_ok){
					addUserInputItem (cmd, env) ;

					env = environnement_assistant = ret[1] ;

					const msgs = ret[2] ;
					for (let i = 0; i < msgs.length; i++)
						addFeedback (msgs[i], env) ;

					return true ;
				} else {
					addErrorFeedback (item, ret[1]) ;
				}
    } catch (error) {
        console.error (error);
        addErrorFeedback (item, error) ;
    }

	return false ;
}

// Commande exécutée lorsque l'on appuie sur la touche Entrée.
function onEnter(event) {
	while (currentlyEditingItems.length > 0){
		const current = currentlyEditingItems[0] ;
		if (current.response !== undefined) current.response.remove ();
		if (process_command (current, current.textarea.value)){
			const reached_current = (event.target === current.textarea) ;
			current.div.remove () ;
			currentlyEditingItems.shift () ;
			if (reached_current) break ;
		} else break ;
	}

	if (currentlyEditingItems.length === 0){
		const textarea = createEditingElement ("") ;
		insertEditingLast (textarea) ;
		textarea.focus () ;
	}
}

