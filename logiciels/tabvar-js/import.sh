#!/bin/bash
set -xeuo pipefail

cd ../tabvar-ocaml/ ; esy ; cd ../tabvar-js/
cp -f ../tabvar-ocaml/_build/default/web/tabvar.js .
cp -f ../tabvar-ocaml/_build/default/web/tabvar-addons.js .
cp -f ../tabvar-ocaml/_build/default/web/math.css .

