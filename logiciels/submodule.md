Lorsque vous clonez votre dépôt GitLab, vous pouvez utiliser l'option `--recursive` pour également cloner les sous-modules :

```bash
git clone --recursive git@gitlab.inria.fr:mbodin1/td-math-coq.git
```

Si vous avez déjà cloné le dépôt GitLab et que vous voulez cloner le sous-module jscoq, vous pouvez utiliser les commandes suivantes :

```bash
git submodule init
git submodule update
```

Notez que les sous-modules pointent vers un commit spécifique dans le dépôt GitHub. Si vous voulez mettre à jour le sous-module avec les derniers changements du dépôt GitHub, vous devez naviguer vers le sous-module et tirer les derniers changements :

```bash
cd jscoq
git pull origin v8.16
```

Ensuite, retournez au dépôt GitLab, commitez et poussez le changement pour mettre à jour le pointeur du sous-module :

```bash
cd ..
git commit -m "Mise à jour du sous-module"
git push
```

