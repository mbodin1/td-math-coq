TD math Coq
===========

Ce dépôt contient les documents et développements en lien avec le projet de TD
de maths en Coq.

- [doc](doc/) Publications, rapports.
- [experimentations](experimentations/) Liens vers des expérimentations avec des élèves.
- [logiciels](logiciels/) Liens vers les prototypes de logiciels conçus.
- [maquettes](maquettes/) Différentes maquettes d'interfaces / d'interactions.
- [notes](notes/) Notes de réunions.

