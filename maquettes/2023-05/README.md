
# Récits utilisateurs détaillés

Ce dossier recouvre plusieurs exemples d'interactions que l'on aimerait avoir avoir l'outil.
À chaque fois, ils se présentent sous la forme d'une présentation Beamer

- s1 : un élève créé un tableau de variation en mode textuel uniquement.
- s2 : un élève créé un tableau de variation en mode visuel uniquement.
- s3 : un élève créé un tableau de variation en alternant les modes textuels et visuels.
- s4 : un élève manipule un tableau de variation en mode visuel uniquement, fait une erreur, et la corrige.
- s5 : un élève manipule un tableau de variation en mode visuel et textuel, fait une erreur, et la corrige.

# Compilation

Nécessite l'installation de Pygments.
```bash
sudo apt install python3-pygments
```

La compilation a été testée sous XeLaTeX :
```bash
for f in `ls s[0-9]*.tex`; do
	xelatex -shell-escape ${f}
done
```

